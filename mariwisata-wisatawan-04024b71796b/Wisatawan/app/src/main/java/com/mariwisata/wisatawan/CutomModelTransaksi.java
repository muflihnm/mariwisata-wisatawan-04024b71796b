package com.mariwisata.wisatawan;

import android.util.Log;

/**
 * Created by Cerwyn on 21/02/2018.
 */

public class CutomModelTransaksi {
    public interface OnCustomStateListener {
        void stateChangeTransaksi(String destinasi, String kota, String transaksidari);
    }

    private static CutomModelTransaksi mInstance;
    private CutomModelTransaksi.OnCustomStateListener mListener;
    private boolean mState;

    private CutomModelTransaksi() {}

    public static CutomModelTransaksi getInstance() {
        if(mInstance == null) {
            mInstance = new CutomModelTransaksi();
        }
        return mInstance;
    }

    public void setListener(CutomModelTransaksi.OnCustomStateListener listener) {
        mListener = listener;
    }

    public void changeState(boolean state, String destinasi, String kota, String transaksidari) {
        if(mListener != null) {
            mState = state;
            Log.d("ChangeState", "True");
            notifyStateChange(destinasi, kota, transaksidari);
        }
    }

    public boolean getState() {
        return mState;
    }

    private void notifyStateChange(String destinasi, String kota, String transaksidari) {
        Log.d("Notify", "True");
        mListener.stateChangeTransaksi(destinasi, kota, transaksidari);
    }
}
