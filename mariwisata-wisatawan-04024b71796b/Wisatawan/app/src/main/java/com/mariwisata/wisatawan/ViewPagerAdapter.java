package com.mariwisata.wisatawan;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v4.view.PagerAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by Wasim on 11-06-2015.
 */
public class ViewPagerAdapter extends PagerAdapter {

    private Context mContext;
    private int[] mResources;
    private ArrayList<String> ArrayUrl = new ArrayList<String>();
    private URL url;
    public ViewPagerAdapter(Context mContext, ArrayList<String> ArrayUrl) {
        this.mContext = mContext;
        this.ArrayUrl = ArrayUrl;
    }

    @Override
    public int getCount() {
        return ArrayUrl.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((LinearLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        View itemView = LayoutInflater.from(mContext).inflate(R.layout.pager_item, container, false);

        ImageView imageView = (ImageView) itemView.findViewById(R.id.img_pager_item);

/*       https://stackoverflow.com/questions/4338400/getresources-does-not-work-undefined-java
 Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(), mResources[position]);
        RoundedBitmapDrawable roundedBitmapDrawable = RoundedBitmapDrawableFactory.create(mContext.getResources(), bitmap);
        roundedBitmapDrawable.setCircular(true);
        imageView.setImageDrawable(roundedBitmapDrawable);*/
//http://www.viralandroid.com/2015/11/how-to-make-imageview-image-rounded-corner-in-android.html

/*        Bitmap mbitmap = BitmapFactory.decodeResource(mContext.getResources(), mResources[position]);
        Bitmap imageRounded = Bitmap.createBitmap(mbitmap.getWidth(), mbitmap.getHeight(), mbitmap.getConfig());
        Canvas canvas = new Canvas(imageRounded);
        Paint mpaint = new Paint();
        mpaint.setAntiAlias(true);
        mpaint.setShader(new BitmapShader(mbitmap, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP));
        canvas.drawRoundRect((new RectF(0, 0, mbitmap.getWidth(), mbitmap.getHeight())), 50, 50, mpaint);// Round Image Corner 100 100 100 100
        imageView.setImageBitmap(imageRounded);*/

        //Default
       // imageView.setImageResource(mResources[position]);

        //https://stackoverflow.com/questions/30704581/make-imageview-with-round-corner-using-picasso
        Log.d("ViewAdapter", "URL:"+ArrayUrl.get(position));
        Picasso.with(container.getContext())
                .load(ArrayUrl.get(position))
                .transform(new RoundedTransformation(20, 0))
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.loading)      // optional
                .fit()
                .centerInside()
                .into(imageView);

        container.addView(itemView);

        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((LinearLayout) object);
    }
}