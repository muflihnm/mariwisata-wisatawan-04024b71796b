package com.mariwisata.wisatawan;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ViralFragmentFilter.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ViralFragmentFilter#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ViralFragmentFilter extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private GridView gridview;
    private ArrayList<DataModel> gridList;
    private DatabaseReference db;
    private String kota;
    private AdapterLmission adapter;
    private ProgressBar pb;
    private String destinasi;
    private OnFragmentInteractionListener mListener;
    private Spinner spinner1;
    String[] arraySpinner = new String[] {
            "Semua Kota", "Yogyakarta", "Kalimantan Timur", "Jawa Barat"
    };

    public ViralFragmentFilter() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ViralFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ViralFragmentFilter newInstance(String param1) {
        ViralFragmentFilter fragment = new ViralFragmentFilter();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            kota = mParam1;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_viral, container, false);
        gridview = v.findViewById(R.id.grid_viral);
        gridList = new ArrayList<DataModel>();
        spinner1 = (Spinner) v.findViewById(R.id.spinner2);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, arraySpinner);
        adapter2.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner1.setAdapter(adapter2);
        int spinnerPosition = adapter2.getPosition(kota);
        spinner1.setSelection(spinnerPosition);
        spinner1.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String txt = spinner1.getSelectedItem().toString();
                Log.d("Viral", "Terpilih"+txt);
                if(kota.equals("Semua Kota")){
                    Fragment fragment2 = new ViralFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment2);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
                if (!kota.equals(txt)) {
                        Fragment fragment2 = new ViralFragmentFilter().newInstance(txt);
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_container, fragment2);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

  /*      TextView to_referensi = (TextView) v.findViewById(R.id.textView2_viral);
        to_referensi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment2 = new ReferensiFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });*/


        pb = (ProgressBar) v.findViewById(R.id.pb_viral);
        pb.setVisibility(View.VISIBLE);

        db= FirebaseDatabase.getInstance().getReference("lmission").child(kota);
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String foto = postSnapshot.child("foto").getValue(String.class);
                    String deskripsi = postSnapshot.child("deskripsi").getValue(String.class);
                    long waktu = postSnapshot.child("waktu").getValue(long.class);
                    String creator = postSnapshot.child("creator").getValue(String.class);
                    String total_cares = postSnapshot.child("cares").child("total").getValue(int.class).toString();
                    String destinasi2 = postSnapshot.child("destinasi").getValue(String.class);
                    String key = postSnapshot.getKey();
                    String kota2 = kota;

                    String date = DateFormat.format("dd-MM-yyyy", waktu).toString();

                    gridList.add(new DataModel(foto, deskripsi, date, creator, total_cares, key, kota2, destinasi2));
                    Log.d("VIRALFRAGMENT", "Data:"+foto+deskripsi+date+creator+total_cares+key+kota2+destinasi2);
                }
                pb.setVisibility(View.INVISIBLE);
                adapter = new AdapterLmission(gridList);
                gridview.setAdapter(adapter);
            }


            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });


        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
