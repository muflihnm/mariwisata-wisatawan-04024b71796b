package com.mariwisata.wisatawan;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Cerwyn on 09/02/2018.
 */

public class AdapterTransaksi extends BaseAdapter {


    private static final String TAG = CustomAdapterWisataB.class.getSimpleName();
    ArrayList<DataModel> listArray;

    //testcase1
    Boolean saved=null;
    private String foto_pemandu;
    private ImageView gbrprofil;
    private StorageReference mStorageRef;
    private FirebaseStorage storage;
    private int index2;
    private String alamat2;
    private DatabaseReference db;
    private TextView tanggal_transaksi;

    public AdapterTransaksi(ArrayList<DataModel> listArray) {

        this.listArray = listArray;
        Log.d(TAG, "int: " + listArray.toString());

    }


    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }

    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.single_transaksi, parent, false);
        }

        final DataModel dataModel = listArray.get(index);

        TextView status_transaksi = (TextView) view.findViewById(R.id.status_transaksi);
        TextView destinasi_transaksi = (TextView) view.findViewById(R.id.destinasi_transaksi);
        TextView kota_transaksi = (TextView) view.findViewById(R.id.kota_transaksi);
        tanggal_transaksi = (TextView) view.findViewById(R.id.tanggal_transaksi);

        String s = dataModel.getStatus_transaksi();
//transaksi aktif = aktif, transaksi berhasil dan expired=non aktif, pembayaran telat = expired, menunggu pembayaran=menunggu
        if (s.equals("aktif")) {
            status_transaksi.setText("Status: Berhasil/Aktif");
        } else if(s.equals("nonaktif")){
            status_transaksi.setText("Status: Perjalanan selesai");
        } else if (s.equals("expired")){
            status_transaksi.setText("Status: Pembayaran expired/telat. Silahkan booking lagi");
        }else{
            status_transaksi.setText("Status: Menunggu Pembayaran");
        }
        Log.d("AdapterTransaksi","GET:"+dataModel.getKota_transaksi()+dataModel.getDestinasi_transaksi()+dataModel.getKey_transaksi());
        db= FirebaseDatabase.getInstance().getReference("jadwal").child(dataModel.getKota_transaksi()).child(dataModel.getDestinasi_transaksi()).child(dataModel.getKey_transaksi());
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                Log.d("AdapterTransaksi", ""+dataModel.getKey_transaksi()+dataSnapshot.child("Tanggal").getValue(String.class));
                tanggal_transaksi.setText("Tanggal Perjalanan: "+dataSnapshot.child("Tanggal").getValue(String.class));
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        destinasi_transaksi.setText(dataModel.getDestinasi_transaksi());
        kota_transaksi.setText(dataModel.getKota_transaksi());




        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String s = dataModel.getStatus_transaksi();
                if (s.equals("true")){
                    Toast.makeText(parent.getContext(), "Silahkan lihat perjalanan anda", Toast.LENGTH_SHORT).show();
                } else if (s.equals("error")){
                    Toast.makeText(parent.getContext(), "Pastikan anda membayar tepat waktu", Toast.LENGTH_SHORT).show();
                }else{
                    Toast.makeText(parent.getContext(), "Silahkan melanjutkan pembayaran", Toast.LENGTH_SHORT).show();
                }


            }
        });

        return view;
    }

}