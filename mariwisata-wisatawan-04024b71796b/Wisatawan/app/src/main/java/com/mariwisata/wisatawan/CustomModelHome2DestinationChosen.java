package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 09/03/2018.
 */

public class CustomModelHome2DestinationChosen {
    public interface OnCustomStateListener {
        void stateChangedHome2Dest(String kota, String destinasi);
    }

    private static CustomModelHome2DestinationChosen mInstance;
    private OnCustomStateListener mListener;
    private boolean mState;

    private CustomModelHome2DestinationChosen() {}

    public static CustomModelHome2DestinationChosen getInstance() {
        if(mInstance == null) {
            mInstance = new CustomModelHome2DestinationChosen();
        }
        return mInstance;
    }

    public void setListener(OnCustomStateListener listener) {
        mListener = listener;
    }

    public void changeStateHome2Dest(boolean state, String kota, String destinasi) {
        if(mListener != null) {
            mState = state;
            notifyStateChange(kota, destinasi);
        }
    }

    public boolean getState() {
        return mState;
    }

    private void notifyStateChange(String kota, String destinasi) {
        mListener.stateChangedHome2Dest(kota, destinasi);
    }
}
