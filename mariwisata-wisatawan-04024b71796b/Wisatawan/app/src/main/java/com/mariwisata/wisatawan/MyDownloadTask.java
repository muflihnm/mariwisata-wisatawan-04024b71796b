package com.mariwisata.wisatawan;

import android.os.AsyncTask;
import android.util.Log;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.ContentValues.TAG;

/**
 * Created by Cerwyn on 03/02/2018.
 */

class MyDownloadTask extends AsyncTask<Void,Void,Void>
{

    private String result;
    protected void onPreExecute() {
        //display progress dialog.

    }
    protected Void doInBackground(Void... params) {
        try {

            String url = "https://api.mainapi.net/token";
            URL obj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "Basic U2hEbUpWYl9HT0xTVkh5UDNiNU0yaXM5ZVFRYTp1cVZMQlpsMzdSNGNpMElmenVFNEpPeGlnN1Vh");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
Log.d(TAG, "dataxx" + "error1");

/*            JSONObject cred = new JSONObject();
            cred.put("grant_type","client_credentials");
            out.write(cred.toString());*/

            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            String s = "grant_type=client_credentials";
            out.write(s);
            Log.d(TAG, "dataxx" + "error2");
            out.flush();

            conn.connect();

            int HttpResult = conn.getResponseCode();
Log.d(TAG, "dataxx" + "respons"+HttpResult);
            //new InputStreamReader(conn.getInputStream());

            switch (HttpResult) {
                case 200:
                case 201:
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        sb.append(line + "\n");
                        Log.d("dataxx", "line1"+line);
                    }
                    bufferedReader.close();
                    Log.d("HTTP Client", "Received String : " + sb.toString());
                    //return received string
                    //return sb.toString();
                    result = sb.toString();
            }

            JSONObject jObject = new JSONObject(result);

            String aJsonString = jObject.getString("scope");
            String aJsonString2 = jObject.getString("access_token");
            Log.d(TAG, "dataxx" + "string1" + aJsonString+"String 2"+aJsonString2);

        } catch (Exception e) {
            Log.d(TAG, "dataxx" + "error" + e.toString());
            e.printStackTrace();
        }
        return null;
    }



    protected void onPostExecute(Void result) {
        // dismiss progress dialog and update ui
    }
}