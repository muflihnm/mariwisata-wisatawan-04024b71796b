package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 02/01/2018.
 */
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class LoadDataWisata {

    private static final String TAG = LoadDataWisata.class.getSimpleName();
    ArrayList<DataModel> listArray;
    DatabaseReference db;

    public LoadDataWisata(){
        listArray = new ArrayList<DataModel>();
        db= FirebaseDatabase.getInstance().getReference("jadwal");
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
               /* String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);*/
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Log.e(TAG, "harga======="+postSnapshot.child("fasilitas").getValue());
                    listArray.add(new DataModel("dfd", 1220, 2.38, "weq"));
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
    }

    public ArrayList<DataModel> getListArray() {
        return listArray;
    }
}
