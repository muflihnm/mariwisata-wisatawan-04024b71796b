package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 02/01/2018.
 */

import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Shahab
 * Date: 8/22/12
 * Time: 11:37 AM
 * To change this template use File | Settings | File Templates.
 */

public class CustomAdapterDestChosen extends BaseAdapter {


    private static final String TAG = CustomAdapterDestChosen.class.getSimpleName();
    ArrayList<DataModel> listArray;
    ArrayList<String> bulan = new ArrayList<String>();

    //testcase1
    DatabaseReference db;

    private net.cachapa.expandablelayout.ExpandableLayout expand_price;
    private TextView expanded_price;
    private ImageView view_price;
    private ImageView foto;

    public CustomAdapterDestChosen(ArrayList<DataModel> listArray) {

        this.listArray = listArray;
        Log.d(TAG, "int: " + listArray.toString());

    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    public void updateIfPresent(int index, String nama, String foto){
/*        DataModel dm_temp = new DataModel();
        listArray.set(index, dm_temp);*/
        listArray.get(index).setNama_destinationc(nama);
        listArray.get(index).setImage_home(foto);
        notifyDataSetChanged();
    }

    public void add(){
/*        listArray.add(listArray.get(1));
        DataModel dm = new DataModel();
        dm = listArray.get(1);
        notifyDataSetChanged();*/
    }
    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }

    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.single_destinationchosen, parent, false);
        }

        bulan.add(0, "January");
        bulan.add(1, "February");
        bulan.add(2, "March");
        bulan.add(3, "April");
        bulan.add(4, "May");
        bulan.add(5, "June");
        bulan.add(6, "July");
        bulan.add(7, "August");
        bulan.add(8, "September");
        bulan.add(9, "October");
        bulan.add(10, "November");
        bulan.add(11, "December");

        final DataModel dataModel = listArray.get(index);

        TextView nama = (TextView) view.findViewById(R.id.nama_destinationc);
        TextView tanggal = (TextView) view.findViewById(R.id.tanggal_destionationc);
        TextView fasilitas = (TextView) view.findViewById(R.id.fasilitas_destinationc);
        TextView harga = (TextView) view.findViewById(R.id.harga_destinationc);
         foto = (ImageView) view.findViewById(R.id.foto_destinationc);

        //expanded layout
/*        expand_price = (net.cachapa.expandablelayout.ExpandableLayout) view.findViewById(R.id.expand_price);
        view_price = (ImageView) view.findViewById(R.id.view_price);
        expanded_price = (TextView) view.findViewById(R.id.expanded_price);

        view_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expand_price.toggle();

            }
        });*/

        String[] parts_tanggal = dataModel.getTanggal_destinationc().split("-");
        String hari = parts_tanggal[0];
        String bulan_index = parts_tanggal[1];
        String tahun = parts_tanggal[2];
        Log.d("DestChosen", "Hari bulan tahun"+hari+bulan_index+tahun);
        int bulan_int = Integer.parseInt(bulan_index);
        Log.d("DestChosen", "Casting bulan index"+bulan_int);
        String bulan2 = bulan.get(bulan_int-1);
        Log.d("DestChosen", "Bulan2"+bulan2);
        nama.setText(dataModel.getNama_destinationc());
        fasilitas.setText(dataModel.getFasilitas_destinationc());
        harga.setText("IDR: "+dataModel.getHargatot_destinationc());
        tanggal.setText(hari+" "+bulan2+" "+tahun);

        //Circle image
       // Picasso.with(parent.getContext()).load(dataModel.getImage_home()).transform(new CircleTransform()).into(foto);

       Picasso.with(parent.getContext())
                .load(dataModel.getImage_home())
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.loading)      // optional
                .fit()
                .centerInside()
                .into(foto);
        Log.d("AdapterHome", "Transaksidari"+dataModel.getTransaksi_darihome());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomModelDestChosen2Travelmate.getInstance().changeStateDest2Mate(true, dataModel.getKota_destinatioc(), dataModel.getDestinasi_destinationc(), dataModel.getKey_destinationc());
                //CustomModelHome2DestinationChosen.getInstance().changeStateHome2Dest(true, dataModel.getKota_home(), dataModel.getDestinasi_home());
            }
        });

        return view;
    }

}