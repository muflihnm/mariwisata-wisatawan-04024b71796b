package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 09/03/2018.
 */
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

/**
 * Created by ASUS on 06/02/2018.
 */

public class ReferensiHomeDialog extends DialogFragment {

    private String destinasi;
    private String kota;
    private String foto;
    private DatabaseReference db;
    private TextView deskripsi_ref;
    private ImageView btn_exit;

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public void setDestinasi(String destinasi) {
        this.destinasi = destinasi;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        View v = getActivity().getLayoutInflater().inflate(R.layout.fragment_destinasi_fragment2, null, false);
        ImageView foto_ref = (ImageView) v.findViewById(R.id.foto_ref);
        TextView destinasi_ref = (TextView) v.findViewById(R.id.destinasi_ref);
        TextView kota_ref = (TextView) v.findViewById(R.id.kota_ref);
        deskripsi_ref = (TextView) v.findViewById(R.id.deskripsi_ref);


/*        btn_exit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });*/
        v.findViewById(R.id.layar_ref).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        destinasi_ref.setText(destinasi);
        kota_ref.setText(kota);

        Picasso.with(getActivity())
                .load(foto)
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.loading)      // optional
                .fit()
                .centerInside()
                .into(foto_ref);

        db = FirebaseDatabase.getInstance().getReference("referensi").child("destinasi").child(kota).child(destinasi);
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String deskripsi = dataSnapshot.child("deskripsi").getValue(String.class);
                if (deskripsi.isEmpty()){
                    deskripsi_ref.setText("Description not found");
                }else {
                    deskripsi_ref.setText(deskripsi);
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        builder.setView(v);
        return builder.create();
    }
}