package com.mariwisata.wisatawan;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.ContentValues.TAG;

/**
 * Created by Cerwyn on 04/02/2018.
 */
class NotifTask extends AsyncTask<Void,Void,Void> {

    private String result;
    private String param1;
    private String param2;
    private Context mContext;
    private View rootView;
    public NotifTask(String param1) {
        this.param1 = param1;
    }

    protected void onPreExecute() {
        //display progress dialog.

    }

    protected Void doInBackground(Void... params) {
        try {
            String url = "https://api.mainapi.net/token";
            URL obj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "Basic U2hEbUpWYl9HT0xTVkh5UDNiNU0yaXM5ZVFRYTp1cVZMQlpsMzdSNGNpMElmenVFNEpPeGlnN1Vh");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            Log.d(TAG, "NOTIFTASK" + "error1");
            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            String s = "grant_type=client_credentials";
            out.write(s);
            Log.d(TAG, "NOTIFTASK" + "error2");
            out.flush();
            conn.connect();
            int HttpResult = conn.getResponseCode();
            Log.d(TAG, "NOTIFTASK" + "respons" + HttpResult);
            switch (HttpResult) {
                case 200:
                case 201:
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        sb.append(line + "\n");
                        Log.d("NOTIFTASK", "line1" + line);
                    }
                    bufferedReader.close();
                    Log.d("NOTIFTASK", "Received String : " + sb.toString());
                    result = sb.toString();
            }
            JSONObject jObject = new JSONObject(result);
            String aJsonString = jObject.getString("scope");
            String bearer_token = jObject.getString("access_token");
            Log.d(TAG, "NOTIFTASK" + "string1" + aJsonString + "Bearer token " + bearer_token);



            String url2 = "https://api.mainapi.net/smsnotification/1.0.0/messages";
            Log.d("NOTIFTASK", "url"+url2);
            URL obj2 = new URL(url2);
            HttpURLConnection conn2 = (HttpURLConnection) obj2.openConnection();
            conn2.setRequestMethod("POST");
            conn2.setRequestProperty("Authorization", "Bearer "+bearer_token);
            conn2.setRequestProperty("Accept", "application/json");
            conn2.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn2.setDoOutput(true);
            conn2.setDoInput(true);
            conn2.setUseCaches(false);
            String body = "content=Selamat datang di Lamial, Lamial: get Travel and help People&msisdn="+param1;
            Log.d("NOTIFTASK", "BODY:"+body);
            OutputStreamWriter out2 = new OutputStreamWriter(conn2.getOutputStream());
            out2.write(body);
            out2.flush();
            conn2.connect();
            int HttpResult2 = conn2.getResponseCode();
            Log.d("NOTIFTASK", "Http Respon"+HttpResult2);
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
            String line;
            StringBuilder sb2 = new StringBuilder();
            while ((line = bufferedReader2.readLine()) != null) {
                Log.d("NOTIFTASK", "line1" + line);
                sb2.append(line + "\n");
            }

        } catch (Exception e) {
            Log.d(TAG, "NOTIFTASK" + "error:" + e.toString());
            e.printStackTrace();
        }
        return null;
    }


    protected void onPostExecute(Void result) {

    }
}