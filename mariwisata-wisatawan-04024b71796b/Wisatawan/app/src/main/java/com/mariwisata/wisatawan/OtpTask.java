package com.mariwisata.wisatawan;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.ContentValues.TAG;

/**
 * Created by Cerwyn on 04/02/2018.
 */

class OtpTask extends AsyncTask<Void,Void,Void> {

    private String result;
    private String param1;
    private String param2;
    private Context mContext;
    private View rootView;
    public OtpTask(String param1, Context context, View rootView) {
        this.param1 = param1;
        this.mContext=context;
        this.rootView=rootView;
    }

    protected void onPreExecute() {
        //display progress dialog.

    }

    protected Void doInBackground(Void... params) {
        try {

            String url = "https://api.mainapi.net/token";
            URL obj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "Basic U2hEbUpWYl9HT0xTVkh5UDNiNU0yaXM5ZVFRYTp1cVZMQlpsMzdSNGNpMElmenVFNEpPeGlnN1Vh");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
Log.d(TAG, "OTPTASK" + "error1");

/*            JSONObject cred = new JSONObject();
            cred.put("grant_type","client_credentials");
            out.write(cred.toString());*/

            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            String s = "grant_type=client_credentials";
            out.write(s);
Log.d(TAG, "OTPTASK" + "error2");
            out.flush();

            conn.connect();

            int HttpResult = conn.getResponseCode();
Log.d(TAG, "OTPTASK" + "respons" + HttpResult);
            //new InputStreamReader(conn.getInputStream());

            switch (HttpResult) {
                case 200:
                case 201:
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        sb.append(line + "\n");
Log.d("OTPTASK", "line1" + line);
                    }
                    bufferedReader.close();
Log.d("OTPTASK", "Received String : " + sb.toString());
                    //return received string
                    //return sb.toString();
                    result = sb.toString();
            }

            JSONObject jObject = new JSONObject(result);

            String aJsonString = jObject.getString("scope");
            String bearer_token = jObject.getString("access_token");
Log.d(TAG, "OTPTASK" + "string1" + aJsonString + "Bearer token " + bearer_token);


            int i = param1.length();
            String key = param1.substring(i-4);
Log.d("OTPTASK", "Lenght" + i + "Key: "+key);

            String url2 = "https://api.mainapi.net/smsotp/1.0.1/otp/"+key;
Log.d("OTPTASK", "url"+url2);
            URL obj2 = new URL(url2);
            HttpURLConnection conn2 = (HttpURLConnection) obj2.openConnection();
            conn2.setRequestMethod("PUT");
            conn2.setRequestProperty("Authorization", "Bearer "+bearer_token);
            conn2.setRequestProperty("Accept", "application/json");
            conn2.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn2.setDoOutput(true);
            conn2.setDoInput(true);
            conn2.setUseCaches(false);
            String phoneNum = "phoneNum="+param1+"&digit=4";
Log.d("OTPTASK", "BODY:"+phoneNum);
            String digit = "digit=4";
            OutputStreamWriter out2 = new OutputStreamWriter(conn2.getOutputStream());
            out2.write(phoneNum);
            //out2.write(digit);
            out2.flush();
            conn2.connect();
            int HttpResult2 = conn2.getResponseCode();
            Log.d("OTPTASK", "Http Respon"+HttpResult2);
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
            String line;
            StringBuilder sb2 = new StringBuilder();
            while ((line = bufferedReader2.readLine()) != null) {
                Log.d("OTPTASK", "line1" + line);
                sb2.append(line + "\n");
            }
//{"status":true,"msgId":"9292131698","message":"SUCCESS RECIPIENT PROCESSED.","maxAttempt":"~","expireIn":60}
//https://stackoverflow.com/questions/14164128/cant-access-findviewbyid-in-asynctask

        } catch (Exception e) {
            Log.d(TAG, "OTPTASK" + "error" + e.toString());
            e.printStackTrace();
        }
        return null;
    }


    protected void onPostExecute(Void result) {
/*            Button btn = (Button) rootView.findViewById(R.id.daftar);

        btn.setEnabled(true);*/
        // dismiss progress dialog and update ui

    }
}