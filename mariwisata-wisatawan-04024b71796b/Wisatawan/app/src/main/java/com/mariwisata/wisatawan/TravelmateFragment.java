package com.mariwisata.wisatawan;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TravelmateFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TravelmateFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TravelmateFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    private String kota_param;
    private String destinasi_param;
    private ProgressBar pb_dest;
    private FloatingActionButton fab;

    private DatabaseReference db;
    private DatabaseReference db_getgambar;
    private DatabaseReference db_getprofil;
    private ImageView img_dest;
    HashMap<String, String> harga_map=new HashMap<String, String>();

    private String pj;
    private String nama_pemandu;
    private String foto_profil;

    private GridView gridView;
    ArrayList<DataModel> arrayList = new ArrayList<DataModel>();

    private CustomAdapterDestChosen customAdapterDestChosen;
    private int x;
    private HashMap<String, String> nama_img = new HashMap<String, String>();
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public TravelmateFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TravelmateFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TravelmateFragment newInstance(String param1, String param2) {
        TravelmateFragment fragment = new TravelmateFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            kota_param = getArguments().getString(ARG_PARAM1);
            destinasi_param = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_travelmate, container, false);
        img_dest = (ImageView) v.findViewById(R.id.img_dest);
        gridView = (GridView) v.findViewById(R.id.grid_destinationchosen);
        pb_dest = (ProgressBar) v.findViewById(R.id.pb_dest);


        Log.d("TMFragment", "Sudahmasukk");
        pb_dest.setVisibility(View.VISIBLE);
        db= FirebaseDatabase.getInstance().getReference("jadwal").child(kota_param).child(destinasi_param);
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (final DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                    String transaksiid = postSnapshot.getKey();
                    //String fasilitas = postSnapshot.child("Fasilitas").getValue(String.class);
                    String jasa = postSnapshot.child("jasa").getValue(String.class);
                    pj = postSnapshot.child("pj").getValue(String.class);
                    String tanggal = postSnapshot.child("tanggal").getValue(String.class);
                    String jumlah_maks = postSnapshot.child("kuota").getValue(String.class);
                    long peserta = postSnapshot.child("peserta").getChildrenCount();

                    int maks_peserta = Integer.parseInt(jumlah_maks);
                    int peserta_now = (int) peserta;

                    //batasi panjang fasilitas
      /*              if (jasa.length() > 35)
                        jasa = jasa.substring(0, 34)+"...";*/

                    String harga_total = postSnapshot.child("harga").child("total").getValue(String .class);
                    Log.d("DestChosen", "Ringkasan"+transaksiid+jasa+maks_peserta+peserta_now+harga_total);
                    int i = harga_total.length();
                    Log.d("HARGA", "harga_format"+harga_total+" hargaformatlength"+i+" i-3:");
                    while((i-3) > 0){
                        int j = i-3;
                        harga_total = harga_total.substring(0, j)+"."+harga_total.substring(j, harga_total.length());
                        i = i - 3;
                        if (i == 3)
                            break;
                    }
                    Log.d("HARGA", "Harga Total"+harga_total);
                    for (DataSnapshot postSnapshot_harga : postSnapshot.child("Harga").getChildren()){
                        String key = postSnapshot_harga.getKey();
                        if (!key.equals("Total")) {
                            String value = postSnapshot_harga.getValue(String.class);
                            harga_map.put(key, value);
                            Log.d("HARGA", "Harga Deskripsi"+key+value);
                        }
                    }


                    //menghitung tanggal, jumlah peserta, dan
                    //checking

                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                    Date strDate = null;
                    try {
                        strDate = sdf.parse(tanggal);
                        Log.d("DatabaseHome", "Cek strDate"+strDate.getTime());
                    } catch (Throwable e) {
                        e.printStackTrace();
                    }
                    Log.d("DatabaseHome", "Perbandingan Tanggal"+System.currentTimeMillis()+"<"+strDate.getTime());
                    if (System.currentTimeMillis() < strDate.getTime()) { //not outdated
                        if (maks_peserta >= peserta_now){ //not full
                            Log.d("DestChosen", "List Masuk");
                            Log.d("USERGUIDE", "nama pemandu dan foto HASHMAP"+pj+"SIZE"+nama_img.size()+x);
                            arrayList.add(new DataModel(kota_param, destinasi_param, transaksiid, nama_pemandu, jasa, pj, tanggal, harga_total, foto_profil, harga_map));

                        }
                    }

                }
                arrayList = order_tgl(arrayList);
                customAdapterDestChosen = new CustomAdapterDestChosen(arrayList);
                gridView.setAdapter(customAdapterDestChosen);
                pb_dest.setVisibility(View.INVISIBLE);

                //kurang foto pemandu dan nama pemandu, kalau bisa diambil dari datanya langsung
                //nested aja kok repot
                db_getprofil= FirebaseDatabase.getInstance().getReference("Users").child("Guide");
                db_getprofil.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        int x = 0;
                        for (Map.Entry<String, String> entry : nama_img.entrySet()) {
                            String k = entry.getKey();
                            int i = Integer.parseInt(k);
                            //String s = entry.getValue();
                            String s = nama_img.get(String.valueOf(x));
                            nama_pemandu = dataSnapshot.child(s).child("Name").getValue(String.class);
                            foto_profil = dataSnapshot.child(s).child("profileImageUrl").getValue(String.class);
                            Log.d("USERGUIDE", "nama pemandu dan foto"+nama_pemandu+foto_profil+s);
                            customAdapterDestChosen.updateIfPresent(i, nama_pemandu, foto_profil);
                            x++;
                        }


                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return v;

    }
    public ArrayList<DataModel> order_tgl(ArrayList<DataModel> ar){
        ArrayList<DataModel> list_ordered = new ArrayList<DataModel>();
        x = 0;
        while(ar.size() > 1){

            //set default sdf_terkecil
            SimpleDateFormat sdf_terkecil = new SimpleDateFormat("dd-MM-yyyy");
            Date date_terkecil = null;
            DataModel dm_terkecil = ar.get(0);
            try {
                date_terkecil = sdf_terkecil.parse(dm_terkecil.getTanggal_destinationc());
            } catch (Throwable e) {
                e.printStackTrace();
            }

            //cari tanggal terkecil
            for (int i = 0 ; i < ar.size() ; i++){
                DataModel dm = ar.get(i);
                Log.d("OrderTgl", "//Tanggal Terkecil"+i+dm.getTanggal_destinationc());

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date strDate = null;
                try {
                    strDate = sdf.parse(dm.getTanggal_destinationc());
                    Log.d("DatabaseHome", "Cek strDate"+strDate.getTime());
                } catch (Throwable e) {
                    e.printStackTrace();
                }

                if (date_terkecil.getTime() > strDate.getTime())
                    date_terkecil = strDate;

            }

            //remove dari ar, cari tgl tsb, isi ke ordered
            for (int i = 0 ; i < ar.size() ; i++){
                DataModel dm = ar.get(i);

                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                Date strDate = null;
                try {
                    strDate = sdf.parse(dm.getTanggal_destinationc());
                } catch (Throwable e) {
                    e.printStackTrace();
                }

                if (date_terkecil.getTime() == strDate.getTime()){
                    Log.d("OrderTgl", "//Tukar Tanggal"+i+dm.getTanggal_destinationc());
                    list_ordered.add(dm);
                    nama_img.put(String.valueOf(x), dm.getPj_destinationc());
                    x++;
                    ar.remove(i);
                }
            }
        }
        DataModel dm = ar.get(0);
        list_ordered.add(dm);
        nama_img.put(String.valueOf(x), dm.getPj_destinationc());
        return list_ordered;
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
