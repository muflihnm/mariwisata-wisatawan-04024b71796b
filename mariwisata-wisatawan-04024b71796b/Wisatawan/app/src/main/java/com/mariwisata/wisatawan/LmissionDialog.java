package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 09/03/2018.
 */
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.like.LikeButton;
import com.like.OnLikeListener;
import com.squareup.picasso.Picasso;

/**
 * Created by ASUS on 06/02/2018.
 */

public class LmissionDialog extends DialogFragment implements OnLikeListener{

    private TextView text_tagar, text_kota, text_desc;
    private ImageView foto_ref;
    private Button btn_foto1,btshare;
    private TableLayout table_gerakan;

    private String l_tagar;
    private String l_kota;
    private String l_key;
    private String l_destinasi;
    private String l_deskripsi;
    private String l_foto;

    public void setL_tagar(String l_tagar) {
        this.l_tagar = l_tagar;
    }

    public void setL_kota(String l_kota) {
        this.l_kota = l_kota;
    }

    public void setL_key(String l_key) {
        this.l_key = l_key;
    }

    public void setL_destinasi(String l_destinasi) {
        this.l_destinasi = l_destinasi;
    }

    public void setL_deskripsi(String l_deskripsi) {
        this.l_deskripsi = l_deskripsi;
    }

    public void setL_foto(String l_foto) {
        this.l_foto = l_foto;
    }



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        View v = getActivity().getLayoutInflater().inflate(R.layout.lmission_dialog_fragment, null, false);
        text_tagar = (TextView) v.findViewById(R.id.text_tagar);
        text_kota = (TextView) v.findViewById(R.id.text_kota);
        text_desc = (TextView) v.findViewById(R.id.text_desc);
        foto_ref = (ImageView) v.findViewById(R.id.foto_ref);
        table_gerakan = (TableLayout) v.findViewById(R.id.table_gerakan);
        btn_foto1 = (Button) v.findViewById(R.id.btn_foto);
        btshare = (Button) v.findViewById(R.id.share1);
        text_tagar.setText("#"+l_tagar);
        text_kota.setText("on "+l_destinasi+" (DI Yogyakarta)");
        text_desc.setText("Desc: "+l_deskripsi);

        btshare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent myIntent = new Intent(Intent.ACTION_SEND);
                String share = "aku telah melakukan"+l_deskripsi;
                myIntent.putExtra(Intent.EXTRA_SUBJECT,share);
                startActivity(Intent.createChooser(myIntent,"Share using"));
            }
        });
        btn_foto1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,1);
            }
        });

        Picasso.with(getActivity())
                .load(l_foto)
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.loading)      // optional
                .fit()
                .centerInside()
                .into(foto_ref);


        Log.d("LMDIALOG", "Kota, key, destinasi"+l_kota+l_key+l_destinasi);
        DatabaseReference db = FirebaseDatabase.getInstance().getReference("lmission").child(l_kota).child(l_key).child("gerakan");
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapsot : dataSnapshot.getChildren()){
                    String temp = postSnapsot.getKey();
                    if (!temp.equals("done")){
                        if (postSnapsot.getValue(Boolean.class)){
                            Log.d("LMD", "temp"+temp);

                            TableRow row = new TableRow(getActivity());
                            TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                            row.setLayoutParams(lp);
                            row.setGravity(Gravity.CENTER);
                            TextView gerakan = new TextView(getActivity());

                            gerakan.setTypeface(null, Typeface.BOLD);
                            gerakan.setTextColor(Color.BLACK);
                            gerakan.setTextSize(14);
                            gerakan.setGravity(Gravity.CENTER);

                            gerakan.setText(temp);
                            row.addView(gerakan);
                            table_gerakan.addView(row);
                        }
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        builder.setView(v);
        return builder.create();
    }

    @Override
    public void liked(LikeButton likeButton) {

    }

    @Override
    public void unLiked(LikeButton likeButton) {

    }
}