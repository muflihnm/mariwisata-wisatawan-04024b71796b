package com.mariwisata.wisatawan;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PembayaranFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PembayaranFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PembayaranFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String key;
    private String harga_total;
    private String kota;
    private String destinasi;
    private DatabaseReference db;
    private DatabaseReference db2;
    private DatabaseReference myRef;
    private FirebaseUser user;
    private String obrolan;
    private long durasi;
    private String key2;
    private DatabaseReference myRef2;
    private TextView url_pembayaran;
    private String user_id;
    private TextView harga_view;
    private TextView waktu_view;
    private Button sudah_bayar;
    private OnFragmentInteractionListener mListener;

    public PembayaranFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PembayaranFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PembayaranFragment newInstance(String param1, String param2, long durasi, String param3, String param4) { //10800000
        PembayaranFragment fragment = new PembayaranFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putLong(ARG_PARAM3, durasi);
        args.putString(ARG_PARAM4, param3);
        args.putString(ARG_PARAM5, param4);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            long mParam3 = getArguments().getLong(ARG_PARAM3);
            key = mParam1;
            harga_total = mParam2;
            durasi = mParam3;
            kota = getArguments().getString(ARG_PARAM4);
            destinasi = getArguments().getString(ARG_PARAM5);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_pembayaran, container, false);
        harga_view = (TextView) v.findViewById(R.id.textView2_pembayaran);
        waktu_view = (TextView) v.findViewById(R.id.textView6_pembayaran);
        sudah_bayar = (Button) v.findViewById(R.id.button1_pembayaran);
        url_pembayaran = (TextView) v.findViewById(R.id.textView4_pembayaran);

        db= FirebaseDatabase.getInstance().getReference("jadwal").child("Yogyakarta").child("Candi Borobudur").child(key);
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                //harga_total = dataSnapshot.child("Harga").child("Total").getValue(String.class);
                harga_view.setText("Rp. "+harga_total);
                obrolan = dataSnapshot.child("Chat").getValue(String.class);
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        harga_view.setText("Rp. "+harga_total);

        new CountDownTimer(durasi, 1000) {

            public void onTick(long millisUntilFinished) {
                int seconds = (int) (millisUntilFinished / 1000) % 60 ;
                int minutes = (int) ((millisUntilFinished / (1000*60)) % 60);
                int hours   = (int) ((millisUntilFinished / (1000*60*60)) % 24);
                waktu_view.setText(hours + " Jam " + minutes + " Menit "+seconds+" Detik");
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                waktu_view.setText("done!");
            }

        }.start();

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        Log.d("PEMBAYARAN","Current Date Time : " + dateFormat.format(cal.getTime())+"Millis:"+cal.getTimeInMillis());
        long date_now = cal.getTimeInMillis();
        cal.add(Calendar.HOUR, 3); //https://www.mkyong.com/java/how-to-modify-date-time-date-manipulation-java/
        long batas_pembayaran = cal.getTimeInMillis(); //https://stackoverflow.com/questions/14337649/compare-two-datetime-in-android
        Log.d("PEMBAYARAN","Current Date Time : " + dateFormat.format(cal.getTime())+"Millis:"+batas_pembayaran);

        user = FirebaseAuth.getInstance().getCurrentUser();
        user_id = user.getUid();

        FirebaseDatabase database = FirebaseDatabase.getInstance();
        myRef = database.getReference("Users").child("Customers").child(user_id).child("Transaksi");;
        key2 =  myRef.push().getKey();

        new FinpayClass(key2, harga_total, getActivity(), v).execute();

        myRef2 = database.getReference("jadwal").child(kota).child(destinasi).child(key).child("peserta");

        db2 =  FirebaseDatabase.getInstance().getReference("Users").child("Customers").child(user_id);
        myRef.child(key2).child("transaksidari").setValue(key);
        myRef.child(key2).child("batas_pembayaran").setValue(String.valueOf(batas_pembayaran));
        myRef.child(key2).child("status").setValue("menunggu");
        myRef.child(key2).child("destinasi").setValue(destinasi); //destinasi&kota untuk melanjutkan pending pembayaran
        myRef.child(key2).child("kota").setValue(kota);

        sudah_bayar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myRef.child(key2).child("status").setValue("aktif");
                db2.child("chats").child(key).setValue(true);
                myRef2.child(user_id).setValue(false);
                Toast.makeText(getActivity(), "Pembayaran diterima. Silahkan cek obrolan dan transaksi aktif", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });
        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
