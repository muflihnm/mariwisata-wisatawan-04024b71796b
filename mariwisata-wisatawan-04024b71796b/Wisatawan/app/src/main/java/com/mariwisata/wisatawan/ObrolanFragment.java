package com.mariwisata.wisatawan;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ObrolanFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ObrolanFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ObrolanFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private EditText input;
    private OnFragmentInteractionListener mListener;
    private FirebaseListAdapter<ChatMessage> adapter;
    private ListView listOfGroups;
    int i;
    private String user_id;
    ArrayList<DataModel> listArray;
    ArrayList<String> listGroups;
    DatabaseReference db;
    DatabaseReference db2;
    FirebaseAuth mAuth;
    CustomAdapterObrolanGroup adapterObrolanGroup;
    FirebaseUser user;
    private ProgressBar pb;
    public ObrolanFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ObrolanFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ObrolanFragment newInstance(String param1, String param2) {
        ObrolanFragment fragment = new ObrolanFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_obrolan, container, false);
        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            Toast.makeText(getActivity(), "Obrolan Tidak Tersedia", Toast.LENGTH_LONG).show();
            v = inflater.inflate(R.layout.obrolan_tidak_tersedia, container, false);
            return v;
        }
        listOfGroups = (ListView) v.findViewById(R.id.list_of_groups);

        listArray = new ArrayList<DataModel>();
        listGroups = new ArrayList<String>();

        pb = (ProgressBar) v.findViewById(R.id.progressbar1);
        pb.setVisibility(View.VISIBLE);

        mAuth = FirebaseAuth.getInstance();
        user_id = mAuth.getCurrentUser().getUid();
        db = FirebaseDatabase.getInstance().getReference("Users").child("Customers").child(user_id).child("chats");
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Log.d("ObrolanFragment", "cekk "+ postSnapshot.getKey());
                    Log.d("ObrolanFragment", "cekk "+ dataSnapshot.child(postSnapshot.getKey()).getValue());
                    if (dataSnapshot.child(postSnapshot.getKey()).getValue(Boolean.class)){
                        String key = postSnapshot.getKey();
                        listGroups.add(key);
                        Log.d("ObrolanFragment", "Key "+key);
                    }
                }
                if (listGroups.size() == 0) {
                    Toast.makeText(getActivity(), "Anda tidak mempunyai obrolan", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        db2 = FirebaseDatabase.getInstance().getReference("chats");
        db2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){

                    for (int i = 0 ; i < listGroups.size() ; i++){
                        if (postSnapshot.getKey().equals(listGroups.get(i))){
                            String nama_grup = postSnapshot.child("nama").getValue(String.class);
                            String key = postSnapshot.getKey();
                            listArray.add(new DataModel(nama_grup, key));
                            Log.d("ObrolanFragment", "add "+key + nama_grup);
                        }

                    }


                }

                adapterObrolanGroup = new CustomAdapterObrolanGroup(listArray);
                listOfGroups.setAdapter(adapterObrolanGroup);
                pb.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return v;
    }



    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
