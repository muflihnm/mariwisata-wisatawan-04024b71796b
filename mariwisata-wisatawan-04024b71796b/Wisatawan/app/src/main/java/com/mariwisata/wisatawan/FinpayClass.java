package com.mariwisata.wisatawan;

import android.content.Context;
import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.ContentValues.TAG;

/**
 * Created by Cerwyn on 10/02/2018.
 */

class FinpayClass extends AsyncTask<Void,Void,Void> {

    private String result;
    private String param1;
    private String param2;
    private Context mContext;
    private View rootView;
    private int HttpResult2;
    private String key;
    private String harga_total;
    private String web;

    public FinpayClass(String param1, String param2, Context context, View view) {
        this.param1 = param1;
        this.param2 = param2;
        this.mContext = context;
        this.rootView = view;
        key = param1;
        harga_total = param2;
    }

    protected void onPreExecute() {
        //display progress dialog.

    }

    protected Void doInBackground(Void... params) {
        try {

            String url = "https://api.mainapi.net/token";
            URL obj = new URL(url);
            HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("Authorization", "Basic U2hEbUpWYl9HT0xTVkh5UDNiNU0yaXM5ZVFRYTp1cVZMQlpsMzdSNGNpMElmenVFNEpPeGlnN1Vh");
            conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setUseCaches(false);
            Log.d(TAG, "FINPAY" + "error1");

/*            JSONObject cred = new JSONObject();
            cred.put("grant_type","client_credentials");
            out.write(cred.toString());*/

            OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
            String s = "grant_type=client_credentials";
            out.write(s);
            Log.d(TAG, "FINPAY" + "error2");
            out.flush();

            conn.connect();

            int HttpResult = conn.getResponseCode();
            Log.d(TAG, "FINPAY" + "respons" + HttpResult);
            //new InputStreamReader(conn.getInputStream());

            switch (HttpResult) {
                case 200:
                case 201:
                    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                    StringBuilder sb = new StringBuilder();
                    String line;
                    while ((line = bufferedReader.readLine()) != null) {
                        sb.append(line + "\n");
                        Log.d("FINPAY", "line1" + line);
                    }
                    bufferedReader.close();
                    Log.d("FINPAY", "Received String : " + sb.toString());
                    result = sb.toString();
            }

            JSONObject jObject = new JSONObject(result);

            String aJsonString = jObject.getString("scope");
            String bearer_token = jObject.getString("access_token");
            Log.d(TAG, "FINPAY" + "string1" + aJsonString + "Bearer token " + bearer_token);

            int i = param1.length();
            String key = param1.substring(i - 4);
            Log.d("FINPAY", "length" + i + "Key: " + key);
            String url2 = "https://api.mainapi.net/finpay/2.0.0/transactions";
            Log.d("FINPAY", "url2" + url2);

            URL obj2 = new URL(url2);
            HttpURLConnection conn2 = (HttpURLConnection) obj2.openConnection();
            conn2.setRequestMethod("POST");
            conn2.setRequestProperty("Authorization", "Bearer " + bearer_token);
            conn2.setRequestProperty("Accept", "application/json");
            conn2.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            conn2.setDoOutput(true);
            conn2.setDoInput(true);
            conn2.setUseCaches(false);
            Log.d("FINPAY", "param2 " + param2);

            String otpstr = "ivp_method=create&ivp_store=19508&ivp_authkey=Q6ZLS~KHks^rwSX7&ivp_amount="+harga_total+"&ivp_currency=idr&ivp_test=0&ivp_cart="+key+"&ivp_desc=desccc&return_auth=https://www.mainapi.net/store/client/&return_decl=https://www.mainapi.net/store/client/&return_can=https://www.mainapi.net/store/client/";

            OutputStreamWriter out2 = new OutputStreamWriter(conn2.getOutputStream());
            out2.write(otpstr);

            out2.flush();
            conn2.connect();
            HttpResult2 = conn2.getResponseCode();
            Log.d("FINPAY", "Http Respon " + HttpResult2);
            BufferedReader bufferedReader2 = new BufferedReader(new InputStreamReader(conn2.getInputStream()));
            String line;
            StringBuilder sb2 = new StringBuilder();
            while ((line = bufferedReader2.readLine()) != null) {
                Log.d("FINPAY", "line1" + line);
                sb2.append(line + "\n");
            }
            String result2 = sb2.toString();
            JSONObject jObject2 = new JSONObject(result2);

            JSONObject order  = (JSONObject) jObject2.get("order");
            web = order.getString("url");
            String ref = order.getString("ref");
            Log.d("FINPAY", "WEb:"+web+" Ref:"+ref);

        } catch (Exception e) {
            Log.d(TAG, "FINPAY" + "error" + e.toString());
            e.printStackTrace();
        }
        return null;
    }


    protected void onPostExecute(Void result) {
        if (HttpResult2 == 200) {
            TextView bayar= (TextView) rootView.findViewById(R.id.textView4_pembayaran);
            bayar.setText(web);
            //bayar(Html.fromHtml("<a href=\""+ "www.google.com"+ "\">" + web + "</a>"));
        } else {
            Toast.makeText(mContext, "OTP Tidak Valid", Toast.LENGTH_SHORT).show();
        }
    }
}