package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 09/03/2018.
 */
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

/**
 * Created by ASUS on 06/02/2018.
 */

public class FilterDestChosen extends DialogFragment {

    private String destinasi;
    private String kota;
    private String foto;
    private DatabaseReference db;
    private TextView deskripsi_ref;
    private ImageView btn_exit;

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public void setDestinasi(String destinasi) {
        this.destinasi = destinasi;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        View v = getActivity().getLayoutInflater().inflate(R.layout.fragment_filter_destchosen, null, false);

        builder.setView(v);
        return builder.create();
    }
}