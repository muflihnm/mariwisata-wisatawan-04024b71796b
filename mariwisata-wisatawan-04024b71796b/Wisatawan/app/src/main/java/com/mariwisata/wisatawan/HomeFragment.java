package com.mariwisata.wisatawan;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link HomeFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private int[] mImageResources = {
            R.drawable.berita1,
            R.drawable.berita2,
            R.drawable.berita3
    };
    private ViewPagerAdapter mAdapter;
    private int dotsCount;
    private ImageView[] dots;
    private LinearLayout indicator;
    private ViewPager vp;


   private Timer timer;
    private  final long DELAY_MS = 500;//delay in milliseconds before task is to be executed
    private final long PERIOD_MS = 5000; // time in milliseconds between successive task exec
    private DatabaseReference db;
    private DatabaseReference db_natural;
    private String filter_kota;
    private TextView city_chosen;
    private ProgressBar pb_homewisata, pb_homeberita;

    ArrayList<DataModel> array_natural = new ArrayList<DataModel>();
    ArrayList<DataModel> array_historical = new ArrayList<DataModel>();
    ArrayList<DataModel> array_cultural = new ArrayList<DataModel>();
    ArrayList<DataModel> array_culinary = new ArrayList<DataModel>();
    ArrayList<DataModel> array_education = new ArrayList<DataModel>();

    ArrayList<String> ArrayUrl = new ArrayList<>();
    ArrayList<String> natural = new ArrayList<String>();
    ArrayList<String> historical = new ArrayList<String>();
    ArrayList<String> cultural = new ArrayList<String>();
    ArrayList<String> culinary = new ArrayList<String>();
    ArrayList<String> education = new ArrayList<String>();

    CustomAdapterHome adapterHome;
    CustomAdapterHome adapterHomeCulture;
    CustomAdapterHome adapterHomeHistory;
    CustomAdapterHome adapterHomeCulinary;
    CustomAdapterHome adapterHomeEducation;
    GridView natural_grid;
    GridView cultural_grid;
    GridView historical_grid;
    GridView culinary_grid;
    GridView education_grid;

    LinearLayout natural_notfound;
    LinearLayout cultural_notfound;
    LinearLayout historical_notfound;
    LinearLayout culinary_notfound;
    LinearLayout education_notfound;
    public HomeFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment HomeFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            filter_kota = getArguments().getString(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_home, container, false);

        setHasOptionsMenu(true);

        //http://www.androprogrammer.com/2015/06/view-pager-with-circular-indicator.html
        vp  = (ViewPager) v.findViewById(R.id.pager_introduction);
        indicator =(LinearLayout) v.findViewById(R.id.viewPagerCountDots);
        pb_homewisata = (ProgressBar) v.findViewById(R.id.pb_homewisata);
        pb_homeberita = (ProgressBar) v.findViewById(R.id.pb_homeberita);

        natural_grid = (GridView) v.findViewById(R.id.natural_grid);
        cultural_grid = (GridView) v.findViewById(R.id.culture_grid);
        historical_grid = (GridView) v.findViewById(R.id.historical_grid);
        culinary_grid = (GridView) v.findViewById(R.id.culinary_grid);
        education_grid = (GridView) v.findViewById(R.id.education_grid);

        natural_notfound = (LinearLayout) v.findViewById(R.id.natural_notfound);
        culinary_notfound = (LinearLayout) v.findViewById(R.id.culinary_notfound);
        cultural_notfound = (LinearLayout) v.findViewById(R.id.culture_notfound);
        historical_notfound = (LinearLayout) v.findViewById(R.id.historical_notfound);
        education_notfound = (LinearLayout) v.findViewById(R.id.educational_notfound);

        city_chosen = (TextView) v.findViewById(R.id.city_chosen);

        if (filter_kota != null)
            city_chosen.setText("#"+filter_kota);

        pb_homeberita.setVisibility(View.VISIBLE);
        db= FirebaseDatabase.getInstance().getReference("berita");
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                ArrayUrl.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String url = postSnapshot.child("foto").getValue(String.class);
                    Log.d("Home", "URL:"+url);
                    ArrayUrl.add(url);
                }
                dotsCount = 0;
                dotsCount = ArrayUrl.size();
                Log.d("DOTS", "Dot size"+dotsCount);
                mAdapter = new ViewPagerAdapter(getActivity(), ArrayUrl);
                vp.setAdapter(mAdapter);
                vp.setCurrentItem(0);
                setUiPageViewController();
                pb_homeberita.setVisibility(View.INVISIBLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        pb_homewisata.setVisibility(View.VISIBLE);
        db_natural = FirebaseDatabase.getInstance().getReference("jadwal");
        db_natural.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                array_culinary.clear();
                array_cultural.clear();
                array_education.clear();
                array_historical.clear();
                array_natural.clear();
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    String kota_temp = postSnapshot.getKey();
                    Log.d("DatabaseHome", "KOTA:"+kota_temp);
                    for (DataSnapshot postSnapshot2 : postSnapshot.getChildren()){
                        String destinasi_temp = postSnapshot2.getKey();
                        Long murah_natural = new Long(999999);
                        Long murah_historical = new Long(999999);
                        Long murah_cultural = new Long(999999);
                        Long murah_culinary = new Long(999999);
                        Long murah_education = new Long(999999);
                        Log.d("DatabaseHome", "DESTINASI:"+destinasi_temp);
                        for (DataSnapshot postSnapshot3 : postSnapshot2.getChildren()){
                            Log.d("DatabaseHome", "Child Destinasi:"+postSnapshot3.getKey());
                            String transaksi_dari = postSnapshot3.getKey();
                            String image_url = postSnapshot3.child("foto").getValue(String.class);
                            String harga_string = postSnapshot3.child("harga").child("total").getValue(String.class);
                            Long harga_long = Long.parseLong(harga_string);
                            String jenis = postSnapshot3.child("kategori").getValue(String.class).toLowerCase();
                            String tanggal = postSnapshot3.child("tanggal").getValue(String.class);

                            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
                            Date strDate = null;
                            try {
                                strDate = sdf.parse(tanggal);
                                Log.d("DatabaseHome", "Cek strDate"+strDate.getTime());
                            } catch (Throwable e) {
                                e.printStackTrace();
                            }
                            Log.d("DatabaseHome", "Perbandingan Tanggal"+System.currentTimeMillis()+"<"+strDate.getTime());
                            if (System.currentTimeMillis() < strDate.getTime()) { //not outdated

                                if (jenis.indexOf("alam") != -1){ //Jenis Alam
                                    Log.d("DatabaseHome", "ALAM"+harga_long+"<"+murah_natural);
                                    if (harga_long < murah_natural){
                                        murah_natural = harga_long;
                                        natural.add(0, kota_temp);
                                        natural.add(1, destinasi_temp);
                                        natural.add(2, image_url);
                                        natural.add(3, harga_string);
                                        natural.add(4, jenis);
                                        natural.add(5, tanggal);
                                        natural.add(6, transaksi_dari);
                                    }
                                }
                                if (jenis.indexOf("sejarah") != -1){ //Jenis Sejarah
                                    Log.d("DatabaseHome", "SEJARAH"+harga_long+"<"+murah_historical);
                                    if (harga_long < murah_historical){
                                        murah_historical = harga_long;
                                        historical.add(0, kota_temp);
                                        historical.add(1, destinasi_temp);
                                        historical.add(2, image_url);
                                        historical.add(3, harga_string);
                                        historical.add(4, jenis);
                                        historical.add(5, tanggal);
                                        historical.add(6, transaksi_dari);
                                    }
                                }
                                if (jenis.indexOf("budaya") != -1){ //Jenis Budaya
                                    Log.d("DatabaseHome", "BUDAYA"+harga_long+"<"+murah_cultural);
                                    if (harga_long < murah_cultural){
                                        murah_cultural = harga_long;
                                        cultural.add(0, kota_temp);
                                        cultural.add(1, destinasi_temp);
                                        cultural.add(2, image_url);
                                        cultural.add(3, harga_string);
                                        cultural.add(4, jenis);
                                        cultural.add(5, tanggal);
                                        cultural.add(6, transaksi_dari);
                                    }
                                }
                                if (jenis.indexOf("kuliner") != -1){ //Jenis Kuliner
                                    Log.d("DatabaseHome", "KULINER"+harga_long+"<"+murah_culinary);
                                    if (harga_long < murah_culinary){
                                        murah_culinary = harga_long;
                                        culinary.add(0, kota_temp);
                                        culinary.add(1, destinasi_temp);
                                        culinary.add(2, image_url);
                                        culinary.add(3, harga_string);
                                        culinary.add(4, jenis);
                                        culinary.add(5, tanggal);
                                        culinary.add(6, transaksi_dari);
                                    }
                                }
                                if (jenis.indexOf("edukasi") != -1){ //Jenis Edukasi
                                    Log.d("DatabaseHome", "EDUKASI"+harga_long+"<"+murah_education);
                                    if (harga_long < murah_education){
                                        murah_education = harga_long;
                                        education.add(0, kota_temp);
                                        education.add(1, destinasi_temp);
                                        education.add(2, image_url);
                                        education.add(3, harga_string);
                                        education.add(4, jenis);
                                        education.add(5, tanggal);
                                        education.add(6, transaksi_dari);
                                    }
                                }

                            }
                            //Perbandingan tanggal dl
                            // perbandingan jenis nya -> dimasukkin sesuai kelas yang sesuai
                            // perbandingan harga termurah di setiap destinasinya sesuai jenisnya
                            // Dibatasi empat (keterbatasan memori karena fixed size gridview
                            // Yang belum ada arraynya, di beri perlakuan khusus
                        }

                        if (filter_kota != null){
                            if (natural.size() != 0 && array_natural.size() <= 4 && natural.get(0).equals(filter_kota))
                                array_natural.add(new DataModel(natural.get(0), natural.get(1), natural.get(2), natural.get(3), natural.get(4), natural.get(5), natural.get(6)));
                            if (culinary.size() != 0 && array_culinary.size() <= 4 && culinary.get(0).equals(filter_kota))
                                array_culinary.add(new DataModel(culinary.get(0), culinary.get(1), culinary.get(2), culinary.get(3), culinary.get(4), culinary.get(5), culinary.get(6)));
                            if(cultural.size() != 0 && array_cultural.size() <= 4 && cultural.get(0).equals(filter_kota))
                                array_cultural.add(new DataModel(cultural.get(0), cultural.get(1), cultural.get(2), cultural.get(3), cultural.get(4), cultural.get(5), cultural.get(6)));
                            if (education.size() != 0 && array_education.size() <= 4 && education.get(0).equals(filter_kota))
                                array_education.add(new DataModel(education.get(0), education.get(1), education.get(2), education.get(3), education.get(4), education.get(5), education.get(6)));
                            if (historical.size() != 0 && array_historical.size() <= 4 && historical.get(0).equals(filter_kota))
                                array_historical.add(new DataModel(historical.get(0), historical.get(1), historical.get(2), historical.get(3), historical.get(4), historical.get(5), historical.get(6)));
                        } else {
                            if (natural.size() != 0 && array_natural.size() <= 4)
                                array_natural.add(new DataModel(natural.get(0), natural.get(1), natural.get(2), natural.get(3), natural.get(4), natural.get(5), natural.get(6)));
                            if (culinary.size() != 0 && array_culinary.size() <= 4)
                                array_culinary.add(new DataModel(culinary.get(0), culinary.get(1), culinary.get(2), culinary.get(3), culinary.get(4), culinary.get(5), culinary.get(6)));
                            if(cultural.size() != 0 && array_cultural.size() <= 4)
                                array_cultural.add(new DataModel(cultural.get(0), cultural.get(1), cultural.get(2), cultural.get(3), cultural.get(4), cultural.get(5), cultural.get(6)));
                            if (education.size() != 0 && array_education.size() <= 4)
                                array_education.add(new DataModel(education.get(0), education.get(1), education.get(2), education.get(3), education.get(4), education.get(5), education.get(6)));
                            if (historical.size() != 0 && array_historical.size() <= 4)
                                array_historical.add(new DataModel(historical.get(0), historical.get(1), historical.get(2), historical.get(3), historical.get(4), historical.get(5), historical.get(6)));
                        }



                        natural.clear();
                        culinary.clear();
                        cultural.clear();
                        education.clear();
                        historical.clear();
                        //natural dll clear
                        //termurah dihilangkan

                    }

                }
                if(array_natural.size() == 0)
                    natural_notfound.setVisibility(View.VISIBLE);
                if(array_culinary.size() == 0)
                    culinary_notfound.setVisibility(View.VISIBLE);
                if(array_cultural.size() == 0)
                    cultural_notfound.setVisibility(View.VISIBLE);
                if(array_historical.size() == 0)
                    historical_notfound.setVisibility(View.VISIBLE);
                if(array_education.size() == 0)
                    education_notfound.setVisibility(View.VISIBLE);

/*                adapterHome.notifyDataSetChanged();
                adapterHomeCulture.notifyDataSetChanged();
                */
                pb_homewisata.setVisibility(View.INVISIBLE);
                adapterHome = new CustomAdapterHome(array_natural);
                adapterHomeCulture = new CustomAdapterHome(array_cultural);
                adapterHomeHistory = new CustomAdapterHome(array_historical);
                adapterHomeCulinary = new CustomAdapterHome(array_culinary);
                adapterHomeEducation = new CustomAdapterHome(array_education);
                natural_grid.setAdapter(adapterHome);
                cultural_grid.setAdapter(adapterHomeCulture);
                historical_grid.setAdapter(adapterHomeHistory);
                culinary_grid.setAdapter(adapterHomeCulinary);
                education_grid.setAdapter(adapterHomeEducation);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if (vp.getCurrentItem() == dotsCount-1) {
                    vp.setCurrentItem(0);
                } else {
                    vp.setCurrentItem(vp.getCurrentItem() + 1, true);
                }
            }
        };

        timer = new Timer(); // This will create a new Thread
        timer .schedule(new TimerTask() { // task to be scheduled

            @Override
            public void run() {
                handler.post(Update);
            }
        }, DELAY_MS, PERIOD_MS);

        vp.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                for (int i = 0; i < dotsCount; i++) {
                    dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));
                }
                dots[position].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        return v;
    }

    private void setUiPageViewController() {

      // dotsCount = 3;
        dots = new ImageView[dotsCount];

        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(getActivity());
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.nonselecteditem_dot));

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.setMargins(4, 0, 4, 0);

            indicator.addView(dots[i], params);
        }

        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.selecteditem_dot));
    }


    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
