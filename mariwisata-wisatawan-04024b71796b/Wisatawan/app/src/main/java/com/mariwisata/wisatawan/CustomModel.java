package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 04/01/2018.
 */

public class CustomModel {
    public interface OnCustomStateListener {
        void stateChanged(String kota, String destinasi);
    }

    private static CustomModel mInstance;
    private OnCustomStateListener mListener;
    private boolean mState;

    private CustomModel() {}

    public static CustomModel getInstance() {
        if(mInstance == null) {
            mInstance = new CustomModel();
        }
        return mInstance;
    }

    public void setListener(OnCustomStateListener listener) {
        mListener = listener;
    }

    public void changeState(boolean state, String kota, String destinasi) {
        if(mListener != null) {
            mState = state;
            notifyStateChange(kota, destinasi);
        }
    }

    public boolean getState() {
        return mState;
    }

    private void notifyStateChange(String kota, String destinasi) {
        mListener.stateChanged(kota, destinasi);
    }
}
