package com.mariwisata.wisatawan;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link Referensi2Fragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link Referensi2Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class Referensi2Fragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String destinasi;
    private String kota;
    private String deskripsi;
    private String foto;

    private OnFragmentInteractionListener mListener;

    public Referensi2Fragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment Referensi2Fragment.
     */
    // TODO: Rename and change types and number of parameters
    public static Referensi2Fragment newInstance(String param1, String param2, String param3, String param4) {
        Referensi2Fragment fragment = new Referensi2Fragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        args.putString(ARG_PARAM4, param4);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            destinasi = getArguments().getString(ARG_PARAM1);
            kota = getArguments().getString(ARG_PARAM2);
            deskripsi = getArguments().getString(ARG_PARAM3);
            foto = getArguments().getString(ARG_PARAM4);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_referensi2, container, false);
        ImageView foto_ref = (ImageView) v.findViewById(R.id.foto_ref);
        TextView destinasi_ref = (TextView) v.findViewById(R.id.destinasi_ref);
        TextView kota_ref = (TextView) v.findViewById(R.id.kota_ref);
        TextView deskripsi_ref = (TextView) v.findViewById(R.id.deskripsi_ref);

        destinasi_ref.setText(destinasi);
        kota_ref.setText(kota);
        deskripsi_ref.setText(deskripsi);

        Picasso.with(getContext())
                .load(foto)
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.loading)      // optional
                .fit()
                .centerInside()
                .into(foto_ref);

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
