package com.mariwisata.wisatawan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import org.w3c.dom.Text;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link UploadLmissionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link UploadLmissionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class UploadLmissionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private EditText captionlmission;
    private ImageView imagelmission;
    private Button buttonlmission;
    private Uri ResultUri;
    private String destinasi, kota, transaksidari;
    private FirebaseUser user;
    private String user_id;
    private TextView kota_spinner;
    private TextView destinasi_spinner;

    private DatabaseReference db;
    private DatabaseReference db2;
    public UploadLmissionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment UploadLmissionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static UploadLmissionFragment newInstance(String param1, String param2, String param3) {
        UploadLmissionFragment fragment = new UploadLmissionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            destinasi = mParam1;
            kota = mParam2;
            transaksidari = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_uploadlmission, container, false);

        imagelmission = (ImageView) v.findViewById(R.id.imagelmission);
        captionlmission = (EditText) v.findViewById(R.id.captionlmission);
        buttonlmission = (Button) v.findViewById(R.id.buttonlmission);
        destinasi_spinner = (TextView) v.findViewById(R.id.textView9);
        kota_spinner = (TextView) v.findViewById(R.id.textView8);

        destinasi_spinner.setText(destinasi);
        kota_spinner.setText(kota);
        buttonlmission.setText("Upload L-Mission");

        imagelmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                startActivityForResult(intent,1);
            }
        });

        buttonlmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String caption = captionlmission.getText().toString();
                user = FirebaseAuth.getInstance().getCurrentUser();
                user_id = user.getUid();
                if (ResultUri != null){
                    db = FirebaseDatabase.getInstance().getReference("lmission").child(kota).push();
                    String key = db.getKey();
                    db2 = FirebaseDatabase.getInstance().getReference("lmission").child(kota).child(key);
                    db2.child("creator").setValue(user_id);
                    db2.child("deskripsi").setValue(caption);
                    db2.child("destinasi").setValue(destinasi);
                    db2.child("cares").child("total").setValue(0);
                    Calendar cal = Calendar.getInstance();
                    long waktu = cal.getTimeInMillis();
                    db2.child("waktu").setValue(waktu);
                    StorageReference filePath = FirebaseStorage.getInstance().getReference().child("lmission").child(key);
                    Bitmap bitmap = null;
                    try {
                        bitmap = MediaStore.Images.Media.getBitmap(getActivity().getApplication().getContentResolver(),ResultUri);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    ByteArrayOutputStream baos = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG,20,baos);
                    byte[] data = baos.toByteArray();
                    UploadTask uploadTask = filePath.putBytes(data);
                    uploadTask.addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            return;
                        }
                    });
                    uploadTask.addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                        @Override
                        public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                            @SuppressWarnings("VisibleForTests") Uri downloadUri = taskSnapshot.getDownloadUrl();

                            Map newImage = new HashMap();
                            newImage.put("profileImageUrl",downloadUri.toString());
                            String donloduri = downloadUri.toString();
                            db2.child("foto").setValue(donloduri);
                            Toast.makeText(getActivity(),"Berhasil upload lmission", Toast.LENGTH_SHORT).show();
                            return;
                        }
                    });
                    Toast.makeText(getActivity(),"Berhasil upload lmission", Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(getActivity(), MainActivity.class);
                    startActivity(intent);
                } else{
                    Toast.makeText(getActivity(),"Upload Gambar", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;
    }
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1 && resultCode == Activity.RESULT_OK)
        {
            final Uri imageUri = data.getData();
            ResultUri = imageUri;
            imagelmission.setImageURI(ResultUri);
        }
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
