package com.mariwisata.wisatawan;

import android.app.Application;
import android.os.SystemClock;

/**
 * Created by Cerwyn on 08/01/2018.
 */

public class SplashScreenTimer extends Application {
    @Override
    public void onCreate() {
        super.onCreate();
        SystemClock.sleep(100);
    }
}
