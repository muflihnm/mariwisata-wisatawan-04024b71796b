package com.mariwisata.wisatawan;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ReferensiFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ReferensiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ReferensiFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    GridView gridView;
    private String[] lokasi;
    private String[] osImages;
    private DatabaseReference db;
    private ArrayList<DataModel> gridList;
    private OnFragmentInteractionListener mListener;
    private CustomAdapterGrid adapter;

    private ProgressBar pb;
    public ReferensiFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ReferensiFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static ReferensiFragment newInstance(String param1, String param2) {
        ReferensiFragment fragment = new ReferensiFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_referensi, container, false);
        gridView = v.findViewById(R.id.grid_referensi);
        TextView to_viral = (TextView) v.findViewById(R.id.textView_referensi);
        to_viral.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment2 = new ViralFragment().newInstance("","");
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        gridList = new ArrayList<DataModel>();

       pb = (ProgressBar) v.findViewById(R.id.pb);
        pb.setVisibility(View.VISIBLE);

        db= FirebaseDatabase.getInstance().getReference("referensi").child("destinasi");
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
               /* String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);*/
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    for (DataSnapshot postSnapshot2 : postSnapshot.getChildren()) {


                        String deskripsi_temp = postSnapshot2.child("deskripsi").getValue(String.class);
                        String foto_temp = postSnapshot2.child("foto").getValue(String.class);
                        String kota_temp = postSnapshot.getKey();
                        String destinasi_temp = postSnapshot2.getKey();
                        Log.d("Rangkuman", deskripsi_temp+foto_temp+kota_temp+destinasi_temp);

                        gridList.add(new DataModel(destinasi_temp, kota_temp, foto_temp, deskripsi_temp, 1));

                    }


                }
                pb.setVisibility(View.INVISIBLE);
                adapter = new CustomAdapterGrid(gridList);
                gridView.setAdapter(adapter);
            }


            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });



/*          String[] osNameList = {
                "Android",
                "iOS",
                "Linux",
                "MacOS",
                "MS DOS",
                "Symbian",
                "Windows 10",
                "Windows XP",
        };*/
/*          int[] osImages = {
                R.drawable.background,
                R.drawable.candi,
                R.drawable.chip_1,
                R.drawable.goapindul,
                R.drawable.ic_menu_camera,
                R.drawable.ic_card_travel_black_48dp,
                R.drawable.ic_card_travel_black_48dp,
                R.drawable.ic_menu_manage,};*/


/*        CustomAdapterGrid adapter = new CustomAdapterGrid(osNameList, osImages);
        gridView.setAdapter(adapter);*/
        return v;

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
