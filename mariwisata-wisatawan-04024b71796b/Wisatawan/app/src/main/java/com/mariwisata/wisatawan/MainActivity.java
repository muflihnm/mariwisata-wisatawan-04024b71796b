package com.mariwisata.wisatawan;

import android.app.SearchManager;
import android.content.Intent;
import android.database.MatrixCursor;
import android.graphics.Color;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.BaseColumns;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.CursorAdapter;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.aurelhubert.ahbottomnavigation.AHBottomNavigation;
import com.aurelhubert.ahbottomnavigation.AHBottomNavigationItem;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.mariwisata.wisatawan.WisataFragment;
import com.mariwisata.wisatawan.ObrolanFragment;
import com.mariwisata.wisatawan.ReferensiFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.view.ViewGroup;
import android.view.ViewStub;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;


import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;
import java.util.List;
import java.util.Locale;

import static android.content.ContentValues.TAG;
import static java.net.Proxy.Type.HTTP;

import com.google.firebase.auth.FirebaseUser;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener,
        WisataFragment.OnFragmentInteractionListener,
ObrolanFragment.OnFragmentInteractionListener,
ReferensiFragment.OnFragmentInteractionListener, WisataFragment_B.OnFragmentInteractionListener, AwalFragment.OnFragmentInteractionListener, CustomModel.OnCustomStateListener, CustomModelRangkuman.OnCustomStateListener,
        RangkumanFragment.OnFragmentInteractionListener,CustomModelReferensi.OnCustomStateListener, Referensi2Fragment.OnFragmentInteractionListener, LoginFragment.OnFragmentInteractionListener,
DaftarFragment.OnFragmentInteractionListener, CustomModelObrolan.OnCustomStateListener, ObrolanFragment_2.OnFragmentInteractionListener, EditBiodataFragment.OnFragmentInteractionListener, ViralFragment.OnFragmentInteractionListener,
CustomModelLmission.OnCustomStateListener, LMissionFragment.OnFragmentInteractionListener, PembayaranFragment.OnFragmentInteractionListener, TransaksiFragment.OnFragmentInteractionListener, TransaksiAktifFragment.OnFragmentInteractionListener
, CutomModelTransaksi.OnCustomStateListener, PerjalananAktifFragment.OnFragmentInteractionListener, UploadLmissionFragment.OnFragmentInteractionListener, MyLmissionFragment.OnFragmentInteractionListener, WisataFragment_BFilter.OnFragmentInteractionListener
, ViralFragmentFilter.OnFragmentInteractionListener, HomeFragment.OnFragmentInteractionListener, CityChosenFragment.OnFragmentInteractionListener, CustomModelHome.OnCustomStateListener, DestinationChosenFragment.OnFragmentInteractionListener
, CustomModelHome2DestinationChosen.OnCustomStateListener, TravelmateChosenFragment.OnFragmentInteractionListener, CustomModelDestChosen2Travelmate.OnCustomStateListener, RequestFragment.OnFragmentInteractionListener, TravelmateFragment.OnFragmentInteractionListener{
    private static final String[] SUGGESTIONS = {
            "DI Yogyakarta"
    };
    private Toolbar toolbar;
private SimpleCursorAdapter mAdapter;
private AHBottomNavigation bottomNavigation;
    FirebaseAuth mAuth;
    FirebaseUser user;
    TextView name;
    public String nama_profil;
    private SearchView searchView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                "/Android/data/mariwisata/files/data.txt";
        File directory = new File(path);
        if (directory.exists()) {
            Log.w(TAG, "Directory Exist");
            if(directory.delete()){
                Log.w(TAG, "Directory success deleted");
            }
        }
        else {
            Log.w(TAG, "Directory not Exist");
            final Intent introclass = new Intent(MainActivity.this, IntroActivity.class);

            runOnUiThread(new Runnable() {
                @Override public void run() {
                    startActivity(introclass);
                }
            });

/*            if (directory.mkdirs()) {  Log.w(TAG, "Directory success Created");  }
            Log.w(TAG, "Directory Created");*/
        }

getRequest();
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(Color.BLACK);
        toolbar.setNavigationIcon(R.drawable.logofix);
        //toolbar.setTitle("");

        CustomModel.getInstance().setListener(this);
        CustomModelRangkuman.getInstance().setListener(this);
        CustomModelReferensi.getInstance().setListener(this);
        CustomModelObrolan.getInstance().setListener(this);
        CustomModelLmission.getInstance().setListener(this);
        CutomModelTransaksi.getInstance().setListener(this);
        CustomModelHome.getInstance().setListener(this);
        CustomModelHome2DestinationChosen.getInstance().setListener(this);
        CustomModelDestChosen2Travelmate.getInstance().setListener(this);


        AHBottomNavigationItem item1 =
                new AHBottomNavigationItem("Referensi",
                        R.drawable.referensi, R.color.cyan);
        AHBottomNavigationItem item2 =
                new AHBottomNavigationItem("Beranda",
                        R.drawable.home, R.color.aqua);
        AHBottomNavigationItem item3 =
                new AHBottomNavigationItem("Obrolan",
                        R.drawable.chat, R.color.magenta);

        bottomNavigation = (AHBottomNavigation) findViewById(R.id.bottom_navigation);
/*        BottomNavigationView mBottomNavigationView = (BottomNavigationView) findViewById(R.id.navigation);*/
/*        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams) bottomNavigation.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationViewBehavior());*/

        // Set background color
        bottomNavigation.setDefaultBackgroundColor(Color.parseColor("#FEFEFE"));
        // Change colors
        bottomNavigation.setAccentColor(Color.parseColor("#12a1b6"));
        bottomNavigation.setInactiveColor(Color.parseColor("#747474"));

        //  Enables color Reveal effect
/*        bottomNavigation.setColored(true);

        bottomNavigation.setColoredModeColors(Color.WHITE,Color.BLUE);*/
        bottomNavigation.addItem(item1);
        bottomNavigation.addItem(item2);
        bottomNavigation.addItem(item3);
        bottomNavigation.setBehaviorTranslationEnabled(true);

        bottomNavigation.setCurrentItem(1);
        Fragment fragment;
        fragment = new HomeFragment();
        loadFragment(fragment);

        bottomNavigation.setOnTabSelectedListener(new AHBottomNavigation.OnTabSelectedListener() {
            @Override
            public boolean onTabSelected(int position, boolean wasSelected) {
                Fragment fragment;
                switch (position){
                    case 0:
                        toolbar.setTitle("Referensi");
                        fragment = new ReferensiFragment();
                        //fragment = new TravelmateChosenFragment();
                        loadFragment(fragment);
                        return true;
                    case 1:
/*                        fragment = new WisataFragment().newInstance("Yogyakarta");
                        loadFragment(fragment);*/
                        toolbar.setTitle("Beranda");
                        fragment = new HomeFragment();
                        loadFragment(fragment);
                        return true;
                    case 2:
                        toolbar.setTitle("Obrolan");
                        fragment = new ObrolanFragment();
                        loadFragment(fragment);

                        return true;

                }
            return true;
            }
        });

 /*       loadFragment(new WisataFragment());
        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                Fragment fragment;
                switch (item.getItemId()) {
                    case R.id.cari:
                        fragment = new WisataFragment();
                        loadFragment(fragment);
                        toolbar.setTitle("Pilih Jadwal");
                        setSupportActionBar(toolbar);
                        return true;
                    case R.id.referensi:
                        fragment = new ReferensiFragment();
                        loadFragment(fragment);
                        toolbar.setTitle("Referensi/Buat Jadwal");
                        setSupportActionBar(toolbar);
                        return true;
                    case R.id.akunku:
                        fragment = new ObrolanFragment();
                        loadFragment(fragment);
                        toolbar.setTitle("Obrolan");
                        setSupportActionBar(toolbar);
*//*                        ViewStub stub3 = (ViewStub) findViewById(R.id.layout_stub);
                        stub3.setLayoutResource(R.layout.content_main_tambahan);
                        stub3.inflate();*//*
                        return true;

                }
                return false;
            }

        });
*/
/*        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);

        mAuth = FirebaseAuth.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null) {
            //belum login
            Log.d("Login", "Belum Login");

            navigationView.inflateMenu(R.menu.activity_main_drawer_notlogin);
            navigationView.inflateHeaderView(R.layout.nav_header_main_notlogin);
        } else {
            // sudah login
            Log.d("Login", "Sudah Login");
            navigationView.inflateMenu(R.menu.activity_main_drawer);
            View headerLayout = navigationView.inflateHeaderView(R.layout.nav_header_main);

/*            Menu menu = navigationView.getMenu();
            MenuItem test2 = menu.findItem(R.id.nav_camera);
            test2.setTitle("Test 2 cuyy");*/

            name = (TextView) headerLayout.findViewById(R.id.nama_header);
            TextView email = (TextView) headerLayout.findViewById(R.id.email_header);

            email.setText(user.getEmail());

            DatabaseReference  db= FirebaseDatabase.getInstance().getReference("Users").child("Customers");
            db.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    String nama = dataSnapshot.child(user.getUid()).child("Nama Profil").getValue(String.class);
                    name.setText(nama);
                    nama_profil = nama;
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            headerLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d("MainActivity", "Header di click");
                    toolbar.setTitle("Biodata");
                    Fragment fragment;
                    fragment = new EditBiodataFragment();
                    loadFragment(fragment);
                }
            });


        }

        navigationView.setNavigationItemSelectedListener(this);
    }

    public String getNama_profil(){
        return nama_profil;
    }
    public void changeFragment(int i, String toolbars){
        Fragment fragment;
        switch (i){
            case 1:
                toolbar.setTitle("Beranda");
                bottomNavigation.setCurrentItem(1);
                fragment = new AwalFragment();
                loadFragment(fragment);
                break;
            case 2:
                toolbar.setTitle("Referensi");
                bottomNavigation.setCurrentItem(0);
                fragment = new ReferensiFragment();
                loadFragment(fragment);
                break;
            case 3:
                toolbar.setTitle("Obrolan");
                bottomNavigation.setCurrentItem(2);
                fragment = new ObrolanFragment();
                loadFragment(fragment);
                break;
        }
    }

    @Override
    public void stateChanged(String kota, String destinasi) {
        Fragment fragment;
        fragment = new WisataFragment_B().newInstance(kota, destinasi);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(destinasi);
        setSupportActionBar(toolbar);
        loadFragment(fragment);


        /*boolean modelState = CustomModel.getInstance().getState();*/
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }

/*        if (getFragmentManager().getBackStackEntryCount() > 0 ) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.menu_search_google, menu);
       //searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        //searchView.setBackgroundColor(Color.WHITE);
        searchView = (SearchView) findViewById(R.id.searchview2);
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        final String[] from = new String[] {"cityName"};
        final int[] to = new int[] {R.id.text1};
        mAdapter = new SimpleCursorAdapter(this,
                R.layout.simple_list_item_1_custom,
                null,
                from,
                to,
                CursorAdapter.FLAG_REGISTER_CONTENT_OBSERVER);
        searchView.setSuggestionsAdapter(mAdapter);
        searchView.setIconified(false);
        searchView.setIconifiedByDefault(false);
        //searchView.requestFocus();
        searchView.setQueryHint("Choose city");
        searchView.clearFocus();
        //searchView.setFocusable(true);
        searchView.setIconified(false);
        searchView.requestFocusFromTouch();
        searchView.clearFocus();

        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("SearchView", "disentuh");

            }
        });
        searchView.setOnSearchClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("SearchView", "disentuh2");
            }
        });

        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (hasFocus) {
                    Log.d("SearchView", "disentuh3");
                    populateAdapter("");
                }
            }
        });

        searchView.setOnSuggestionListener(new SearchView.OnSuggestionListener() {
            @Override
            public boolean onSuggestionSelect(int position) {
                Log.d("SearchView clicked", "1");
                return false;
            }

            @Override
            public boolean onSuggestionClick(int position) {
                Log.d("SearchView clicked", "2");
                MatrixCursor cursor = (MatrixCursor) searchView.getSuggestionsAdapter().getItem(position);
                int indexColumnSuggestion = cursor.getColumnIndex("cityName");
                searchView.setQuery(cursor.getString(indexColumnSuggestion), false);

                String s = cursor.getString(indexColumnSuggestion);
                boolean b = false;
                for (int i = 0; i < SUGGESTIONS.length; i++) {
                    if (SUGGESTIONS[i].equals(s))
                        b = true;
                }
                if (b){
                    toolbar.setTitle(s);
                    Fragment fragment2 = new HomeFragment().newInstance(s);
                    loadFragment(fragment2);
                    return true;
                } else {
                    return true;
                }

            }
        });
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                String s = query;
                Log.d("SearchView query", s);
                //s.toLowerCase();
                boolean b = false;
                for (int i = 0; i < SUGGESTIONS.length; i++) {
                    if (SUGGESTIONS[i].equals(query))
                        b = true;
                }
                if (b){
                    toolbar.setTitle(s);
                    Fragment fragment2 = new WisataFragment().newInstance(s);
                    loadFragment(fragment2);
                    return true;
                } else {

                    return true;
                }

            }

            @Override
            public boolean onQueryTextChange(String newText) {
                String s = newText;
                Log.d("SearchView newText", s);
                populateAdapter(newText);
                return false;
            }
        });



        return true;
    }
    private void populateAdapter(String query) {
        final MatrixCursor c = new MatrixCursor(new String[]{ BaseColumns._ID, "cityName" });
        Log.d("SearchView panjang ", "Q:"+ query.length());
        if(query.length() != 0) {
            for (int i = 0; i < SUGGESTIONS.length; i++) {
                if (SUGGESTIONS[i].toLowerCase().startsWith(query.toLowerCase()))
                    c.addRow(new Object[]{i, SUGGESTIONS[i]});
            }
        } else {
            Log.d("SearchView test", "test tampil semua");
            for (int i = 0; i < SUGGESTIONS.length; i++) {
                    c.addRow(new Object[]{i, SUGGESTIONS[i]});
                Log.d("SearchView masukkan", "test tampil semua"+SUGGESTIONS[i]);
            }
        }
/*        for (int i = 0; i < SUGGESTIONS.length; i++)
            c.addRow(new Object[]{i, SUGGESTIONS[i]});*/
        mAdapter.changeCursor(c);
        mAdapter.notifyDataSetChanged();
        //searchView.setSuggestionsAdapter(mAdapter);
    }
    
    public void showDatePickerDialog(View v) {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.histori_transaksi) {
            Fragment fragment;
            fragment = new TransaksiFragment();
            loadFragment(fragment);
        } else if (id == R.id.perjalanan_anda) {
            Fragment fragment;
            fragment = new TransaksiAktifFragment();
            loadFragment(fragment);
        } else if (id == R.id.nav_slideshow) {
            Fragment fragment;
            fragment = new MyLmissionFragment();
            loadFragment(fragment);
        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.keluar) {
            mAuth.signOut();
            Intent intent = new Intent(MainActivity.this,MainActivity.class);
            startActivity(intent);
        } else if (id == R.id.masuk){
            Log.d("Masuk", "Masuk berhasil");
            Fragment fragment;
            fragment = new LoginFragment();
            loadFragment(fragment);
        }else if (id==R.id.daftar){
            Log.d("Daftar", "Daftar berhasil");
            Fragment fragment;
            fragment = new DaftarFragment();
            loadFragment(fragment);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;

    }

    private void loadFragment(Fragment fragment) {
        // load fragment
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.enter, R.anim.exit, R.anim.pop_enter, R.anim.pop_exit);
        transaction.replace(R.id.frame_container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void stateChangedPemandu(String kota, String destinasi, String pemandu) {
        Fragment fragment;
        fragment = new RangkumanFragment().newInstance(kota, destinasi, pemandu);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
/*        setSupportActionBar(toolbar);
        toolbar.setTitle(destinasi);
        setSupportActionBar(toolbar);*/
        loadFragment(fragment);
    }

    @Override
    public void stateChangedReferensi(String destinasi, String kota, String deskripsi, String foto) {
/*        Fragment fragment;
        fragment = new Referensi2Fragment().newInstance(destinasi, kota, deskripsi, foto);
        loadFragment(fragment);*/
        DetailDestinasi detailDestinasi=new DetailDestinasi();
        detailDestinasi.setDeskripsi(deskripsi);
        detailDestinasi.setDestinasi(destinasi);
        detailDestinasi.setFoto(foto);
        detailDestinasi.setKota(kota);
        detailDestinasi.show(getFragmentManager(),"");
    }
    @Override
    public void stateChangedLmission( String tagar, String kota, String key, String destinasi, String deskripsi, String foto) {
        LmissionDialog lmissionDialog = new LmissionDialog();
        lmissionDialog.setL_tagar(tagar);
        lmissionDialog.setL_kota(kota);
        lmissionDialog.setL_key(key);
        lmissionDialog.setL_destinasi(destinasi);
        lmissionDialog.setL_deskripsi(deskripsi);
        lmissionDialog.setL_foto(foto);
        lmissionDialog.show(getFragmentManager(), "");

    }
    @Override
    public void stateChangedObrolan(String key, String grup) {
        Fragment fragment;
        fragment = new ObrolanFragment_2().newInstance(key, grup);
        loadFragment(fragment);

    }

    @Override
    public void stateChangeTransaksi(String destinasi, String kota, String transaksidari) {
        Fragment fragment;
        fragment = new PerjalananAktifFragment().newInstance(destinasi, kota, transaksidari);
        loadFragment(fragment);
    }

    public static String getRequest() {
        
        new MyDownloadTask().execute();
        return "re";
    }


    @Override
    public void stateChangedHomeRef(String kota, String destinasi, String foto) {
        ReferensiHomeDialog detailDestinasi=new ReferensiHomeDialog();
        detailDestinasi.setDestinasi(destinasi);
        detailDestinasi.setFoto(foto);
        detailDestinasi.setKota(kota);
        detailDestinasi.show(getFragmentManager(),"");
    }

    @Override
    public void stateChangedHome2Dest(String kota, String destinasi) {
        Fragment fragment;
        fragment = new DestinationChosenFragment().newInstance(kota, destinasi);
        loadFragment(fragment);
    }

    @Override
    public void stateChangedDest2Mate(String kota, String destinasi, String id) {
        Fragment fragment;
        fragment = new TravelmateChosenFragment().newInstance(kota, destinasi, id);
        loadFragment(fragment);
    }
}
