package com.mariwisata.wisatawan;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link CityChosenFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link CityChosenFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class CityChosenFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private String city;

    private FloatingActionButton fab1;
    private FloatingActionButton fab2;
    private TextView labelfab2;
    private LinearLayout layout_fab2;
    public CityChosenFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @return A new instance of fragment CityChosenFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CityChosenFragment newInstance(String param1) {
        CityChosenFragment fragment = new CityChosenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            city = mParam1;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_city_chosen, container, false);

        final Animation show_fab_1 = AnimationUtils.loadAnimation(getContext(), R.anim.scale_up);
        final Animation hide_fab_1 = AnimationUtils.loadAnimation(getContext(), R.anim.scale_down);

        fab1 = (FloatingActionButton) v.findViewById(R.id.floatingActionButton2);
        layout_fab2 = (LinearLayout) v.findViewById(R.id.layout_fab2);
        fab2 = (FloatingActionButton) v.findViewById(R.id.floatingActionButton3);
        labelfab2 = (TextView) v.findViewById(R.id.labelfab2);

        fab1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("CityChosen", "Fab clicked");

                if (fab2.getVisibility() == View.VISIBLE){
                    Log.d("CityChosen", "Layout visible");
                    fab2.startAnimation(hide_fab_1);
                    labelfab2.startAnimation(hide_fab_1);
                    labelfab2.setVisibility(View.INVISIBLE);
                    fab2.setVisibility(View.INVISIBLE);
                } else {
                    Log.d("CityChosen", "Layout invisible");
                    fab2.startAnimation(show_fab_1);
                    labelfab2.startAnimation(show_fab_1);
                    //fab2.startAnimation(show_fab_1);
                    //fab1.setBackgroundTintList(ColorStateList.valueOf(Color.GRAY));
                    labelfab2.setVisibility(View.VISIBLE);
                    fab2.setVisibility(View.VISIBLE);
                }
            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
