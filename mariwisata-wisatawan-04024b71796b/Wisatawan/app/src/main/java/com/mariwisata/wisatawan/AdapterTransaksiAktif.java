package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 19/02/2018.
 */

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Cerwyn on 09/02/2018.
 */

public class AdapterTransaksiAktif extends BaseAdapter {


    private static final String TAG = CustomAdapterWisataB.class.getSimpleName();
    ArrayList<DataModel> listArray;

    //testcase1
    Boolean saved=null;
    private String foto_pemandu;
    private ImageView gbrprofil;
    private StorageReference mStorageRef;
    private FirebaseStorage storage;
    private int index2;
    private String alamat2;
    private DatabaseReference db;
    private TextView tanggal_transaksi;

    public AdapterTransaksiAktif(ArrayList<DataModel> listArray) {

        this.listArray = listArray;
        Log.d(TAG, "int: " + listArray.toString());

    }


    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }

    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.single_transaksi, parent, false);
        }

        final DataModel dataModel = listArray.get(index);

        TextView status_transaksi = (TextView) view.findViewById(R.id.status_transaksi);
        TextView destinasi_transaksi = (TextView) view.findViewById(R.id.destinasi_transaksi);
        TextView kota_transaksi = (TextView) view.findViewById(R.id.kota_transaksi);
        tanggal_transaksi = (TextView) view.findViewById(R.id.tanggal_transaksi);

        destinasi_transaksi.setText(dataModel.getDestinasi_aktif());
        kota_transaksi.setText(dataModel.getKota_aktif());
        tanggal_transaksi.setText("Tanggal");
        status_transaksi.setText("Aktif");

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            Log.d("TransaksiAktif", "view:"+dataModel.getTransaksidari_aktif());
                CutomModelTransaksi.getInstance().changeState(true, dataModel.getDestinasi_aktif(), dataModel.getKota_aktif(), dataModel.getTransaksidari_aktif());
            }
        });

        return view;
    }

}