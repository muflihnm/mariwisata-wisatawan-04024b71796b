package com.mariwisata.wisatawan;

import android.app.AlertDialog;
import android.app.Dialog;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.Button;

public class SyaratFragment extends android.app.DialogFragment {

    private Button btn_setuju;

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        View v = getActivity().getLayoutInflater().inflate(R.layout.fragment_syarat, null, false);

        setCancelable(false);

        btn_setuju = (Button) v.findViewById(R.id.setuju_btn);

        btn_setuju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });


        builder.setView(v);
        return builder.create();
    }
}