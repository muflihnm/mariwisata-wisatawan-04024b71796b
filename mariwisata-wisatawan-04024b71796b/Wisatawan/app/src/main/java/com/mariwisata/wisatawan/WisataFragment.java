package com.mariwisata.wisatawan;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.zip.Inflater;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WisataFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WisataFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WisataFragment extends Fragment {
    String namesList[] = {"Ehsan", "Rafique", "Qasim", "Doctor Qaleem"};
    ListView lvSimpleListView;
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private Fragment fragment;
    private OnFragmentInteractionListener mListener;
    private CustomAdapter adapter;
    ArrayList<DataModel> listArray;
    DatabaseReference db;
    private String destinasi_kota;
    private TextView lihlmission;

    private ProgressBar pb;
    public WisataFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WisataFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static WisataFragment newInstance(String param1) {
        WisataFragment fragment = new WisataFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        //args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            destinasi_kota = mParam1;
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        //testcase 1
/*        View view = inflater.inflate(R.layout.fragment_wisata,container,false);
        lvSimpleListView = (ListView) view.findViewById(R.id.list_wisata);
        ArrayAdapter adapter = new ArrayAdapter(getActivity(),android.R.layout.simple_list_item_1,namesList);
        lvSimpleListView.setAdapter(adapter);
        return view;*/

        //testcase 2
        View view = inflater.inflate(R.layout.fragment_wisata,container,false);
        lvSimpleListView = (ListView) view.findViewById(R.id.list_wisata);

        lihlmission = (TextView) view.findViewById(R.id.lihlmission);
        lihlmission.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment2 = new ViralFragment().newInstance("","");
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        pb = (ProgressBar) view.findViewById(R.id.pb2);
        pb.setVisibility(View.VISIBLE);


        listArray = new ArrayList<DataModel>();
        db= FirebaseDatabase.getInstance().getReference("jadwal").child(destinasi_kota);
        //addValueEventListener
        //addListenerForSingleValueEvent == sekali aja dipanggil mungkin
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
               /* String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);*/
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    String destinasi = postSnapshot.getKey();
                    DataSnapshot child2 = dataSnapshot.child(destinasi);

/*                    String harga_termurah = "Mulai Dari "+ child2.child("Harga Termurah").getValue(String.class);
                    String kategori = "kategori: "+ child2.child("Kategori").getValue(String.class);
                    String tanggal = "tanggal: "+child2.child("Tanggal").getValue(String.class);*/
                    String foto = child2.child("Foto").getValue(String.class);
                    Log.e(TAG, "harga======="+foto);
/*                    String lokasi = postSnapshot.child("Yogyakarta").getValue(String.class);
                    String fasilitas = postSnapshot.child("fasilitas").getValue(String.class);
                    String catatan = postSnapshot.child("catatan").getValue(String.class);*/
                    //listArray.add(new DataModel("fasilitas", 1220, 2.38, "catatan"));
                    listArray.add(new DataModel(destinasi_kota, destinasi, foto));
                }
                //fragment = new WisataFragment_B();

                adapter = new CustomAdapter(listArray);
                lvSimpleListView.setAdapter(adapter);
                pb.setVisibility(View.INVISIBLE);

            }


            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });

        Log.w(TAG, "List Array");
        CustomAdapter adapter = new CustomAdapter(listArray);
        lvSimpleListView.setAdapter(adapter);
        return view;

        //default
        //return inflater.inflate(R.layout.fragment_wisata, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
