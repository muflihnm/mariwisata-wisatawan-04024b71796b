package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 02/01/2018.
 */

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: Shahab
 * Date: 8/22/12
 * Time: 11:37 AM
 * To change this template use File | Settings | File Templates.
 */

public class CustomAdapterHome extends BaseAdapter {


    private static final String TAG = CustomAdapterHome.class.getSimpleName();
    ArrayList<DataModel> listArray;

    //testcase1
    DatabaseReference db;

    public CustomAdapterHome(ArrayList<DataModel> listArray) {

        this.listArray = listArray;
        Log.d(TAG, "int: " + listArray.toString());

    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    public void updateIfPresent(int index){
/*        DataModel dm_temp = new DataModel();
        listArray.set(index, dm_temp);*/
        listArray.get(1).setTanggal("Tanggal yang ke set yuhu");

        notifyDataSetChanged();
    }

    public void add(){
/*        listArray.add(listArray.get(1));
        DataModel dm = new DataModel();
        dm = listArray.get(1);
        notifyDataSetChanged();*/
    }
    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }

    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.single_home, parent, false);
        }

        final DataModel dataModel = listArray.get(index);

        TextView destinasi = (TextView) view.findViewById(R.id.destinasi_home);
        TextView city = (TextView) view.findViewById(R.id.city_home);
        ImageView referensi = (ImageView) view.findViewById(R.id.referensi_home);
        ImageView image = (ImageView) view.findViewById(R.id.image_home);
        TextView harga = (TextView) view.findViewById(R.id.harga_home);

        destinasi.setText(dataModel.getDestinasi_home());
        city.setText("on "+dataModel.getKota_home());

/*        int i = 3/3;
        int j = 4/3;
        int k = 2/3;
        Log.d("TEST", "i:"+i+" J:"+j+" K:"+k);*/ // 1, 1, 0

        String harga_format = dataModel.getHarga_home();
        int i = harga_format.length();
        Log.d("HARGA", "harga_format"+harga_format+" hargaformatlength"+i+" i-3:");
        while((i-3) > 0){
            int j = i-3;
            harga_format = harga_format.substring(0, j)+"."+harga_format.substring(j, harga_format.length());
            i = i - 3;
            if (i == 3)
                break;
        }

        harga.setText("Start From IDR "+harga_format);

        Picasso.with(parent.getContext())
                .load(dataModel.getImage_home())
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.loading)      // optional
                .fit()
                .centerInside()
                .into(image);
        Log.d("AdapterHome", "Transaksidari"+dataModel.getTransaksi_darihome());
        referensi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomModelHome.getInstance().changeStateHomeRef(true, dataModel.getKota_home(), dataModel.getDestinasi_home(), dataModel.getImage_home());
            }
        });

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CustomModelHome2DestinationChosen.getInstance().changeStateHome2Dest(true, dataModel.getKota_home(), dataModel.getDestinasi_home());
            }
        });

        return view;
    }

}