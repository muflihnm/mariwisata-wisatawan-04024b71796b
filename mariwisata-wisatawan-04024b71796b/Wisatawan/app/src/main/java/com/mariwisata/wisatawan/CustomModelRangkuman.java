package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 14/01/2018.
 */

public class CustomModelRangkuman {
    public interface OnCustomStateListener {
        void stateChangedPemandu(String kota, String destinasi, String pemandu);
    }

    private static CustomModelRangkuman mInstance;
    private OnCustomStateListener mListener;
    private boolean mState;

    private CustomModelRangkuman() {}

    public static CustomModelRangkuman getInstance() {
        if(mInstance == null) {
            mInstance = new CustomModelRangkuman();
        }
        return mInstance;
    }

    public void setListener(OnCustomStateListener listener) {
        mListener = listener;
    }

    public void changeState(boolean state, String kota, String destinasi, String pemandu) {
        if(mListener != null) {
            mState = state;
            notifyStateChange(kota, destinasi, pemandu);
        }
    }

    public boolean getState() {
        return mState;
    }

    private void notifyStateChange(String kota, String destinasi, String pemandu) {
        mListener.stateChangedPemandu(kota, destinasi, pemandu);
    }
}
