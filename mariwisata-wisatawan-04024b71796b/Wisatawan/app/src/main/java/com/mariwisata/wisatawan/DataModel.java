package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 02/01/2018.
 */

import java.util.HashMap;

/**
 * Created with IntelliJ IDEA.
 * User: Shahab
 * Date: 8/22/12
 * Time: 11:38 AM
 * To change this template use File | Settings | File Templates.
 */
public class DataModel {
    private String name;
    private int anInt;
    private double aDouble;
    private String OtherData;

    private String destinasi;
    private String harga_termurah;
    private String kategori;
    private String tanggal;
    private String foto;
    private String kota;

    //wisata_fragment_b
    private String id_pemandu;
    private String nama;
    private String tanggal_b;
    private String jam_mulai;
    private String jam_selesai;
    private String harga;
    private String ruangan_sisa;
    private String fasilitas;
    private String foto_pemandu;
    private String transaksiid;
    private String kotaB;
    private String destinasiB;

    //referensi
    private String foto_ref;
    private String destinasi_ref;
    private String deskripsi_ref;
    private String kota_ref;

    //Group Chat
    private String nama_grup;
    private String key;

    //lmission
    private String foto_viral;
    private String deskripsi_viral;
    private String waktu_viral;
    private String creator_viral;
    private String total_cares_viral;
    private String key_viral;
    private String kota_viral;
    private String destinasi_viral;

    //Histori transaksi
    private String destinasi_transaksi;
    private String kota_transaksi;
    private String key_transaksi;
    private String status_transaksi;
    private int status_transaksi2;

    //Transaksi Aktif
    private String destinasi_aktif;
    private String kota_aktif;
    private String transaksidari_aktif;
    private String batas_pembayaran_aktif;
    private String status_aktif;

    //Home Fragment
    private String kota_home;
    private String destinasi_home;
    private String image_home;
    private String harga_home;
    private String jenis_home;
    private String tanggal_home;
    private String transaksi_darihome;

    //destinationchosen kota_param, destinasi_param, key, nama_pemandu, fasilitas, pj, tanggal, harga_total, foto_profil, harga_map
    private String kota_destinatioc;
    private String destinasi_destinationc;
    private String key_destinationc;
    private String nama_destinationc;
    private String fasilitas_destinationc;
    private String pj_destinationc;
    private String tanggal_destinationc;
    private String hargatot_destinationc;
    private String foto_destinationc;
    private HashMap<String, String> harga_map=new HashMap<String, String>();

    //lmission destination chosen
    private String l_kota;
    private String l_key;
    private String l_destinasi;
    private String l_deskripsi;
    private String l_foto;
    private String l_tagar;


    public DataModel(String name, int anInt, double aDouble, String otherData) {
        this.name = name;
        this.anInt = anInt;
        this.aDouble = aDouble;
        OtherData = otherData;
    }

    public DataModel(String destinasi, String harga_termurah, String kategori, String tanggal) {
        this.destinasi = destinasi;
        this.harga_termurah = harga_termurah;
        this.kategori = kategori;
        this.tanggal = tanggal;
    }

    public DataModel(String kota, String destinasi, String foto) {
        this.destinasi = destinasi;
        this.harga_termurah = harga_termurah;
        this.kategori = kategori;
        this.tanggal = tanggal;
        this.foto = foto;
        this.kota = kota;
    }

    public DataModel(String kotaB, String destinasiB, String id_pemandu, String nama, String tanggal_b, String jam_mulai, String jam_selesai, String harga, String ruangan_sisa, String fasilitas, String foto_pemandu, String transaksiid){
        this.id_pemandu = id_pemandu;
        this.nama = nama;
        this.tanggal_b = tanggal_b;
        this.jam_mulai = jam_mulai;
        this.jam_selesai = jam_selesai;
        this.harga = harga;
        this.ruangan_sisa = ruangan_sisa;
        this.fasilitas = fasilitas;
        this.foto_pemandu = foto_pemandu;
        this.transaksiid = transaksiid;
        this.kotaB = kotaB;
        this.destinasiB = destinasiB;
    }


    public DataModel(String destinasi_ref, String kota_ref, String foto_ref, String deskripsi_ref, int i){
        this.destinasi_ref = destinasi_ref;
        this.kota_ref = kota_ref;
        this.foto_ref = foto_ref;
        this.deskripsi_ref = deskripsi_ref;
    }

    public DataModel(String nama_grup, String key){
        this.nama_grup = nama_grup;
        this.key = key;
    }

    public DataModel(String foto, String deskripsi, String waktu, String creator, String total_cares, String key, String kota, String destinasi){
        this.foto_viral = foto;
        this.deskripsi_viral = deskripsi;
        this.waktu_viral = waktu;
        this.creator_viral = creator;
        this.total_cares_viral = total_cares;
        this.key_viral = key;
        this.kota_viral = kota;
        this.destinasi_viral = destinasi;
    }

    public DataModel(String destinasi_transaksi, String kota_transaksi, String key_transaksi, String status_transaksi, int status_transaksi2, int i_transaksi){
        this.destinasi_transaksi = destinasi_transaksi;
        this.kota_transaksi = kota_transaksi;
        this.key_transaksi = key_transaksi;
        this.status_transaksi = status_transaksi;
        this.status_transaksi2 = status_transaksi2;
    }

    public DataModel(String destinasi_aktif, String kota_aktif, String transaksidari_aktif, String batas_pembayaran_aktif, String status_aktif, int i){
        this.destinasi_aktif = destinasi_aktif;
        this.kota_aktif = kota_aktif;
        this.transaksidari_aktif = transaksidari_aktif;
        this.batas_pembayaran_aktif = batas_pembayaran_aktif;
        this.status_aktif = status_aktif;
    }

    public DataModel(String kota_home, String destinasi_home, String image_home, String harga_home, String jenis_home, String tanggal_home, String transaksi_darihome){
        this.kota_home = kota_home;
        this.destinasi_home = destinasi_home;
        this.image_home = image_home;
        this.harga_home = harga_home;
        this.jenis_home = jenis_home;
        this.tanggal_home = tanggal_home;
        this.transaksi_darihome = transaksi_darihome;
    }
    //destinationchosen kota_param, destinasi_param, key, nama_pemandu, fasilitas, pj, tanggal, harga_total, foto_profil, harga_map
    public DataModel(String kota_destinatioc, String destinasi_destinationc, String key_destinationc, String nama_destinationc, String fasilitas_destinationc, String pj_destinationc, String tanggal_destinationc, String hargatot_destinationc, String foto_destinationc, HashMap<String, String> harga_map){
        this.kota_destinatioc = kota_destinatioc;
        this.destinasi_destinationc = destinasi_destinationc;
        this.key_destinationc = key_destinationc;
        this.nama_destinationc = nama_destinationc;
        this.fasilitas_destinationc = fasilitas_destinationc;
        this.pj_destinationc = pj_destinationc;
        this.tanggal_destinationc = tanggal_destinationc;
        this.hargatot_destinationc = hargatot_destinationc;
        this.foto_destinationc = foto_destinationc;
        this.harga_map = harga_map;
    }

    public DataModel(String l_kota, String l_key, String l_destinasi, String l_deskripsi, String l_foto, String l_tagar){
        this.l_kota = l_kota;
        this.l_key = l_key;
        this.l_destinasi = l_destinasi;
        this.l_deskripsi = l_deskripsi;
        this.l_foto = l_foto;
        this.l_tagar = l_tagar;

    }
    public String getL_kota() {
        return l_kota;
    }

    public void setL_kota(String l_kota) {
        this.l_kota = l_kota;
    }

    public String getL_key() {
        return l_key;
    }

    public void setL_key(String l_key) {
        this.l_key = l_key;
    }

    public String getL_destinasi() {
        return l_destinasi;
    }

    public void setL_destinasi(String l_destinasi) {
        this.l_destinasi = l_destinasi;
    }

    public String getL_deskripsi() {
        return l_deskripsi;
    }

    public void setL_deskripsi(String l_deskripsi) {
        this.l_deskripsi = l_deskripsi;
    }

    public String getL_foto() {
        return l_foto;
    }

    public void setL_foto(String l_foto) {
        this.l_foto = l_foto;
    }

    public String getL_tagar() {
        return l_tagar;
    }

    public void setL_tagar(String l_tagar) {
        this.l_tagar = l_tagar;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAnInt() {
        return anInt;
    }

    public void setAnInt(int anInt) {
        this.anInt = anInt;
    }

    public double getaDouble() {
        return aDouble;
    }

    public void setaDouble(double aDouble) {
        this.aDouble = aDouble;
    }

    public String getOtherData() {
        return OtherData;
    }

    public void setOtherData(String otherData) {
        OtherData = otherData;
    }

    public String getDestinasi(){return destinasi;}

    public void setDestinasi(String destinasi){this.destinasi = destinasi;}

    public String getKota(){return kota;}

    public void setKota(String kota){this.kota = kota;}

    public String getHarga_termurah(){return harga_termurah;};

    public void setHarga_termurah(String harga_termurah){this.harga_termurah = harga_termurah;}

    public String getKategori(){return kategori;}

    public void setKategori(String kategori){this.kategori = kategori;}

    public String getTanggal(){return  tanggal;}

    public void setTanggal(String tanggal){this.tanggal = tanggal;}

    public String getFoto(){return foto;}

    public void setFoto(String foto){this.foto = foto;};

    public String getId_pemandu() {
        return id_pemandu;
    }

    public void setId_pemandu(String id_pemandu) {
        this.id_pemandu = id_pemandu;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getTanggal_b() {
        return tanggal_b;
    }

    public void setTanggal_b(String tanggal_b) {
        this.tanggal_b = tanggal_b;
    }

    public String getJam_mulai() {
        return jam_mulai;
    }

    public void setJam_mulai(String jam_mulai) {
        this.jam_mulai = jam_mulai;
    }

    public String getJam_selesai() {
        return jam_selesai;
    }

    public void setJam_selesai(String jam_selesai) {
        this.jam_selesai = jam_selesai;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getRuangan_sisa() {
        return ruangan_sisa;
    }

    public void setRuangan_sisa(String ruangan_sisa) {
        this.ruangan_sisa = ruangan_sisa;
    }

    public String getFasilitas() {
        return fasilitas;
    }

    public void setFasilitas(String fasilitas) {
        this.fasilitas = fasilitas;
    }

    public String getFoto_pemandu() {
        return foto_pemandu;
    }

    public void setFoto_pemandu(String foto_pemandu) {
        this.foto_pemandu = foto_pemandu;
    }

    public String getTransaksiid(){return transaksiid;}

    public void setTransaksiid(String transaksiid){this.transaksiid = transaksiid;}

    public String getKotaB(){return kotaB;}

    public void setKotaB(String kotaB){this.kotaB = kotaB;}

    public String getDestinasiB(){return destinasiB;}

    public void setDestinasiB(String destinasiB){this.destinasiB = destinasiB;}

    public String getFoto_ref(){return foto_ref;}

    public void setFoto_ref(String foto_ref ){this.foto_ref = foto_ref;}

    public String getDestinasi_ref(){return destinasi_ref;}

    public void setDestinasi_ref(String destinasi_ref){this.destinasi_ref = destinasi_ref;}

    public String getDeskripsi_ref(){return deskripsi_ref;}

    public void setDeskripsi_ref(String deskripsi_ref){this.deskripsi_ref = deskripsi_ref;}

    public String getKota_ref(){return kota_ref;}

    public void setKota_ref(String kota_ref){this.kota_ref = kota_ref;}

    public String getNama_grup(){ return nama_grup;}

    public void setNama_grup(String nama_grup){this.nama_grup = nama_grup;}

    public String getKey(){return key;}

    public void setKey(String key){this.key = key;}

    public String getFoto_viral(){return foto_viral;}

    public void setFoto_viral(String foto_viral){this.foto_viral = foto_viral;}

    public String getDeskripsi_viral(){return deskripsi_viral;}

    public void setDeskripsi_viral(String deskripsi_viral){this.deskripsi_viral = deskripsi_viral;}

    public String getWaktu_viral(){return waktu_viral;}

    public void setWaktu_viral(String waktu_viral){this.waktu_viral = waktu_viral;}

    public String getCreator_viral(){return creator_viral;}

    public void setCreator_viral(String creator_viral){this.creator_viral = creator_viral;}

    public String getTotal_cares_viral(){return total_cares_viral;}

    public void setTotal_cares_viral(String total_cares_viral){this.total_cares_viral = total_cares_viral;}

    public String getKey_viral(){return key_viral;}

    public void setKey_viral(String key_viral){this.key_viral = key_viral;}

    public String getKota_viral(){return kota_viral;}

    public void setKota_viral(String kota_viral){this.kota_viral = kota_viral;}

    public String getDestinasi_viral(){return destinasi_viral;}

    public void setDestinasi_viral(String destinasi_viral){this.destinasi_viral = destinasi_viral;}

    public String getDestinasi_transaksi(){return destinasi_transaksi;}

    public void setDestinasi_transaksi(String destinasi_transaksi){this.destinasi_transaksi = destinasi_transaksi;}

    public String getKota_transaksi(){return kota_transaksi;}

    public void setKota_transaksi(String kota_transaksi){this.kota_transaksi = kota_transaksi;}

    public String getKey_transaksi(){return key_transaksi;}

    public void setKey_transaksi(String key_transaksi){this.key_transaksi = key_transaksi;}

    public String getStatus_transaksi(){return status_transaksi;}

    public void setStatus_transaksi(String status_transaksi){this.status_transaksi = status_transaksi;}

    public int getStatus_transaksi2(){return status_transaksi2;}

    public void setStatus_transaksi2(int status_transaksi2){this.status_transaksi2 = status_transaksi2;}

    public String getDestinasi_aktif(){return destinasi_aktif;}

    public void setDestinasi_aktif(String destinasi_aktif){this.destinasi_aktif = destinasi_aktif;}

    public String getKota_aktif(){return kota_aktif;}

    public void setKota_aktif(String kota_aktif){this.kota_aktif = kota_aktif;}

    public String getTransaksidari_aktif(){return transaksidari_aktif;}

    public void setTransaksidari_aktif(String transaksidari_aktif){this.transaksidari_aktif = transaksidari_aktif;}

    public String getBatas_pembayaran_aktif(){return batas_pembayaran_aktif;}

    public void setBatas_pembayaran_aktif(String batas_pembayaran_aktif){this.batas_pembayaran_aktif = batas_pembayaran_aktif;}

    public String getStatus_aktif(){return status_aktif;}

    public void setStatus_aktif(String status_aktif){this.status_aktif = status_aktif;}

    public String getKota_home(){return kota_home;}

    public void setKota_home(String kota_home){this.kota_home = kota_home;}

    public String getDestinasi_home(){return destinasi_home;}

    public void setDestinasi_home(String destinasi_home){this.destinasi_home = destinasi_home;}

    public String getImage_home(){return image_home;}

    public void setImage_home(String image_home){this.image_home = image_home;}

    public String getHarga_home(){return harga_home;}

    public void setHarga_home(String harga_home){this.harga_home = harga_home;}

    public String getJenis_home(){return jenis_home;}

    public void setJenis_home(String jenis_home){this.jenis_home = jenis_home;}

    public String getTanggal_home(){return tanggal_home;}

    public void setTanggal_home(String tanggal_home){this.tanggal_home = tanggal_home;}

    public String getTransaksi_darihome(){ return transaksi_darihome;}

    public void setTransaksi_darihome(String transaksi_darihome){this.transaksi_darihome = transaksi_darihome;}

    public String getKota_destinatioc() {
        return kota_destinatioc;
    }

    public void setKota_destinatioc(String kota_destinatioc) {
        this.kota_destinatioc = kota_destinatioc;
    }

    public String getDestinasi_destinationc() {
        return destinasi_destinationc;
    }

    public void setDestinasi_destinationc(String destinasi_destinationc) {
        this.destinasi_destinationc = destinasi_destinationc;
    }

    public String getKey_destinationc() {
        return key_destinationc;
    }

    public void setKey_destinationc(String key_destinationc) {
        this.key_destinationc = key_destinationc;
    }

    public String getNama_destinationc() {
        return nama_destinationc;
    }

    public void setNama_destinationc(String nama_destinationc) {
        this.nama_destinationc = nama_destinationc;
    }

    public String getFasilitas_destinationc() {
        return fasilitas_destinationc;
    }

    public void setFasilitas_destinationc(String fasilitas_destinationc) {
        this.fasilitas_destinationc = fasilitas_destinationc;
    }

    public String getPj_destinationc() {
        return pj_destinationc;
    }

    public void setPj_destinationc(String pj_destinationc) {
        this.pj_destinationc = pj_destinationc;
    }

    public String getTanggal_destinationc() {
        return tanggal_destinationc;
    }

    public void setTanggal_destinationc(String tanggal_destinationc) {
        this.tanggal_destinationc = tanggal_destinationc;
    }

    public String getHargatot_destinationc() {
        return hargatot_destinationc;
    }

    public void setHargatot_destinationc(String hargatot_destinationc) {
        this.hargatot_destinationc = hargatot_destinationc;
    }

    public String getFoto_destinationc() {
        return foto_destinationc;
    }

    public void setFoto_destinationc(String foto_destinationc) {
        this.foto_destinationc = foto_destinationc;
    }

    public HashMap<String, String> getHarga_map() {
        return harga_map;
    }

    public void setHarga_map(HashMap<String, String> harga_map) {
        this.harga_map = harga_map;
    }
}