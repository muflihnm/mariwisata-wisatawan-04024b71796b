package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 09/03/2018.
 */

public class CustomModelHome {
    public interface OnCustomStateListener {
        void stateChangedHomeRef(String kota, String destinasi, String foto);
    }

    private static CustomModelHome mInstance;
    private OnCustomStateListener mListener;
    private boolean mState;

    private CustomModelHome() {}

    public static CustomModelHome getInstance() {
        if(mInstance == null) {
            mInstance = new CustomModelHome();
        }
        return mInstance;
    }

    public void setListener(OnCustomStateListener listener) {
        mListener = listener;
    }

    public void changeStateHomeRef(boolean state, String kota, String destinasi, String foto) {
        if(mListener != null) {
            mState = state;
            notifyStateChange(kota, destinasi, foto);
        }
    }

    public boolean getState() {
        return mState;
    }

    private void notifyStateChange(String kota, String destinasi, String foto) {
        mListener.stateChangedHomeRef(kota, destinasi, foto);
    }
}
