package com.mariwisata.wisatawan;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DaftarFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DaftarFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DaftarFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    FirebaseAuth mAuth;
    private EditText nama_profil;
    private EditText email_daftar;
    private EditText no_hp;
    private EditText password1;
    private EditText password2;
    private EditText nama_lengkap;
    private Button btn_otp;
    private EditText sms_otp;
    private Button verif;
    private OnFragmentInteractionListener mListener;
    DatabaseReference mDatabase;
    private FirebaseUser user;
    private String no_hp_replace;
    FirebaseAuth.AuthStateListener mAuthListener;
    public DaftarFragment(){
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DaftarFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DaftarFragment newInstance(String param1, String param2) {
        DaftarFragment fragment = new DaftarFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        final View v = inflater.inflate(R.layout.fragment_daftar, container, false);
        mAuth = FirebaseAuth.getInstance();
        nama_lengkap = (EditText) v.findViewById(R.id.nama_lengkap);
        nama_profil = (EditText) v.findViewById(R.id.nama_profil);
        email_daftar = (EditText) v.findViewById(R.id.email_daftar);
        no_hp = (EditText) v.findViewById(R.id.no_hp);
        password1 = (EditText) v.findViewById(R.id.password_daftar);
        password2 = (EditText) v.findViewById(R.id.password_daftar2);
        btn_otp = (Button) v.findViewById(R.id.btn_otp);
         sms_otp = (EditText) v.findViewById(R.id.sms_otp);
        verif = (Button) v.findViewById(R.id.verifikasi);
        Button daftar = (Button) v.findViewById(R.id.daftar);
        daftar.setEnabled(false);
        btn_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (no_hp.getText().toString().indexOf('+') == -1){
                    Toast.makeText(getActivity(), "Format NO HP Salah, gunakan +62", Toast.LENGTH_SHORT).show();
                }else{
                    no_hp_replace = no_hp.getText().toString().replace('+','B');
                no_hp_replace = "%2"+no_hp_replace;
                Log.d("DAFTAR", "sTRING"+no_hp_replace);
                new OtpTask(no_hp_replace, getActivity(), v).execute();
                }
            }
        });

        verif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new OtpVerif(no_hp.getText().toString(),sms_otp.getText().toString(), getActivity(), v).execute();
            }
        });

        daftar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {

                final String email = email_daftar.getText().toString();
                final String password = password1.getText().toString();
                final String password_2 = password2.getText().toString();

                if (nama_lengkap.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "Nama lengkap harus valid", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (nama_profil.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "Nama profil harus valid", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!isEmailValid(email)){
                    Toast.makeText(getActivity(), "Email tidak valid", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (password.length() < 6 ){
                    Toast.makeText(getActivity(), "Password minimal 6 karakter", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (no_hp.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), "No HP harus valid", Toast.LENGTH_SHORT).show();
                    return;
                }
                if (!password.equals(password_2)){
                    Toast.makeText(getActivity(), "Password tidak sama", Toast.LENGTH_SHORT).show();
                    return;
                }
                mAuth.createUserWithEmailAndPassword(email,password).addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if(!task.isSuccessful())
                        {
                            Log.d("Gagal Daftar", "Gagal Daftar");
                            Toast.makeText(getActivity(), "Email sudah terdaftar", Toast.LENGTH_SHORT).show();

                        }
                        else
                        {
                            String user_id = mAuth.getCurrentUser().getUid();

                            mDatabase = FirebaseDatabase.getInstance().getReference().child("Users").child("Customers").child(user_id);
                            mDatabase.child("Name").setValue(nama_lengkap.getText().toString());
                            mDatabase.child("Phone").setValue(no_hp.getText().toString());
                            mDatabase.child("Email").setValue(email_daftar.getText().toString());
                            mDatabase.child("Nama Profil").setValue(nama_profil.getText().toString());

                            Toast.makeText(getActivity(), "Daftar berhasil", Toast.LENGTH_SHORT).show();

                            user = FirebaseAuth.getInstance().getCurrentUser();

                            user.sendEmailVerification()
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            if (task.isSuccessful()) {
                                                Log.d("DaftarFragment", "Email Verifikasi terkirim" +user.getEmail());
                                                Toast.makeText(getActivity(), "Email Verifikasi terkirim ke "+user.getEmail(), Toast.LENGTH_SHORT).show();
                                            } else {
                                                Log.d("DaftarFragment", "Email Verifikasi gagal" +task.getException() +user.getEmail());
                                                Toast.makeText(getActivity(), "Email Verifikasi tidak terkirim "+user.getEmail(), Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    });
                            Log.d("DAFTAR", "STRING Notif Task"+no_hp_replace);
                            new NotifTask(no_hp_replace).execute();
                            Intent intent = new Intent(getActivity(), MainActivity.class);
                            startActivity(intent);
                           // mDatabase.child("Password").setValue(password1.getText().toString());
                        }
                    }
                });



            }
        });


        return v;
    }


    public boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches())
            return true;
        else
            return false;
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
