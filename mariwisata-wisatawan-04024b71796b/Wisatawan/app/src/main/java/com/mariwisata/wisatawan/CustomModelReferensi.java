package com.mariwisata.wisatawan;

import android.util.Log;

/**
 * Created by Cerwyn on 16/01/2018.
 */

public class CustomModelReferensi {
    public interface OnCustomStateListener {
        void stateChangedReferensi(String destinasi, String kota, String deskripsi, String foto);
    }

    private static CustomModelReferensi mInstance;
    private OnCustomStateListener mListener;
    private boolean mState;

    private CustomModelReferensi() {}

    public static CustomModelReferensi getInstance() {
        if(mInstance == null) {
            mInstance = new CustomModelReferensi();
        }
        return mInstance;
    }

    public void setListener(OnCustomStateListener listener) {
        mListener = listener;
    }

    public void changeState(boolean state, String destinasi, String kota, String deskripsi, String foto) {
        if(mListener != null) {
            mState = state;
            Log.d("ChangeState", "True");
            notifyStateChange(destinasi, kota, deskripsi, foto);
        }
    }

    public boolean getState() {
        return mState;
    }

    private void notifyStateChange(String destinasi, String kota, String deskripsi, String foto) {
        Log.d("Notify", "True");
        mListener.stateChangedReferensi(destinasi, kota, deskripsi, foto);
    }
}
