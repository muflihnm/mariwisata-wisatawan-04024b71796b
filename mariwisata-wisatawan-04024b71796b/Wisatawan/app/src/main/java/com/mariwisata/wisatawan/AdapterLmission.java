package com.mariwisata.wisatawan;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Cerwyn on 05/02/2018.
 */

public class AdapterLmission extends BaseAdapter {

    private static final String TAG = CustomAdapterWisataB.class.getSimpleName();
    ArrayList<DataModel> listArray;
    //testcase1
    DatabaseReference db;
    Boolean saved=null;
    private String foto_ref;
    private ImageView gbrprofil;
    private StorageReference mStorageRef;
    private FirebaseStorage storage;
    private int index2;
    private String alamat2;

    //lmission
    private String foto_viral;
    private String deskripsi_viral;
    private String waktu_viral;
    private String creator_viral;
    private String total_cares_viral;
    private String key_viral;
    private String kota_viral;
    private TextView text_tagar;

    public AdapterLmission(ArrayList<DataModel> listArray) {

        this.listArray = listArray;
        Log.d(TAG, "int: " + listArray.toString());

    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }

    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.grid_single_viral, parent, false);
        }
        final DataModel dataModel = listArray.get(index);

        ImageView gambar_ref = (ImageView) view.findViewById(R.id.imageView1_viral);
        text_tagar = (TextView) view.findViewById(R.id.text_tagar);

        String l_foto = dataModel.getL_foto();
        String l_tagar = dataModel.getL_tagar();
        Log.d("LMIS", " kota, key, destinasi "+dataModel.getL_kota()+dataModel.getL_key()+dataModel.getL_destinasi());
        text_tagar.setText("#"+l_tagar);

        Picasso.with(parent.getContext())
                .load(l_foto)
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.loading)      // optional
                .fit()
                .centerInside()
                .into(gambar_ref);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //tagar kota key destinasi deskripsi foto
/*                Log.d("ADAPTERLMISSION", "CLICKED Data:"+dataModel.getFoto_viral()+dataModel.getDeskripsi_viral()+dataModel.getWaktu_viral()+dataModel.getCreator_viral()+dataModel.getTotal_cares_viral()+dataModel.getKey_viral()+dataModel.getKota_viral()+dataModel.getDestinasi_viral());
                CustomModelLmission.getInstance().changeStateLmission(true, dataModel.getFoto_viral(), dataModel.getDeskripsi_viral(), dataModel.getWaktu_viral(), dataModel.getCreator_viral(), dataModel.getTotal_cares_viral(), dataModel.getKey_viral(), dataModel.getKota_viral(), dataModel.getDestinasi_viral());
                //Log.d("Deskripsi", deskripsi_ref);*/
                CustomModelLmission.getInstance().changeStateLmission(true, dataModel.getL_tagar(), dataModel.getL_kota(), dataModel.getL_key(), dataModel.getL_destinasi(), dataModel.getL_deskripsi(), dataModel.getL_foto());
            }
        });

        return view;
    }
}
