package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 02/01/2018.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static android.R.attr.fragment;

/**
 * Created with IntelliJ IDEA.
 * User: Shahab
 * Date: 8/22/12
 * Time: 11:37 AM
 * To change this template use File | Settings | File Templates.
 */

public class CustomAdapterWisataB extends BaseAdapter {


    private static final String TAG = CustomAdapterWisataB.class.getSimpleName();
    ArrayList<DataModel> listArray;

    //testcase1
    DatabaseReference db;
    Boolean saved=null;
    private String foto_pemandu;
    private ImageView gbrprofil;
    private StorageReference mStorageRef;
    private FirebaseStorage storage;
    private int index2;
    private String alamat2;

    public CustomAdapterWisataB(ArrayList<DataModel> listArray) {

        this.listArray = listArray;
        Log.d(TAG, "int: " + listArray.toString());

    }

    public void updateGbr(int index, String alamat){
/*        DataModel dm_temp = new DataModel();
        listArray.set(index, dm_temp);*/
/*        this.index2 = index;
        this.alamat2 = alamat;
        Log.e("Tuts+", "uri1: " + listArray.size() + "index" + index2 );
        storage = FirebaseStorage.getInstance();
        mStorageRef = storage.getReferenceFromUrl("gs://mariwisata-7dca3.appspot.com").child("Pemandu").child("ida").child("pasfoto.jpg");
        mStorageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
            @Override
            public void onSuccess(Uri uri) {
                Log.e("Tuts+", "uri2: " + listArray.size() + "index" + index2 );

                listArray.get(index2).setFoto_pemandu(alamat2);
                notifyDataSetChanged();

                String s = uri.toString();
                Log.e("Tuts+", "uri3: " + listArray.size());
                //Handle whatever you're going to do with the URL here

            }


        });*/


        listArray.get(index).setFoto_pemandu(alamat);
        notifyDataSetChanged();
    }



    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }

    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.individual_row_wisata_b, parent, false);
        }

        final DataModel dataModel = listArray.get(index);

        TextView nama_pemandu = (TextView) view.findViewById(R.id.nama_pemandu);
        TextView tanggal = (TextView) view.findViewById(R.id.tanggal);
        gbrprofil = (ImageView) view.findViewById(R.id.gbrprofil) ;
       // TextView id_pemandu = (TextView) view.findViewById(R.id.id_pemandu);
        TextView harga = (TextView) view.findViewById(R.id.harga);
        TextView ruangan_tersedia = (TextView) view.findViewById(R.id.ruangan_tersisa);
        //TextView fasilitas = (TextView) view.findViewById(R.id.fasilitas);
        TextView waktu = (TextView) view.findViewById(R.id.textView16);

        waktu.setText("Waktu: "+dataModel.getJam_mulai()+"-"+dataModel.getJam_selesai());
        nama_pemandu.setText(dataModel.getNama());
        tanggal.setText("Tanggal: "+dataModel.getTanggal_b());
        //id_pemandu.setText(dataModel.getId_pemandu());
        harga.setText("Rp. "+dataModel.getHarga());
        ruangan_tersedia.setText(dataModel.getRuangan_sisa());
        //fasilitas.setText(dataModel.getFasilitas());
        foto_pemandu = dataModel.getFoto_pemandu();


                Picasso.with(parent.getContext())
                        .load(foto_pemandu)
                        .placeholder(R.drawable.loading)   // optional
                        .error(R.drawable.loading)      // optional
                        .fit()
                        .centerInside()
                        .into(gbrprofil);



/*        Button button = (Button) view.findViewById(R.id.btn);
        button.setText("" + dataModel.getAnInt());*/

/*        textView = (TextView) view.findViewById(R.id.description);
        textView.setText("" + dataModel.getaDouble());*/

        // button click listener
        // this chunk of code will run, if user click the button
        // because, we set the click listener on the button only
/*
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "string: " + dataModel.getName());
                Log.d(TAG, "int: " + dataModel.getAnInt());
                Log.d(TAG, "double: " + dataModel.getaDouble());
                Log.d(TAG, "otherData: " + dataModel.getOtherData());

                Toast.makeText(parent.getContext(), "button clicked: " + dataModel.getAnInt(), Toast.LENGTH_SHORT).show();
            }
        });*/


        // if you commented out the above chunk of code and
        // activate this chunk of code
        // then if user click on any view inside the list view (except button)
        // this chunk of code will execute
        // because we set the click listener on the whole view


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "string: " + dataModel.getNama());
                Log.d(TAG, "int: " + dataModel.getTanggal());
                Log.d(TAG, "double: " + dataModel.getHarga());
                Log.d(TAG, "otherData: " + dataModel.getRuangan_sisa());
                Log.d(TAG, "TRANSAKSI ID: " + dataModel.getTransaksiid());
                CustomModelRangkuman.getInstance().changeState(true, dataModel.getKotaB(), dataModel.getDestinasiB(), dataModel.getTransaksiid());

                //Toast.makeText(parent.getContext(), "view clicked: " + dataModel.getNama(), Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

}