package com.mariwisata.wisatawan;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.AppBarLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import com.github.aakira.expandablelayout.ExpandableLinearLayout;
import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TravelmateChosenFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TravelmateChosenFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TravelmateChosenFragment extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String kota_param;
    private String dest_param;
    private String id_param;

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR  = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS     = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION              = 200;

    private boolean mIsTheTitleVisible          = false;
    private boolean mIsTheTitleContainerVisible = true;

    private OnFragmentInteractionListener mListener;

    private Toolbar mToolbar;
    private TextView mTitle, info_price;
    private TextView main_name;
    private de.hdodenhof.circleimageview.CircleImageView imageViewProfil;
    private ImageView image_dest;

    private DatabaseReference db;
    private DatabaseReference db2;

    private Button payment_mate;

    private ImageView add_adult, delete_adult, add_point, delete_point;
    private TextView adult_num, point_num;

    private TextView textDesc, textPrice, textNote, date_mate, destinasi_mate, kategori_mate, jasa_mate, budget_mate, include_mate, exclude_mate, alamat_temu, kuota, peserta;
    private CardView card1, card2, card3;
    private net.cachapa.expandablelayout.ExpandableLayout expandableLayout1, expandableLayout2, expandableLayout3;

    private TableLayout table_mate;
    private int i;

    private GoogleMap mMap;
    private SupportMapFragment mapFragment;
    private LatLng location;
    private ImageView img_intercept;
    private NestedScrollView scrollView;
    private CheckBox syarat_cek;

    private FirebaseUser user;
    private String user_id, harga_total, kuotatotal, pesertatotal;
    ArrayList<String> bulan = new ArrayList<String>();

    public TravelmateChosenFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TravelmateChosenFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TravelmateChosenFragment newInstance(String param1, String param2, String param3) {
        TravelmateChosenFragment fragment = new TravelmateChosenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            kota_param = getArguments().getString(ARG_PARAM1);
            dest_param = getArguments().getString(ARG_PARAM2);
            id_param = getArguments().getString(ARG_PARAM3);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_travelmate_chosen, container, false);
        IdentifyElements(v);

        //adult num
        add_adult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adult_change(0);
            }
        });
        delete_adult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                adult_change(1);
            }
        });

        //point num
        add_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                point_change(0);
            }
        });
        delete_point.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                point_change(1);
            }
        });

        //user
        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null)
            user_id = user.getUid();

        img_intercept.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                int action = event.getAction();
                switch (action) {
                    case MotionEvent.ACTION_DOWN:
                        // Disallow ScrollView to intercept touch events.
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        // Disable touch on transparent view
                        return false;

                    case MotionEvent.ACTION_UP:
                        // Allow ScrollView to intercept touch events.
                        scrollView.requestDisallowInterceptTouchEvent(false);
                        return true;

                    case MotionEvent.ACTION_MOVE:
                        scrollView.requestDisallowInterceptTouchEvent(true);
                        return false;

                    default:
                        return true;
                }
            }
        });

        expandableLayout1 = (net.cachapa.expandablelayout.ExpandableLayout) v.findViewById(R.id.expandable_layout1);
        card1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableLayout1.toggle();
            }
        });

        card2 = (CardView) v.findViewById(R.id.card2);
        expandableLayout2 = (net.cachapa.expandablelayout.ExpandableLayout) v.findViewById(R.id.expandable_layout2);
        card2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableLayout2.toggle(); // toggle expand and collapse
            }
        });

        card3 = (CardView) v.findViewById(R.id.card3);
        expandableLayout3 = (net.cachapa.expandablelayout.ExpandableLayout) v.findViewById(R.id.expandable_layout3);
        card3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandableLayout3.toggle();
            }
        });

        syarat_cek.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    SyaratFragment sf = new SyaratFragment();
                    sf.show(getActivity().getFragmentManager(), " ");
                    payment_mate.setEnabled(true);
                }else{
                    payment_mate.setEnabled(false);
                }
            }
        });

        db2 = FirebaseDatabase.getInstance().getReference("jadwal").child(kota_param).child(dest_param).child(id_param);
        db2.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String id_pemandu = dataSnapshot.child("pj").getValue(String.class);
                String aktivitas = dataSnapshot.child("aktivitas").getValue(String.class);
                String fasilitas = dataSnapshot.child("jasa").getValue(String.class);
                String foto_dest = dataSnapshot.child("foto").getValue(String.class);
                String jenis = dataSnapshot.child("kategori").getValue(String.class);
                 kuotatotal = dataSnapshot.child("kuota").getValue(String.class);
                Long peserta_l = dataSnapshot.child("peserta").getChildrenCount();
                 pesertatotal = String.valueOf(peserta_l);
                harga_total = dataSnapshot.child("harga").child("total").getValue(String.class);
                long people_now = dataSnapshot.child("peserta").getChildrenCount();
                String tanggal = dataSnapshot.child("tanggal").getValue(String.class);
                String jam = dataSnapshot.child("jam").getValue(String.class);
                String jasa = dataSnapshot.child("jasa").getValue(String.class);
                String kategori = dataSnapshot.child("kategori").getValue(String.class);
                String prakiraan = dataSnapshot.child("prakiraan").getValue(String.class);
                String include = dataSnapshot.child("include").getValue(String.class);
                String exclude = dataSnapshot.child("exclude").getValue(String.class);
                String notes = dataSnapshot.child("catatan").getValue(String.class);
                String latlng = dataSnapshot.child("latlng").getValue(String.class);
                String[] latlng_s = latlng.split(",");
                String lat_s = latlng_s[0];
                String lng_s = latlng_s[1];
                double lat = Double.parseDouble(lat_s);
                double lng = Double.parseDouble(lng_s);
                location = new LatLng(lat, lng);

                String alamat = dataSnapshot.child("alamat").getValue(String.class);

                //set info_price
                info_price.setText(harga_total);

                //set foto
                Picasso.with(getContext())
                        .load(foto_dest)
                        .placeholder(R.drawable.loading)   // optional
                        .error(R.drawable.loading)      // optional
                        .fit()
                        .centerInside()
                        .into(image_dest);

                //set alamt temu
                alamat_temu.setText(alamat);

                //set kuota dan peserta
                kuota.setText("Kuota total: "+kuotatotal+" orang");
                peserta.setText("Peserta saat ini: "+pesertatotal+" orang");

                //set destinasi
                destinasi_mate.setText(dest_param);

                //setTanggal dan jam
                tanggal = format_tanggal(tanggal);
                date_mate.setText(tanggal+"; "+jam);

                //set jasa dan kategori
                jasa_mate.setText(jasa);
                kategori_mate.setText(kategori);

                //set aktivitas
                textDesc.setText(aktivitas);

                //set budget
                budget_mate.setText(prakiraan);

                //set include/exlude/note
                textNote.setText(notes);
                include_mate.setText(include);
                exclude_mate.setText(exclude);

                //set map
                mapFragment.getMapAsync(new OnMapReadyCallback() {
                    @Override
                    public void onMapReady(GoogleMap googleMap) {
                        mMap = googleMap;
                        mMap.clear();
                        mMap.addMarker(new MarkerOptions().position(location).title("Titik Temu Anda")).showInfoWindow();
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15));
                    }
                });

                //Tabel Harga
                i = 0;
                for(DataSnapshot postSnapshot : dataSnapshot.child("harga").getChildren()){
                    TableRow row= new TableRow(getContext());
                    TableRow.LayoutParams lp = new TableRow.LayoutParams(TableRow.LayoutParams.WRAP_CONTENT);
                    row.setLayoutParams(lp);
                    TextView fac = new TextView(getContext());
                    TextView har = new TextView(getContext());
                    String key = postSnapshot.getKey();
                    String value = postSnapshot.getValue(String.class);
                    //format harga ke bentuk formal
                    value = format_harga(value);
                    //set text
                    fac.setText(key+"    ");
                    har.setText("IDR "+value);
                    //set warna
                    fac.setTextColor(Color.BLACK);
                    har.setTextColor(Color.BLACK);

                    if(key.equals("total")){
                        Log.d("MateChosen", "Key BOLD"+key);
                      fac.setTypeface(null, Typeface.BOLD);
                      har.setTypeface(null, Typeface.BOLD);
                    }
                    row.addView(fac);
                    row.addView(har);
                    Log.d("MateChosen", "Key+value"+key+value);
                    table_mate.addView(row, i);
                    i++;
                }



                //Nsms, foto profilnya, ability
                db2 = FirebaseDatabase.getInstance().getReference("Users").child("Guide").child(id_pemandu);
                db2.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        String nama_pemandu = dataSnapshot.child("Name").getValue(String.class);
                        String foto_profil = dataSnapshot.child("profileImageUrl").getValue(String.class);
                        String no_hp = dataSnapshot.child("Number").getValue(String.class);
                        main_name.setText(nama_pemandu);
                        mTitle.setText(nama_pemandu);

                        Picasso.with(getContext())
                                .load(foto_profil)
                                .placeholder(R.drawable.loading)   // optional
                                .error(R.drawable.loading)      // optional
                                .fit()
                                .centerInside()
                                .into(imageViewProfil);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });

                Log.d("Travelmate", "Deskripsi"+aktivitas);

               // textDesc.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));



            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        payment_mate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user == null){
                    Toast.makeText(getActivity(), "Silahkan login terlebih dahulu", Toast.LENGTH_SHORT).show();
                }else{
                    if (cekData()){
                        Log.d("PAYMENTMATE", "CekData True");
/*                        Fragment fragment2 = new PembayaranFragment().newInstance(id_param, harga_total, 10800000, kota_param, dest_param);
                        FragmentManager fragmentManager = getFragmentManager();
                        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                        fragmentTransaction.replace(R.id.frame_container, fragment2);
                        fragmentTransaction.addToBackStack(null);
                        fragmentTransaction.commit();*/
                    } else {
                        Log.d("PAYMENTMATE", "CekData false");
                    }
                }
            }
        });
        return v;
    }

    public String format_harga(String harga_format2){
        String harga_format = harga_format2;
        int i = harga_format.length();
        Log.d("HARGA", "harga_format"+harga_format+" hargaformatlength"+i+" i-3:");
        while((i-3) > 0){
            int j = i-3;
            harga_format = harga_format.substring(0, j)+"."+harga_format.substring(j, harga_format.length());
            i = i - 3;
            if (i == 3)
                break;
        }
        return harga_format;
    }

    public String format_tanggal(String tanggal){
        bulan.add(0, "January");
        bulan.add(1, "February");
        bulan.add(2, "March");
        bulan.add(3, "April");
        bulan.add(4, "May");
        bulan.add(5, "June");
        bulan.add(6, "July");
        bulan.add(7, "August");
        bulan.add(8, "September");
        bulan.add(9, "October");
        bulan.add(10, "November");
        bulan.add(11, "December");

        String[] parts_tanggal = tanggal.split("-");
        String hari = parts_tanggal[0];
        String bulan_index = parts_tanggal[1];
        String tahun = parts_tanggal[2];
        int bulan_int = Integer.parseInt(bulan_index);
        String bulan2 = bulan.get(bulan_int-1);
        String formatted = hari+" "+bulan2+" "+tahun;
        return formatted;
    }

    public void adult_change(int i){
        String adult = adult_num.getText().toString();
        int adult_int = Integer.parseInt(adult);
        if (i == 0){ //add
            adult_int++;
            String harga = info_price.getText().toString();
            int hargaint = Integer.valueOf(harga);
            int hargatotalint = Integer.valueOf(harga_total);
            info_price.setText(String.valueOf(hargaint+hargatotalint));
        } else { //delete
            if (adult_int != 1) {
                adult_int--;
                String harga = info_price.getText().toString();
                int hargaint = Integer.valueOf(harga);
                int hargatotalint = Integer.valueOf(harga_total);
                info_price.setText(String.valueOf(hargaint-hargatotalint));
            }
        }
        adult_num.setText(Integer.toString(adult_int));
    }

    public void point_change(int i){
        String point = point_num.getText().toString();
        int point_int = Integer.parseInt(point);
        if (i == 0){ //add
            point_int = point_int + 50;
        } else { //delete
            if(point_int != 0)
                point_int = point_int - 50;
        }
        point_num.setText(Integer.toString(point_int));
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public void IdentifyElements(View v){
        imageViewProfil = (de.hdodenhof.circleimageview.CircleImageView) v.findViewById(R.id.main_image);
        image_dest = (ImageView) v.findViewById(R.id.image_dest);
        main_name = (TextView) v.findViewById(R.id.main_name);
        mToolbar        = (Toolbar) v.findViewById(R.id.main_toolbar);
        mTitle          = (TextView) v.findViewById(R.id.main_textview_title);
        table_mate = (TableLayout) v.findViewById(R.id.table_mate);
        date_mate = (TextView) v.findViewById(R.id.date_mate);
        destinasi_mate = (TextView) v.findViewById(R.id.destinasi_mate);
        jasa_mate = (TextView) v.findViewById(R.id.jasa_mate);
        kategori_mate = (TextView) v.findViewById(R.id.kategori_mate);
        payment_mate = (Button) v.findViewById(R.id.payment_mate);
        add_adult = (ImageView) v.findViewById(R.id.add_adult);
        delete_adult = (ImageView) v.findViewById(R.id.delete_adult);
        add_point = (ImageView) v.findViewById(R.id.add_point);
        delete_point = (ImageView) v.findViewById(R.id.delete_point);
        adult_num = (TextView) v.findViewById(R.id.adult_num);
        point_num = (TextView) v.findViewById(R.id.point_num);
        budget_mate = (TextView) v.findViewById(R.id.budget_mate);
        include_mate = (TextView) v.findViewById(R.id.include_mate);
        exclude_mate = (TextView) v.findViewById(R.id.exclude_mate);
        img_intercept = (ImageView) v.findViewById(R.id.intercept_imagering);
        scrollView = (NestedScrollView) v.findViewById(R.id.scrollview);
        textDesc = (TextView) v.findViewById(R.id.text_desc);
        textPrice = (TextView) v.findViewById(R.id.text_price);
        textNote = (TextView) v.findViewById(R.id.text_note);
        card1 = (CardView) v.findViewById(R.id.card);
        alamat_temu = (TextView) v.findViewById(R.id.alamat_temu);
        kuota = (TextView) v.findViewById(R.id.kuota);
        peserta = (TextView) v.findViewById(R.id.peserta);
        syarat_cek = (CheckBox) v.findViewById(R.id.syarat_cek);
        mapFragment = (SupportMapFragment)  getChildFragmentManager().findFragmentById(R.id.map);
        info_price = (TextView) v.findViewById(R.id.info_price);
    }

    public boolean cekData(){
        boolean b = true;

        int kuotatotalint = Integer.valueOf(kuotatotal);
        int pesertatotalint = Integer.valueOf(pesertatotal);

        String adult = adult_num.getText().toString();
        int adultint = Integer.valueOf(adult);
        if ((kuotatotalint-pesertatotalint) < adultint){
            b = false;
        }
        return b;
    }
    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    Fragment fragment2 = new DestinationChosenFragment().newInstance(kota_param, dest_param);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment2);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                    return true;
                }
                return false;
            }
        });
    }
    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
