package com.mariwisata.wisatawan;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TransaksiAktifFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TransaksiAktifFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TransaksiAktifFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private ListView listView;
    ArrayList<DataModel> listArray;

    private FirebaseUser user;
    private String user_id;
    private DatabaseReference db;
    private AdapterTransaksiAktif adapterTransaksiAktif;
    public TransaksiAktifFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TransaksiAktifFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TransaksiAktifFragment newInstance(String param1, String param2) {
        TransaksiAktifFragment fragment = new TransaksiAktifFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_transaksiaktif, container, false);
        listView = (ListView) v.findViewById(R.id.list_transaksi_aktif);
        listArray = new ArrayList<DataModel>();

        user = FirebaseAuth.getInstance().getCurrentUser();
        user_id = user.getUid();

        db = FirebaseDatabase.getInstance().getReference("Users").child("Customers").child(user_id).child("Transaksi");
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()){
                    String status = postSnapshot.child("status").getValue(String.class);
                    if (status.equals("aktif")){
                        String destinasi = postSnapshot.child("destinasi").getValue(String.class);
                        String kota = postSnapshot.child("kota").getValue(String.class);
                        String transaksidari = postSnapshot.child("transaksidari").getValue(String.class);
                        String batas_pembayaran = postSnapshot.child("batas_pembayaran").getValue(String.class);

                        listArray.add(new DataModel(destinasi, kota, transaksidari, batas_pembayaran, status, 1));
                    }
                }
                adapterTransaksiAktif = new AdapterTransaksiAktif(listArray);
                listView.setAdapter(adapterTransaksiAktif);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
