package com.mariwisata.wisatawan;

import android.app.DatePickerDialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import static android.content.ContentValues.TAG;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link WisataFragment_BFilter.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link WisataFragment_BFilter#newInstance} factory method to
 * create an instance of this fragment.
 */
public class WisataFragment_BFilter extends Fragment{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    ListView listViewWisataB;
    // TODO: Rename and change types of parameters
    private String destinasi;
    private String gbrprofil;
    private String id_pemandu;
    private String nama;
    private String tanggal;
    private String jam_mulai;
    private String jam_selesai;
    private String harga;
    private String foto;
    private long ruangan_tersedia;
    private long ruangan_sisa;
    private long ruangan_terpakai;
    private String ruangan;
    private String fasilitas;
    private String foto_pemandu;
    private String temp_txt;
    private int i;
   private   TextView txt;
    private String mParam2;
    ArrayList<DataModel> listArray;
    DatabaseReference db;
    private OnFragmentInteractionListener mListener;
    private StorageReference mStorageRef;
    private FirebaseStorage storage;
    private CustomAdapterWisataB adapterWisataB;
    private ProgressBar pb;
    private TextView lmissionwisatab;
    private String kota;
    private TextView tanggalpilihan;
    private Calendar myCalendar;
    private String tanggalfilter;
    public WisataFragment_BFilter() {
        // Required empty public constructor
    }


    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment WisataFragment_B.
     */
    // TODO: Rename and change types and number of parameters
    public static WisataFragment_BFilter newInstance(String param1, String param2, String param3) {
        WisataFragment_BFilter fragment = new WisataFragment_BFilter();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            kota = getArguments().getString(ARG_PARAM1);
            destinasi = getArguments().getString(ARG_PARAM2);
            tanggalfilter = getArguments().getString(ARG_PARAM3);
            Log.d("WisataB", "Kota destinasi"+kota+destinasi);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        View view = inflater.inflate(R.layout.fragment_wisata_b,container,false);
        listViewWisataB = (ListView) view.findViewById(R.id.list_wisata_b);
        tanggalpilihan = (TextView) view.findViewById(R.id.tanggalpilihan);
        tanggalpilihan.setText(tanggalfilter);
/*        txt = (TextView) view.findViewById(R.id.txt);
        txt.setText(destinasi);*/
        listArray = new ArrayList<DataModel>();

        lmissionwisatab = (TextView) view.findViewById(R.id.lmissionwisatab);
        lmissionwisatab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Fragment fragment2 = new ViralFragment().newInstance("","");
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });

        pb = (ProgressBar) view.findViewById(R.id.pb3);
        pb.setVisibility(View.VISIBLE);

        db= FirebaseDatabase.getInstance().getReference("jadwal").child(kota).child(destinasi);
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
               /* String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);*/
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                        if (postSnapshot.child("Fasilitas").getValue() == null){

                        } else {

                            String transaksiid = postSnapshot.getKey();
                            Log.d("Transaksi ID", transaksiid);
                            id_pemandu = postSnapshot.child("Penanggung Jawab").getValue(String.class);
                            tanggal = postSnapshot.child("Tanggal").getValue(String.class);
                            jam_mulai = "07:00";
                            jam_selesai = "17:00";
                            String tanggal_temp = tanggal + " (Jam " + jam_mulai + "-" + jam_selesai + ")";
                            harga = postSnapshot.child("Harga").child("Total").getValue(String.class);
                            nama = postSnapshot.child("Nama Pemandu").getValue(String.class);
                            foto_pemandu = postSnapshot.child("Foto").getValue(String.class);
                            String ruangtemp = postSnapshot.child("Jumlah Orang").getValue(String.class);
                            Log.d("WisataB", "test" + foto_pemandu + ruangtemp);
                            ruangan_tersedia = Long.parseLong(ruangtemp);
                            ruangan_terpakai = postSnapshot.child("Pengikut").getChildrenCount();
                            ruangan_sisa = ruangan_tersedia - ruangan_terpakai;
                            ruangan = "Grup " +ruangan_tersedia+" slot";
                            fasilitas = postSnapshot.child("Fasilitas").getValue(String.class);
                            String fasilitas_temp = "Fasilitas: " + fasilitas;

                            Log.d("WisataBF", "comparison"+tanggalfilter+tanggal);
                            if (tanggalfilter.equals(tanggal)) {
                                Log.e("Tuts+", "Ringkasan" + id_pemandu + nama + tanggal + jam_mulai + jam_selesai + harga + ruangan_sisa + fasilitas_temp + foto_pemandu + transaksiid);
                                listArray.add(new DataModel(kota, destinasi, id_pemandu, nama, tanggal, jam_mulai, jam_selesai, harga, ruangan, fasilitas_temp, foto_pemandu, transaksiid));
                            }
                        }


                }

                adapterWisataB = new CustomAdapterWisataB(listArray);
                listViewWisataB.setAdapter(adapterWisataB);
                pb.setVisibility(View.INVISIBLE);
/*
                adapterWisataB.updateGbr(0, "https://firebasestorage.googleapis.com/v0/b/mariwisata-7dca3.appspot.com/o/Jadwal%2FYogyakarta%2FCandi%20Borobudur%2Fcandi.jpg?alt=media&token=0805298d-8608-4424-b6b4-5e2c7c5c5783");
                adapterWisataB.updateGbr(1, "https://firebasestorage.googleapis.com/v0/b/mariwisata-7dca3.appspot.com/o/Jadwal%2FYogyakarta%2FCandi%20Borobudur%2Fcandi.jpg?alt=media&token=0805298d-8608-4424-b6b4-5e2c7c5c5783");
                Intent mServiceIntent = new Intent(getActivity(), RSSPullService.class);
                mServiceIntent.putExtra("username", (Parcelable) adapterWisataB);

                //mServiceIntent.putExtra("password", "erwer");
                getActivity().startService(mServiceIntent);

*/

            /*    for ( i = 0 ; i <= listArray.size()-1 ; i++){
                    Log.e("Tuts+", "uri1: " + listArray.size() + "index" + i);
                        storage = FirebaseStorage.getInstance();
                        mStorageRef = storage.getReferenceFromUrl("gs://mariwisata-7dca3.appspot.com").child("Pemandu").child(id_pemandu).child("pasfoto.jpg");
                        mStorageRef.getDownloadUrl().addOnSuccessListener(new OnSuccessListener<Uri>() {
                            @Override
                            public void onSuccess(Uri uri) {
                                Log.e("Tuts+", "uri2: " + listArray.size() + "index" + i);
                                    adapterWisataB.updateGbr(0, "https://firebasestorage.googleapis.com/v0/b/mariwisata-7dca3.appspot.com/o/Jadwal%2FYogyakarta%2FCandi%20Borobudur%2Fcandi.jpg?alt=media&token=0805298d-8608-4424-b6b4-5e2c7c5c5783");

                                String s = uri.toString();
                                    Log.e("Tuts+", "uri3: " + listArray.size());
                                    //Handle whatever you're going to do with the URL here

                            }
                        });
                }*/


            }


            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });


        myCalendar = Calendar.getInstance();

       final DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                String myFormat = "dd-M-yyyy"; //In which you need put here
                SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
                String tgl = sdf.format(myCalendar.getTime());
                Log.d("WisataB", "tanggal"+tgl);

                Fragment fragment2 = new WisataFragment_BFilter().newInstance(kota, destinasi, tgl);
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }

        };

        tanggalpilihan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new DatePickerDialog(getActivity(), date, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();
            }
        });
        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
