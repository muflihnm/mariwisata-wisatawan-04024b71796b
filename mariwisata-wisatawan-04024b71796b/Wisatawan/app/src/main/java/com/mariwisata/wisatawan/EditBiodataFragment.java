package com.mariwisata.wisatawan;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link EditBiodataFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link EditBiodataFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class EditBiodataFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    private DatabaseReference db;
    private FirebaseUser user;
    private String user_id;

    private EditText nama_profil_edit;
    private EditText nama_lengkap_edit;
    private EditText email_edit;
    private EditText no_hp_edit;
    private Button ganti_edit;
    private ImageView edit_nama;
    private ImageView edit_namalengkap;
    private ImageView edit_nohp;
    public EditBiodataFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment EditBiodataFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static EditBiodataFragment newInstance(String param1, String param2) {
        EditBiodataFragment fragment = new EditBiodataFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_edit_biodata, container, false);

        user = FirebaseAuth.getInstance().getCurrentUser();
        user_id = user.getUid();

        nama_lengkap_edit = (EditText) v.findViewById(R.id.nama_lengkap_edit);
        nama_profil_edit  = (EditText) v.findViewById(R.id.nama_profil_edit);
        email_edit  = (EditText) v.findViewById(R.id.email_edit);
        no_hp_edit  = (EditText) v.findViewById(R.id.no_hp_edit);
        edit_nama = (ImageView) v.findViewById(R.id.edit_profil);
        edit_namalengkap  = (ImageView) v.findViewById(R.id.edit_namalengkap);
        edit_nohp  = (ImageView) v.findViewById(R.id.edit_nohp);
        ganti_edit = (Button) v.findViewById(R.id.ganti_edit);

        email_edit.setFocusable(false);

        db= FirebaseDatabase.getInstance().getReference("Users").child("Customers").child(user_id);
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String email = dataSnapshot.child("Email").getValue(String.class);
                String nama_profil = dataSnapshot.child("Nama Profil").getValue(String.class);
                String nama_lengkap = dataSnapshot.child("Name").getValue(String.class);
                String no_hp = dataSnapshot.child("Phone").getValue(String.class);

                nama_profil_edit.setText(nama_profil, TextView.BufferType.EDITABLE);
                nama_lengkap_edit.setText(nama_lengkap, TextView.BufferType.EDITABLE);
                email_edit.setText(email, TextView.BufferType.EDITABLE);
                no_hp_edit.setText(no_hp, TextView.BufferType.EDITABLE);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });




        ganti_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               db =  FirebaseDatabase.getInstance().getReference("Users").child("Customers").child(user_id);
                db.child("Name").setValue(nama_lengkap_edit.getText().toString());
                db.child("Nama Profil").setValue(nama_profil_edit.getText().toString());
                db.child("Phone").setValue(no_hp_edit.getText().toString());
                Toast.makeText(getActivity(), "Update Berhasil", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(getActivity(), MainActivity.class);
                startActivity(intent);
            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
