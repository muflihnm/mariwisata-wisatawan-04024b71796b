package com.mariwisata.wisatawan;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Calendar;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link TransaksiFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link TransaksiFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TransaksiFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private DatabaseReference db;
    private DatabaseReference db2;
    private FirebaseUser user;
    private String user_id;
    private ListView listView;
    private ArrayList<DataModel> listArray;
    private AdapterTransaksi adapterTransaksi;

    private OnFragmentInteractionListener mListener;

    public TransaksiFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TransaksiFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TransaksiFragment newInstance(String param1, String param2) {
        TransaksiFragment fragment = new TransaksiFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_transaksi, container, false);

        user = FirebaseAuth.getInstance().getCurrentUser();
        user_id = user.getUid();
        listView = (ListView) v.findViewById(R.id.list_transaksi);
        listArray = new ArrayList<DataModel>();

        db= FirebaseDatabase.getInstance().getReference("Users").child("Customers").child(user_id).child("Transaksi");
        db.addListenerForSingleValueEvent(new ValueEventListener() {

            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String key1 = postSnapshot.getKey();
                    String key = postSnapshot.child("transaksidari").getValue(String.class);
                    String destinasi = postSnapshot.child("destinasi").getValue(String.class);
                    String kota = postSnapshot.child("kota").getValue(String.class);
                    String status = postSnapshot.child("status").getValue(String.class);
                    String time = postSnapshot.child("batas_pembayaran").getValue(String.class);
                    Log.d("Transaksi","pembacaan:"+destinasi+kota+status);

//transaksi aktif = aktif, transaksi berhasil dan expired=non aktif, pembayaran telat = expired, menunggu pembayaran=menunggu

                    if(status.equals("aktif")){ //transaksi aktif 1
                        listArray.add(new DataModel(destinasi, kota, key, status, 1, 1));
                    }else if(status.equals("nonaktif")){ //transaksi tidak aktif 2

                    }else if (status.equals("expired")){ //telat bayar 3
                        listArray.add(new DataModel(destinasi, kota, key, status, 3, 1));
                    }else{
                        Calendar cal = Calendar.getInstance();
                        long date_now = cal.getTimeInMillis();
                        long selisih = Long.parseLong(time)-date_now;
                        Log.d("Transaksi","date_now:"+date_now+" status:"+Long.parseLong(time)+" selisih:"+selisih);
                        if (selisih<0){//sudah expired
                            Log.d("Transaksi","Minus, Sudah expired");
                            db2 =  FirebaseDatabase.getInstance().getReference("Users").child("Customers").child(user_id).child("Transaksi");
                            db2.child(key).child("status").setValue("expired");
                            listArray.add(new DataModel(destinasi, kota, key, "expired", 3, 1)); //telat bayar 3
                            //do error statement
                        }else{//Belum expired
                            Log.d("Transaksi","Plus, Belum Expired");
                            listArray.add(new DataModel(destinasi, kota, key, status, 4, 1)); //Lanjutin pembayaran 4
                            //lanjutkan pembayaran
                        }
                    }

                }
                adapterTransaksi = new AdapterTransaksi(listArray);
                listView.setAdapter(adapterTransaksi);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
