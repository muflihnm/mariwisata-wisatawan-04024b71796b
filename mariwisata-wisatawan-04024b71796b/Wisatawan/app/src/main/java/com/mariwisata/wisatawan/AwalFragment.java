package com.mariwisata.wisatawan;

import android.app.SearchManager;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.MenuItemCompat;
//import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.ViewFlipper;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link AwalFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link AwalFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class AwalFragment extends Fragment implements View.OnTouchListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;
    private Spinner sp;
    private ViewFlipper mViewFlipper;
    private float initialX;
    private float finalX;
    public AwalFragment(){
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment AwalFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static AwalFragment newInstance(String param1, String param2) {
        AwalFragment fragment = new AwalFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
/*
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ((AppCompatActivity)getActivity()).getSupportActionBar().setDisplayShowCustomEnabled(true);
        LayoutInflater inflator = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
*/

        View v = inflater.inflate(R.layout.fragment_awal, container, false);

        mViewFlipper = (ViewFlipper) v.findViewById(R.id.view_flipper);
        mViewFlipper.setAutoStart(true);
        mViewFlipper.setFlipInterval(4000);
        mViewFlipper.startFlipping();
        mViewFlipper.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                switch (motionEvent.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        initialX = motionEvent.getX();
                        Log.d("GESTURE", "Down:"+initialX);
                        break;
                    case MotionEvent.ACTION_UP:
                        finalX = motionEvent.getX();
                        Log.d("GESTURE", "UP:"+finalX);

                        if (initialX > finalX) {
/*                            if (mViewFlipper.getDisplayedChild() == 1)
                                break;*/

                            Log.d("GESTURE", "InitialX > final x"+initialX+">"+finalX);
/*                            mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.in_from_left));
                            mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.out_from_left));*/
                            mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.left_in));
                            mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.left_out));
                            mViewFlipper.showNext();
                        } else {
/*                            if (mViewFlipper.getDisplayedChild() == 0)
                                break;*/

                            Log.d("GESTURE", "InitialX < final x"+initialX+"<"+finalX);
/*                            mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.in_from_right));
                            mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.out_from_right));*/
                            mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.right_in));
                            mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.right_out));

                            mViewFlipper.showPrevious();
                        }
                        break;
                }
                return true;
            }
        });

 /*       //View v = inflator.inflate(R.layout.fragment_awal,null);


        String [] values =
                {"Time at Residence","Under 6 months","6-12 months","1-2 years","2-4 years","4-8 years","8-15 years","Over 15 years",};
        Spinner spinner = (Spinner) v.findViewById(R.id.spinner2);
        ArrayAdapter<String> LTRadapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, values);
        LTRadapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        spinner.setAdapter(LTRadapter);


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                                   int position, long id) {

        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {

        }
    });

        ((AppCompatActivity)getActivity()).getSupportActionBar().setCustomView(v);
        LTRadapter.notifyDataSetChanged();*/
        TextView subtitle1 = (TextView) v.findViewById(R.id.subtitle1);
        subtitle1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
/*                Fragment fragment = new ReferensiFragment();
                getFragmentManager().beginTransaction()
                        .replace(((ViewGroup) getView().getParent()).getId(), fragment)
                        .addToBackStack(null)
                        .commit();*/
                ((MainActivity)getActivity()).changeFragment(2, "Referensi");
            }
        });

        TextView subtitle4 = (TextView) v.findViewById(R.id.subtitle4);
        subtitle4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
/*                Fragment fragment = new ReferensiFragment();
                getFragmentManager().beginTransaction()
                        .replace(((ViewGroup) getView().getParent()).getId(), fragment)
                        .addToBackStack(null)
                        .commit();*/
                ((MainActivity)getActivity()).changeFragment(2, "Referensi");
            }
        });
        return v;
    }

   /* public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        getActivity().getMenuInflater().inflate(R.menu.menuspinner, menu);

        MenuItem item = menu.findItem(R.id.spinner);
        //Spinner sp = (Spinner)getActivity().findViewById(R.id.spinner);
        sp = (Spinner)item.getActionView();
        if(sp == null)
            Log.i("TAG","spinner == null");
        String [] values =
                {"Pilih Kota","Yogyakarta","6-12 months","1-2 years","2-4 years","4-8 years","8-15 years","Over 15 years",};
        ArrayList arrayList = new ArrayList<String>();
        arrayList.add("ewer");
        arrayList.add("rerw");
        List<String> lables = arrayList;
        //ArrayAdapter<String> spinneradapter= new ArrayAdapter<String>(getView().getContext(),android.R.layout.simple_spinner_item,values);
        ArrayAdapter spinneradapter = new ArrayAdapter<String>(this.getActivity(), android.R.layout.simple_spinner_item, values);

        spinneradapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        sp.setAdapter(spinneradapter);
        sp.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Log.d("tag", "Item Selected");
                long posisi = parent.getItemIdAtPosition(position);
                String s = parent.getItemAtPosition(position).toString();
                Log.d("tag", "Item Selected" + posisi + s);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                Log.d("tag", "nothing submitted");
            }
        });

*//*
        inflater.inflate(R.menu.menusearch, menu);
        MenuItem item = menu.findItem(R.id.action_search);
        SearchView sv = new SearchView(((MainActivity) getActivity()).getSupportActionBar().getThemedContext());
        MenuItemCompat.setShowAsAction(item, MenuItemCompat.SHOW_AS_ACTION_COLLAPSE_ACTION_VIEW | MenuItemCompat.SHOW_AS_ACTION_IF_ROOM);
        MenuItemCompat.setActionView(item, sv);

        sv.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                System.out.println("search query submit");
                Log.d("tag", "query text submit");
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                Log.d("tag", "query tap");
                System.out.println("tap");
                return false;
            }
        });

        // Associate searchable configuration with the SearchView
        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        SearchView searchView = (SearchView) menu.findItem(R.id.action_search).getActionView();
        Log.d("Tab", "SearchManager: " + searchManager + " : " + searchView);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getActivity().getComponentName()));
*//*


    }*/

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    @Override
    public boolean onTouch(View view, MotionEvent touchevent) {
        switch (touchevent.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialX = touchevent.getX();
                break;
            case MotionEvent.ACTION_UP:
                float finalX = touchevent.getX();
                if (initialX > finalX) {
                    Log.d("tag", "yuhu");
                    if (mViewFlipper.getDisplayedChild() == 1)
                        break;

                    mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.in_from_left));
                    mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.out_from_left));

                    mViewFlipper.showNext();
                } else {
                    Log.d("tag", "yuhu2");
                    if (mViewFlipper.getDisplayedChild() == 0)
                        break;

                    mViewFlipper.setInAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.in_from_right));
                    mViewFlipper.setOutAnimation(AnimationUtils.loadAnimation(getContext(), R.anim.out_from_right));

                    mViewFlipper.showPrevious();
                }
                break;
        }

        return false;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
