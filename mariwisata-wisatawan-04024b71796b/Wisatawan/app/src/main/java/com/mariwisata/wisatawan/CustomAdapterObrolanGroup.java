package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 18/01/2018.
 */

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static android.R.attr.fragment;

/**
 * Created with IntelliJ IDEA.
 * User: Shahab
 * Date: 8/22/12
 * Time: 11:37 AM
 * To change this template use File | Settings | File Templates.
 */

public class CustomAdapterObrolanGroup extends BaseAdapter {


    private static final String TAG = CustomAdapterObrolanGroup.class.getSimpleName();
    ArrayList<DataModel> listArray;

    //testcase1
    DatabaseReference db;
    Boolean saved=null;
    private String foto_pemandu;
    private ImageView gbrprofil;
    private StorageReference mStorageRef;
    private FirebaseStorage storage;
    private int index2;
    private String alamat2;

    public CustomAdapterObrolanGroup(ArrayList<DataModel> listArray) {

        this.listArray = listArray;
        Log.d(TAG, "int: " + listArray.toString());

    }




    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }

    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.individual_obrolan_group, parent, false);
        }

        final DataModel dataModel = listArray.get(index);

        TextView nama_grup = (TextView) view.findViewById(R.id.namaGroup);

        nama_grup.setText(dataModel.getNama_grup());

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Log.d(TAG, "ObrolanGroup" + dataModel.getNama_grup());
                Log.d(TAG, "ObrolanGroup" + dataModel.getKey());
                CustomModelObrolan.getInstance().changeState(true, dataModel.getKey(), dataModel.getNama_grup());
                //CustomModelRangkuman.getInstance().changeState(true, dataModel.getTransaksiid());

                //Toast.makeText(parent.getContext(), "view clicked: " + dataModel.getNama(), Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

}