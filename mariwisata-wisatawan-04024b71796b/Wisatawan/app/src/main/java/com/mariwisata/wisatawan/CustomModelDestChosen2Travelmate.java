package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 09/03/2018.
 */

public class CustomModelDestChosen2Travelmate {
    public interface OnCustomStateListener {
        void stateChangedDest2Mate(String kota, String destinasi, String id);
    }

    private static CustomModelDestChosen2Travelmate mInstance;
    private OnCustomStateListener mListener;
    private boolean mState;

    private CustomModelDestChosen2Travelmate() {}

    public static CustomModelDestChosen2Travelmate getInstance() {
        if(mInstance == null) {
            mInstance = new CustomModelDestChosen2Travelmate();
        }
        return mInstance;
    }

    public void setListener(OnCustomStateListener listener) {
        mListener = listener;
    }

    public void changeStateDest2Mate(boolean state, String kota, String destinasi, String id) {
        if(mListener != null) {
            mState = state;
            notifyStateChange(kota, destinasi, id);
        }
    }

    public boolean getState() {
        return mState;
    }

    private void notifyStateChange(String kota, String destinasi, String id) {
        mListener.stateChangedDest2Mate(kota, destinasi, id);
    }
}
