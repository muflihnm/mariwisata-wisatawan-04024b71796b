package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 02/01/2018.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.provider.ContactsContract;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.support.annotation.NonNull;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseException;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import org.w3c.dom.Text;

import java.lang.reflect.Array;
import java.util.ArrayList;

import static android.R.attr.fragment;
import static android.R.attr.notificationTimeout;

/**
 * Created with IntelliJ IDEA.
 * User: Shahab
 * Date: 8/22/12
 * Time: 11:37 AM
 * To change this template use File | Settings | File Templates.
 */

public class CustomAdapter extends BaseAdapter {


    private static final String TAG = CustomAdapter.class.getSimpleName();
    ArrayList<DataModel> listArray;

    //testcase1
    DatabaseReference db;
    Boolean saved=null;
    ArrayList<String> spacecrafts=new ArrayList<>();



    public CustomAdapter(ArrayList<DataModel> listArray) {

        this.listArray = listArray;
        Log.d(TAG, "int: " + listArray.toString());

        /*db= FirebaseDatabase.getInstance().getReference("jadwal");

        listArray = new ArrayList<DataModel>();
        listArray.add(new DataModel("name1", 5, 1.8, "Java"));
        listArray.add(new DataModel("rerw", 1220, 2.38, "test"));

        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
               *//* String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);*//*
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    Log.e(TAG, "catatan======="+postSnapshot.child("catatan").getValue());
                    Log.e(TAG, "harga======="+postSnapshot.child("fasilitas").getValue());


                    listArray.add(new DataModel("dfd", 1220, 2.38, "weq"));
                }
            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });*/

/*        listArray.add(new DataModel("name23", 10, 2.8, "Python"));
        listArray.add(new DataModel("name3", 15, 3.8, "Django"));
        listArray.add(new DataModel("name4", 20, 4.8, "Groovy"));
        listArray.add(new DataModel("name5", 25, 5.8, "Maven"));
        listArray.add(new DataModel("name5", 25, 5.8, "Maven"));
        listArray.add(new DataModel("name5", 25, 5.8, "Maven"));
        listArray.add(new DataModel("name5", 25, 5.8, "Maven"));
        listArray.add(new DataModel("name5", 25, 5.8, "Maven"));*/
    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    public void updateIfPresent(int index){
/*        DataModel dm_temp = new DataModel();
        listArray.set(index, dm_temp);*/
        listArray.get(1).setTanggal("Tanggal yang ke set yuhu");

        notifyDataSetChanged();
    }

    public void add(){
/*        listArray.add(listArray.get(1));
        DataModel dm = new DataModel();
        dm = listArray.get(1);
        notifyDataSetChanged();*/
    }
    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }

    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.individual_row_wisata, parent, false);
        }

        final DataModel dataModel = listArray.get(index);

        TextView destinasi = (TextView) view.findViewById(R.id.destinasi);
/*        TextView harga_termurah = (TextView) view.findViewById(R.id.harga_termurah);
        TextView kategori = (TextView) view.findViewById(R.id.kategori);
        TextView tanggal = (TextView) view.findViewById(R.id.tanggal);*/
        ImageView foto = (ImageView) view.findViewById(R.id.gbrdestinasi);

        destinasi.setText(dataModel.getDestinasi());
/*
        harga_termurah.setText(dataModel.getHarga_termurah());
        kategori.setText(dataModel.getKategori());
        tanggal.setText(dataModel.getTanggal());
*/

        Picasso.with(parent.getContext())
                .load(dataModel.getFoto())
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.loading)      // optional
                .fit()
                .centerInside()
                .into(foto);
/*        Button button = (Button) view.findViewById(R.id.btn);
        button.setText("" + dataModel.getAnInt());*/

/*        textView = (TextView) view.findViewById(R.id.description);
        textView.setText("" + dataModel.getaDouble());*/

        // button click listener
        // this chunk of code will run, if user click the button
        // because, we set the click listener on the button only
/*
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "string: " + dataModel.getName());
                Log.d(TAG, "int: " + dataModel.getAnInt());
                Log.d(TAG, "double: " + dataModel.getaDouble());
                Log.d(TAG, "otherData: " + dataModel.getOtherData());

                Toast.makeText(parent.getContext(), "button clicked: " + dataModel.getAnInt(), Toast.LENGTH_SHORT).show();
            }
        });*/


        // if you commented out the above chunk of code and
        // activate this chunk of code
        // then if user click on any view inside the list view (except button)
        // this chunk of code will execute
        // because we set the click listener on the whole view


        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "string: " + dataModel.getDestinasi());
                Log.d(TAG, "int: " + dataModel.getHarga_termurah());
                Log.d(TAG, "double: " + dataModel.getKategori());
                Log.d(TAG, "otherData: " + dataModel.getTanggal());

                CustomModel.getInstance().changeState(true, dataModel.getKota(), dataModel.getDestinasi());

                //Toast.makeText(parent.getContext(), "view clicked: " + dataModel.getDestinasi(), Toast.LENGTH_SHORT).show();
            }
        });

        return view;
    }

}