package com.mariwisata.wisatawan;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MyLmissionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MyLmissionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MyLmissionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private GridView gridview;
    private ArrayList<DataModel> gridList;
    private DatabaseReference db;
    private String kota;
    private AdapterLmission adapter;
    private ProgressBar pb;
    private String destinasi;
    private OnFragmentInteractionListener mListener;
    private FirebaseUser user;
    private String user_id;

    public MyLmissionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment ViralFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static MyLmissionFragment newInstance(String param1, String param2) {
        MyLmissionFragment fragment = new MyLmissionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
            kota = mParam1;
            destinasi = mParam2;

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.myfragment_viral, container, false);
        gridview = v.findViewById(R.id.grid_viral);
        gridList = new ArrayList<DataModel>();


        user = FirebaseAuth.getInstance().getCurrentUser();
        user_id = user.getUid();

        pb = (ProgressBar) v.findViewById(R.id.pb_viral);
        pb.setVisibility(View.VISIBLE);

        db= FirebaseDatabase.getInstance().getReference("lmission");
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                // This method is called once with the initial value and again
                // whenever data at this location is updated.
               /* String value = dataSnapshot.getValue(String.class);
                Log.d(TAG, "Value is: " + value);*/
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    for(DataSnapshot postSnapshot2 : postSnapshot.getChildren()){
                        String temp = postSnapshot2.child("creator").getValue(String.class);
                        if (user_id.equals(temp)){
                            String foto = postSnapshot2.child("foto").getValue(String.class);
                            String deskripsi = postSnapshot2.child("deskripsi").getValue(String.class);
                            String date = String.valueOf(postSnapshot2.child("waktu").getValue(Long.class));
                            String creator = user_id;
                            String total_cares = String.valueOf(postSnapshot2.child("cares").child("id").getChildrenCount());
                            String key = postSnapshot2.getKey();
                            String kota2 = postSnapshot.getKey();
                            String destinasi2 = postSnapshot2.child("destinasi").getValue(String.class);

                            gridList.add(new DataModel(foto, deskripsi, date, creator, total_cares, key, kota2, destinasi2));
                        }
                    }
                }
                pb.setVisibility(View.INVISIBLE);
                adapter = new AdapterLmission(gridList);
                gridview.setAdapter(adapter);
            }


            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });




        return v;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
