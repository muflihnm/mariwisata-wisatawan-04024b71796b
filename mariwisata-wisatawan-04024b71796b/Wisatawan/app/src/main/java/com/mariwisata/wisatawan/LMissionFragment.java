package com.mariwisata.wisatawan;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.format.DateFormat;
import android.text.style.StyleSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseListAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.Date;

import static android.icu.lang.UProperty.INT_START;
import static android.support.v4.content.ContextCompat.getDrawable;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link LMissionFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link LMissionFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class LMissionFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    private static final String ARG_PARAM4 = "param4";
    private static final String ARG_PARAM5 = "param5";
    private static final String ARG_PARAM6 = "param6";
    private static final String ARG_PARAM7 = "param7";
    private static final String ARG_PARAM8 = "param8";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String foto;
    private String deskripsi;
    private String waktu;
    private String creator;
    private String total_cares;
    private String key;
    private String kota;
    private String destinasi;
    private OnFragmentInteractionListener mListener;

    private TextView creator_layout;
    private ImageView foto_layout;
    private TextView total_cares_layout;
    private TextView location_layout;
    private TextView deskripsi_layout;
    private ImageView care_btn;
    private ImageView share_btn;
    private ImageView btn_comment2;
    private FirebaseListAdapter<CommentMessage> adapter;
    private ListView listOfMessages;
    private Button btn_comment;
    private EditText komentar;
    private int total_temp;

    private FirebaseAuth mAuth;
    private FirebaseUser user;
    public LMissionFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *

     * @return A new instance of fragment LMissionFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static LMissionFragment newInstance(String foto, String deskripsi, String waktu, String creator, String total_cares, String key, String kota, String destinasi) {
        LMissionFragment fragment = new LMissionFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, foto);
        args.putString(ARG_PARAM2, deskripsi);
        args.putString(ARG_PARAM3, waktu);
        args.putString(ARG_PARAM4, creator);
        args.putString(ARG_PARAM5, total_cares);
        args.putString(ARG_PARAM6, key);
        args.putString(ARG_PARAM7, kota);
        args.putString(ARG_PARAM8, destinasi);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            this.foto = getArguments().getString(ARG_PARAM1);
            this.deskripsi = getArguments().getString(ARG_PARAM2);
            this.waktu = getArguments().getString(ARG_PARAM3);
            this.creator = getArguments().getString(ARG_PARAM4);
            this.total_cares = getArguments().getString(ARG_PARAM5);
            this.key = getArguments().getString(ARG_PARAM6);
            this.kota = getArguments().getString(ARG_PARAM7);
            this.destinasi = getArguments().getString(ARG_PARAM8);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_lmission, container, false);
        Log.d("LMISSION", "Data:"+foto+deskripsi+waktu+creator+total_cares+key+kota+destinasi);

        creator_layout = (TextView) v.findViewById(R.id.textView4_lmission);
        foto_layout = (ImageView) v.findViewById(R.id.imageView11);
        total_cares_layout = (TextView) v.findViewById(R.id.textView2_lmission);
        location_layout = (TextView) v.findViewById(R.id.textView5_lmission);
        deskripsi_layout = (TextView) v.findViewById(R.id.textView1_lmission);
        care_btn = (ImageView) v.findViewById(R.id.imageView13_lmission);
        share_btn = (ImageView) v.findViewById(R.id.share_lmission);
        btn_comment2 = (ImageView) v.findViewById(R.id.imageView19);
        share_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // https://stackoverflow.com/questions/6814268/android-share-on-facebook-twitter-mail-ecc
                // https://developer.telerik.com/featured/social-media-integration-android/
                String message = "Hey check this out!";
                Intent share = new Intent(Intent.ACTION_SEND);
                share.setType("text/plain");
                share.putExtra(Intent.EXTRA_TEXT, message);

                startActivity(Intent.createChooser(share, "Share"));
                Toast.makeText(getActivity(), "Sharing feature is under development", Toast.LENGTH_SHORT).show();
            }
        });

        Picasso.with(getContext())
                .load(foto)
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.loading)      // optional
                .fit()
                .centerInside()
                .into(foto_layout);

        total_cares_layout.setText("cared by "+total_cares+" people");
        location_layout.setText("location: "+destinasi+", "+kota);

        deskripsi_layout.setText(deskripsi);

        listOfMessages = (ListView) v.findViewById(R.id.list_of_comments);
        displayComments();

        komentar = (EditText) v.findViewById(R.id.komentar);

        //Cek Jumlah Total Cares
        DatabaseReference db2 =  FirebaseDatabase.getInstance().getReference("lmission").child(kota).child(key).child("cares").child("id");
        db2.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                total_temp = (int) dataSnapshot.getChildrenCount();
                total_cares_layout.setText("cared by "+total_temp+" people");
                Log.d("LMISSION", "total_temp "+total_temp);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //Cek Nama Creator
        DatabaseReference db4 =  FirebaseDatabase.getInstance().getReference("Users").child("Customers").child(creator);
        db4.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String nama_profil = dataSnapshot.child("Nama Profil").getValue(String.class);
                creator_layout.setText(nama_profil);
                Log.d("LMISSION", "total_temp "+total_temp);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        //Cek Comments
        DatabaseReference db =  FirebaseDatabase.getInstance().getReference("lmission").child(kota).child(key).child("comments");
        db.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                UIUtils.setListViewHeightBasedOnItems(listOfMessages);
                //listOfMessages.smoothScrollToPosition(i-1);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
        //Cek Apakah sudah pernah like
        DatabaseReference db3 =  FirebaseDatabase.getInstance().getReference("lmission").child(kota).child(key).child("cares").child("id");
        db3.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {
                    String s= postSnapshot.getValue(String.class);
                    if(s.equals(user.getUid())){
                        care_btn.setEnabled(false);
                        care_btn.setBackground(getDrawable(getContext(), R.drawable.rec_shape_2));
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        mAuth = FirebaseAuth.getInstance();
        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user == null){ //Belum Login
            care_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getActivity(), "Silahkan login terlebih dahulu", Toast.LENGTH_SHORT).show();
                }
            });
            btn_comment2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Toast.makeText(getActivity(), "Silahkan login terlebih dahulu", Toast.LENGTH_SHORT).show();
                }
            });
        } else { //Sudah login, cek apakah sudah punya transaksi?

            care_btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    care_btn.setBackground(getDrawable(getContext(), R.drawable.rec_shape_2));

                    long time =  new Date().getTime();
                   DatabaseReference db =  FirebaseDatabase.getInstance().getReference("lmission").child(kota).child(key).child("cares");
                    Log.d("LMission", "lewat sini"+kota);
                    db.child("id").child(String.valueOf(time)).setValue(user.getUid());
                    total_temp++;
                    db.child("total").setValue(total_temp);
                    care_btn.setEnabled(false);
                }
            });
            btn_comment2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    DatabaseReference  db= FirebaseDatabase.getInstance().getReference("Users").child("Customers");
                    db.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {
                            String nama = dataSnapshot.child(user.getUid()).child("Nama Profil").getValue(String.class);
                            // Read the input field and push a new instance
                            // of ChatMessage to the Firebase database
                            FirebaseDatabase.getInstance()
                                    .getReference()
                                    .child("lmission")
                                    .child(kota)
                                    .child(key)
                                    .child("comments")
                                    .push()
                                    .setValue(new CommentMessage(komentar.getText().toString(), nama)
                                    );

                            // Clear the input
                            komentar.setText("");
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });

                }
            });
        }
        return v;
    }
    private void displayComments() {


        adapter = new FirebaseListAdapter<CommentMessage>(getActivity(), CommentMessage.class, R.layout.comment, FirebaseDatabase.getInstance().getReference().child("lmission").child(kota).child(key).child("comments")) {
            @Override
            protected void populateView(View v, CommentMessage model, int position) {
                // Get references to the views of message.xml
                TextView messageText = (TextView) v.findViewById(R.id.comment_text);
                TextView messageUser = (TextView) v.findViewById(R.id.comment_user);
                TextView messageTime = (TextView) v.findViewById(R.id.comment_time);

                // Set their text
                messageText.setText(model.getMessageText());
                messageUser.setText(model.getMessageUser());

                // Format the date before showing it
                messageTime.setText(DateFormat.format("dd-MM-yyyy (HH:mm:ss)",
                        model.getMessageTime()));

            }
        };
        listOfMessages.setAdapter(adapter);
        UIUtils.setListViewHeightBasedOnItems(listOfMessages);
    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
