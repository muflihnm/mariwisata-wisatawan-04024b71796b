package com.mariwisata.wisatawan;

import android.util.Log;

/**
 * Created by Cerwyn on 18/01/2018.
 */

public class CustomModelObrolan {
    public interface OnCustomStateListener {
        void stateChangedObrolan(String key, String grup);
    }

    private static CustomModelObrolan mInstance;
    private OnCustomStateListener mListener;
    private boolean mState;

    private CustomModelObrolan() {}

    public static CustomModelObrolan getInstance() {
        if(mInstance == null) {
            mInstance = new CustomModelObrolan();
        }
        return mInstance;
    }

    public void setListener(OnCustomStateListener listener) {
        mListener = listener;
    }

    public void changeState(boolean state, String key, String grup) {
        if(mListener != null) {
            mState = state;
            Log.d("ChangeState", "True");
            notifyStateChange(key, grup);
        }
    }

    public boolean getState() {
        return mState;
    }

    private void notifyStateChange(String key, String grup) {
        Log.d("Notify", "True");
        mListener.stateChangedObrolan(key, grup);
    }
}