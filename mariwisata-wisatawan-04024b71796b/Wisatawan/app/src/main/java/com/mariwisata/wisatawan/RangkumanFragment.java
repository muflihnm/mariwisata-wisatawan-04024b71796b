package com.mariwisata.wisatawan;

import android.app.ActionBar;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.content.ContentValues.TAG;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link RangkumanFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link RangkumanFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class RangkumanFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private static final String ARG_PARAM3 = "param3";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String transaksiid;
    private OnFragmentInteractionListener mListener;
    private List<String> child1;
    private List<String> child2;
    private List<String> child3;
    private List<String> child4;
    private String nama;
    private String tanggal;
    private String foto;
    private String jam_mulai;
    private String jam_selesai;
    private String harga_total;
    private TextView nama_pemandu;
    private ImageView foto_pemandu;
    private TextView tanggal_rangkuman;
    private TextView proses_1;
    private TextView proses_2;
    private String destinasi;
    private String kota;
    private TextView jam;
    DatabaseReference db;
    private FirebaseUser user;
    private DatabaseReference db3;
    private String user_id;
    private Boolean boole_cek = false;
    private ArrayList<String> header;
    private Button konfirmasi;
    private ImageView background;
    public RangkumanFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     //* @param param2 Parameter 2.
     * @return A new instance of fragment RangkumanFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static RangkumanFragment newInstance(String param1, String param2, String param3) {
        RangkumanFragment fragment = new RangkumanFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        args.putString(ARG_PARAM3, param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            kota = getArguments().getString(ARG_PARAM1);
            destinasi = getArguments().getString(ARG_PARAM2);
            transaksiid = getArguments().getString(ARG_PARAM3);
            //mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rangkuman,container,false);

        ExpandableListView expandableListView  = (ExpandableListView) view.findViewById(R.id.expand_rangkuman);
        expandableListView.setGroupIndicator(null);

        // Array list for header
        header = new ArrayList<String>();
        // Array list for child items
         child1 = new ArrayList<String>();
         child2 = new ArrayList<String>();
        child3 = new ArrayList<String>();
        child4 = new ArrayList<String>();
        // Hash map for both header and child
        HashMap<String, List<String>> hashMap = new HashMap<String, List<String>>();

/*        header.add("Aktivitas");
        header.add("Fasilitas");
        header.add("Harga");
        header.add("Catatan");*/

        nama_pemandu = (TextView) view.findViewById(R.id.nama_pemandurangkuman);
        foto_pemandu = (ImageView) view.findViewById(R.id.foto_pemandurangkuman);
        tanggal_rangkuman = (TextView) view.findViewById(R.id.tanggal_rangkuman);
        jam = (TextView) view.findViewById(R.id.jam_rangkuman);
        konfirmasi = (Button) view.findViewById(R.id.button2);
        konfirmasi.setEnabled(false);
        background = (ImageView) view.findViewById(R.id.imageView7);

        db= FirebaseDatabase.getInstance().getReference("jadwal").child(kota).child(destinasi).child(transaksiid);
        Log.d("Rangkuman", "test1"+kota+destinasi+transaksiid);
        db.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                String deskripsi = dataSnapshot.child("Deskripsi Kegiatan").getValue(String.class);
                String fasilitas = dataSnapshot.child("Fasilitas").getValue(String.class);
                String catatan = "Belum Ada";

                child1.add(deskripsi);
                child2.add(fasilitas);
                child4.add(catatan);

                 nama = dataSnapshot.child("Nama Pemandu").getValue(String.class);
                 foto = dataSnapshot.child("Foto").getValue(String.class);
                 tanggal = dataSnapshot.child("Tanggal").getValue(String.class);
                 jam_mulai = "07:00";
                 jam_selesai = "15:00";
                 harga_total = dataSnapshot.child("Harga").child("Total").getValue(String.class);
                String foto2 = dataSnapshot.child("ImageUrl").getValue(String.class);
                Picasso.with(getActivity().getApplicationContext())
                        .load(foto2)
                        .placeholder(R.drawable.loading)   // optional
                        .error(R.drawable.loading)      // optional
                        .fit()
                        .centerInside()
                        .into(background);

                nama_pemandu.setText(nama);
                tanggal_rangkuman.setText("Tanggal: "+tanggal);
                jam.setText("Pukul: "+jam_mulai +"-"+jam_selesai + " WIB");

                Picasso.with(getActivity().getApplicationContext())
                        .load(foto)
                        .placeholder(R.drawable.loading)   // optional
                        .error(R.drawable.loading)      // optional
                        .fit()
                        .centerInside()
                        .into(foto_pemandu);

                for (DataSnapshot postSnapshot : dataSnapshot.child("Harga").getChildren()) {
                    Log.d("Header", postSnapshot.getKey());
                    Log.d("Header", " "+postSnapshot.getValue(String.class));
                    String item = postSnapshot.getKey() + " = " +postSnapshot.getValue(String.class);
                    child3.add(item);
                }

            }

            @Override
            public void onCancelled(DatabaseError error) {
                // Failed to read value
                Log.w(TAG, "Failed to read value.", error.toException());
            }
        });
        header.add("Aktivitas");
        header.add("Fasilitas");
        header.add("Harga");
        header.add("Catatan");

        hashMap.put(header.get(0), child1);
        hashMap.put(header.get(1), child2);
        hashMap.put(header.get(2), child3);
        hashMap.put(header.get(3), child4);

        ExpandableListAdapter adapter = new ExpandableListAdapter(getActivity(), header, hashMap);
        expandableListView.setAdapter(adapter);

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {
                setListViewHeight(expandableListView, i);
                return false;
            }
        });

        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null)
        user_id = user.getUid();

        db3= FirebaseDatabase.getInstance().getReference("Users").child("Customers").child(user_id).child("Transaksi");
        db3.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                for (DataSnapshot postSnapshot : dataSnapshot.getChildren()) {

                    Log.d("Rangkuman", "Boole true1"+postSnapshot.child("transaksidari").getValue(String.class));
                    String transaksiid2 = postSnapshot.child("transaksidari").getValue(String.class);

                    if (transaksiid2.equals(transaksiid)){
                        Toast.makeText(getActivity(), "Cek histori transaksi anda", Toast.LENGTH_SHORT).show();
                        boole_cek = true;
                        Log.d("Rangkuman", "Boole true"+boole_cek);
                    }

                }
                if (!boole_cek)
                    konfirmasi.setEnabled(true);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        konfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (user == null){
                    Toast.makeText(getActivity(), "Silahkan login terlebih dahulu", Toast.LENGTH_SHORT).show();
                }else{
                    Fragment fragment2 = new PembayaranFragment().newInstance(transaksiid, harga_total, 10800000, kota, destinasi);
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment2);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }
        });



        return view;
    }
    private void setListViewHeight(ExpandableListView listView,
                                   int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }
    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
