package com.mariwisata.wisatawan;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

/**
 * Created by ASUS on 06/02/2018.
 */

public class DetailDestinasi extends DialogFragment {

    private String destinasi;
    private String kota;
    private String deskripsi;
    private String foto;

    public void setDeskripsi(String deskripsi) {
        this.deskripsi = deskripsi;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public void setKota(String kota) {
        this.kota = kota;
    }

    public void setDestinasi(String destinasi) {
        this.destinasi = destinasi;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder=new AlertDialog.Builder(getActivity());
        View v = getActivity().getLayoutInflater().inflate(R.layout.fragment_destinasi_fragment2, null, false);
        ImageView foto_ref = (ImageView) v.findViewById(R.id.foto_ref);
        TextView destinasi_ref = (TextView) v.findViewById(R.id.destinasi_ref);
        TextView kota_ref = (TextView) v.findViewById(R.id.kota_ref);
        TextView deskripsi_ref = (TextView) v.findViewById(R.id.deskripsi_ref);

        v.findViewById(R.id.layar_ref).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });
        destinasi_ref.setText(destinasi);
        kota_ref.setText(kota);
        deskripsi_ref.setText(deskripsi);

        Picasso.with(getActivity())
                .load(foto)
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.loading)      // optional
                .fit()
                .centerInside()
                .into(foto_ref);

        builder.setView(v);
        return builder.create();
    }
}

