package com.mariwisata.wisatawan;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link DestinationChosenFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link DestinationChosenFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class DestinationChosenFragment extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private String kota_param;
    private String destinasi_param;
    private OnFragmentInteractionListener mListener;
    private ProgressBar pb_dest;
    private FloatingActionButton fab;

    private DatabaseReference db;
    private DatabaseReference db_getgambar;
    private DatabaseReference db_getprofil;
    private ImageView img_dest;
    HashMap<String, String> harga_map=new HashMap<String, String>();

    private String pj;
    private String nama_pemandu;
    private String foto_profil;

    private GridView gridView;
    ArrayList<DataModel> arrayList = new ArrayList<DataModel>();
    ArrayList<DataModel> arrayList_lmission = new ArrayList<DataModel>();
    private CustomAdapterDestChosen customAdapterDestChosen;
    private int x;

    private AppBarLayout appbar;
    private  com.github.mmin18.widget.RealtimeBlurView blurView;

    private ArrayList<ArrayList<DataModel>> data_list = new ArrayList<ArrayList<DataModel>>();

   private HashMap<String, String> nama_img = new HashMap<String, String>();

    private android.support.design.widget.FloatingActionButton filter_fab;

    private GridView grid_lmission;
    private AdapterLmission adapter;
    public DestinationChosenFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment DestinationChosenFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static DestinationChosenFragment newInstance(String param1, String param2) {
        DestinationChosenFragment fragment = new DestinationChosenFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            kota_param = getArguments().getString(ARG_PARAM1);
            destinasi_param = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v =inflater.inflate(R.layout.fragment_destination_chosen, container, false);
        Toolbar toolbar = (Toolbar) v.findViewById(R.id.toolbar2);

        img_dest = (ImageView) v.findViewById(R.id.img_dest);

        appbar = (AppBarLayout) v.findViewById(R.id.appbar);
        blurView = (com.github.mmin18.widget.RealtimeBlurView) v.findViewById(R.id.blurr);

        ViewPager viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        setupViewPager(viewPager);
        TabLayout tabs = (TabLayout) v.findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);
        toolbar.setTitle(destinasi_param);

/*        filter_fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FilterDestChosen filter=new FilterDestChosen();
                filter.show(getActivity().getFragmentManager(), "");
                Fragment fragment2 = new RequestFragment();
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.frame_container, fragment2);
                fragmentTransaction.addToBackStack(null);
                fragmentTransaction.commit();
            }
        });*/

        appbar.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(final AppBarLayout appBarLayout, final int verticalOffset) {
                float offsetAlpha = (appBarLayout.getY() / appbar.getTotalScrollRange());
                blurView.setAlpha(offsetAlpha * -1);
                Log.d("blur", "offset alpha:"+offsetAlpha+" nilai alpha:"+(1 - (offsetAlpha * -1)));
            }
        });

        db_getgambar = FirebaseDatabase.getInstance().getReference("referensi").child("destinasi").child(kota_param).child(destinasi_param);
        db_getgambar.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String url_foto = dataSnapshot.child("foto").getValue(String.class);
                Picasso.with(getContext())
                        .load(url_foto)
                        .placeholder(R.drawable.loading)   // optional
                        .error(R.drawable.loading)      // optional
                        .fit()
                        .centerInside()
                        .into(img_dest);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });


        return v;
    }

    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {

        Adapter adapter = new Adapter(getChildFragmentManager());
        adapter.addFragment(new TravelmateFragment().newInstance(kota_param,destinasi_param), "Travelmate");
        adapter.addFragment(new ViralFragment(), "L-Mission");
        viewPager.setAdapter(adapter);

    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }


    @Override
    public void onResume() {
        super.onResume();

        if(getView() == null){
            return;
        }

        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    Fragment fragment2 = new HomeFragment();
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.replace(R.id.frame_container, fragment2);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                    return true;
                }
                return false;
            }
        });
    }



    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
