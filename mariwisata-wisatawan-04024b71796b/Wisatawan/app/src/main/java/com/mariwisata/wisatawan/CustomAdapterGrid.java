package com.mariwisata.wisatawan;

/**
 * Created by Cerwyn on 15/01/2018.
 */

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class CustomAdapterGrid extends BaseAdapter{

    private static final String TAG = CustomAdapterWisataB.class.getSimpleName();
    ArrayList<DataModel> listArray;
    //testcase1
    DatabaseReference db;
    Boolean saved=null;
    private String foto_ref;
    private ImageView gbrprofil;
    private StorageReference mStorageRef;
    private FirebaseStorage storage;
    private int index2;
    private String alamat2;

    public CustomAdapterGrid(ArrayList<DataModel> listArray) {

        this.listArray = listArray;
        Log.d(TAG, "int: " + listArray.toString());

    }

    @Override
    public int getCount() {
        return listArray.size();    // total number of elements in the list
    }

    @Override
    public Object getItem(int i) {
        return listArray.get(i);    // single item in the list
    }

    @Override
    public long getItemId(int i) {
        return i;                   // index number
    }

    @Override
    public View getView(int index, View view, final ViewGroup parent) {

        if (view == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            view = inflater.inflate(R.layout.grid_single_query, parent, false);
        }

        final DataModel dataModel = listArray.get(index);

        ImageView gambar_ref = (ImageView) view.findViewById(R.id.imageView1);
        TextView destinasi_ref = (TextView) view.findViewById(R.id.textView1);
        TextView kota_ref = (TextView) view.findViewById(R.id.textView3);

        final String deskripsi_ref = dataModel.getDeskripsi_ref();
        destinasi_ref.setText(dataModel.getDestinasi_ref());
        kota_ref.setText(dataModel.getKota_ref());

        foto_ref = dataModel.getFoto_ref();


        Picasso.with(parent.getContext())
                .load(foto_ref)
                .placeholder(R.drawable.loading)   // optional
                .error(R.drawable.loading)      // optional
                .fit()
                .centerInside()
                .into(gambar_ref);

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
/*                Log.d(TAG, "string: " + dataModel.getNama());
                Log.d(TAG, "int: " + dataModel.getTanggal());
                Log.d(TAG, "double: " + dataModel.getHarga());
                Log.d(TAG, "otherData: " + dataModel.getRuangan_sisa());
                Log.d(TAG, "TRANSAKSI ID: " + dataModel.getTransaksiid());
                CustomModelRangkuman.getInstance().changeState(true, dataModel.getTransaksiid());

                Toast.makeText(parent.getContext(), "view clicked: " + dataModel.getNama(), Toast.LENGTH_SHORT).show();*/
                CustomModelReferensi.getInstance().changeState(true, dataModel.getDestinasi_ref(), dataModel.getKota_ref(), dataModel.getDeskripsi_ref(), dataModel.getFoto_ref());
                Log.d("Deskripsi", deskripsi_ref);
            }
        });

        return view;
    }
}
