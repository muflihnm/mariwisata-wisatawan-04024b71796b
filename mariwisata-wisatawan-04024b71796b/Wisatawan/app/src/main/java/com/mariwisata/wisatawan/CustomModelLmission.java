package com.mariwisata.wisatawan;

import android.util.Log;

/**
 * Created by Cerwyn on 06/02/2018.
 */

public class CustomModelLmission {
    public interface OnCustomStateListener {
        void stateChangedLmission(String tagar, String kota, String key, String destinasi, String deskripsi, String foto);
    }

    private static CustomModelLmission mInstance;
    private OnCustomStateListener mListener;
    private boolean mState;

    private CustomModelLmission() {}

    public static CustomModelLmission getInstance() {
        if(mInstance == null) {
            mInstance = new CustomModelLmission();
        }
        return mInstance;
    }

    public void setListener(OnCustomStateListener listener) {
        mListener = listener;
    }
    //tagar, kota, key, destinasi, deskripsi, foto
    public void changeStateLmission(boolean state, String tagar, String kota, String key, String destinasi, String deskripsi, String foto) {
        if(mListener != null) {
            mState = state;
            Log.d("ChangeState", "True");
            notifyStateChange(tagar, kota, key, destinasi, deskripsi, foto);
        }
    }

    public boolean getState() {
        return mState;
    }

    private void notifyStateChange(String tagar, String kota, String key, String destinasi, String deskripsi, String foto) {
        Log.d("Notify", "True");
        mListener.stateChangedLmission(tagar, kota, key, destinasi, deskripsi, foto);
    }
}
